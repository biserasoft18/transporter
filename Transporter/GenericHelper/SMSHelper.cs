﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;

namespace GenericHelper
{
    public static class SMSHelper
    {
        public static string SendSMSMsg(Int64 toNumber, string subject, string body)
        {
            string retVal = "Success sent SMS";
            if (ConfigurationManager.AppSettings["AllowSMS"].ToString().ToUpper() == "YES")
            {
                if (ConfigurationManager.AppSettings["UseTextLocalSMS"].ToString().ToUpper() == "YES")
                {
                    return SendSMSMsg_TextLocal(toNumber, subject, body);
                }
                else
                {
                    return SendSMSMsg_SMSMantra(toNumber, subject, body);
                }
            }
            else
            {
                return retVal;
            }
        }
        public static string SendSMSMsg_SMSMantra(Int64 toNumber, string subject, string body)
        {            
            try
            {
                string retVal = "Success sent SMS";
                if (ConfigurationManager.AppSettings["AllowSMS"].ToString().ToUpper() == "YES")
                {
                    //string URL = ConfigurationManager.AppSettings["SMSGateway"].ToString();
                    if(body.Length > 159)
                    {
                        body = body.Substring(0, 159);
                    }
                    
                    string URL = $"";
                    URL = URL + ConfigurationManager.AppSettings["SMSGateway"].ToString();
                    URL = URL + "?username=" + ConfigurationManager.AppSettings["SMSusername"].ToString();
                    URL = URL + "&password=" + ConfigurationManager.AppSettings["SMSpassword"].ToString();
                    URL = URL + "&sendername=" + ConfigurationManager.AppSettings["SMSsendername"].ToString();
                    URL = URL + "&mobileno=" + toNumber.ToString();
                    URL = URL + "&message=" + body;
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                    client.BaseAddress = new System.Uri(URL);
                    //byte[] cred = System.Text.UTF8Encoding.UTF8.GetBytes("username:password");
                    //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    System.Net.Http.HttpContent content = new StringContent("", System.Text.UTF8Encoding.UTF8, "application/json");
                    HttpResponseMessage messge = client.PostAsync(URL, content).Result;
                    string description = string.Empty;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        retVal = result;
                    }                    
                }
                return retVal;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public static string SendSMSMsg_TextLocal(Int64 toNumber, string subject, string body)
        {
            try
            {
                string retVal = "Success sent SMS";
                if (ConfigurationManager.AppSettings["AllowSMS"].ToString().ToUpper() == "YES")
                {
                    String result;
                    string apiKey = ConfigurationManager.AppSettings["TLSMSApiKey"].ToString();
                    string numbers = toNumber.ToString(); // in a comma seperated list
                    string message = body.ToString();
                    string sender = ConfigurationManager.AppSettings["TLSMSSender"].ToString();

                    String url = ConfigurationManager.AppSettings["TLSMSGateway"].ToString() + "&hash=" + apiKey + "&numbers=" + numbers + "&message=" + message + "&sender=" + sender + "&test=" + ConfigurationManager.AppSettings["TLSMSInTestMode"].ToString();
                    //refer to parameters to complete correct url string

                    StreamWriter myWriter = null;
                    HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);

                    objRequest.Method = "POST";
                    objRequest.ContentLength = Encoding.UTF8.GetByteCount(url);
                    objRequest.ContentType = "application/x-www-form-urlencoded";
                    try
                    {
                        myWriter = new StreamWriter(objRequest.GetRequestStream());
                        myWriter.Write(url);
                    }
                    catch (Exception e)
                    {
                        return e.Message;
                    }
                    finally
                    {
                        myWriter.Close();
                    }

                    HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                    using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                        // Close and clean up the StreamReader
                        sr.Close();
                    }
                    return result;
                }
                return retVal;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public async static Task<string> SendSMS1(Int64 ToNumber, string Subject, string Body)
        {
            if (ConfigurationManager.AppSettings["AllowSMS"].ToString().ToUpper() == "YES")
            {
                //string SMSGatewayURL = $"http://bulksms.mysmsmantra.com:8080/WebSMS/SMSAPI.jsp?username=mantdemo1&password=1630306226&sendername=SMDEMO&mobileno=918750001001&message=test";
                string SMSGatewayURL = $"";
                SMSGatewayURL = SMSGatewayURL + ConfigurationManager.AppSettings["SMSGateway"].ToString();
                SMSGatewayURL = SMSGatewayURL + "?username=" + ConfigurationManager.AppSettings["SMSusername"].ToString();
                SMSGatewayURL = SMSGatewayURL + "&password=" + ConfigurationManager.AppSettings["SMSpassword"].ToString();
                SMSGatewayURL = SMSGatewayURL + "&sendername=" + ConfigurationManager.AppSettings["SMSsendername"].ToString();
                SMSGatewayURL = SMSGatewayURL + "&mobileno=" + ToNumber.ToString();
                SMSGatewayURL = SMSGatewayURL + "&message=" + Body;
                
                var client = new HttpClient();
                var apiGetUrl = SMSGatewayURL;
                string retValue = "";
                HttpResponseMessage retObj = await client.GetAsync(new Uri(apiGetUrl));
                retObj.EnsureSuccessStatusCode();
                retValue = await retObj.Content.ReadAsStringAsync();
                return retValue;
            }
            else
            {
                return "";
            }            
        }

        public static string SendSMSSync1(Int64 ToNumber, string Subject, string Body)
        {
            if (ConfigurationManager.AppSettings["AllowSMS"].ToString().ToUpper() == "YES")
            {
                //string SMSGatewayURL = $"http://bulksms.mysmsmantra.com:8080/WebSMS/SMSAPI.jsp?username=mantdemo1&password=1630306226&sendername=SMDEMO&mobileno=918750001001&message=test";
                string SMSGatewayURL = $"";
                SMSGatewayURL = SMSGatewayURL + ConfigurationManager.AppSettings["SMSGateway"].ToString();
                SMSGatewayURL = SMSGatewayURL + "?username=" + ConfigurationManager.AppSettings["SMSusername"].ToString();
                SMSGatewayURL = SMSGatewayURL + "&password=" + ConfigurationManager.AppSettings["SMSpassword"].ToString();
                SMSGatewayURL = SMSGatewayURL + "&sendername=" + ConfigurationManager.AppSettings["SMSsendername"].ToString();
                SMSGatewayURL = SMSGatewayURL + "&mobileno=" + ToNumber.ToString();
                SMSGatewayURL = SMSGatewayURL + "&message=" + Body;

                var client = new HttpClient();
                var apiGetUrl = SMSGatewayURL;

                using (var httpClient = new HttpClient())
                {
                    var response = httpClient.GetAsync(apiGetUrl).Result;
                    var result = response.Content.ReadAsStringAsync().Result;

                    /*ToDo: Parse Json*/

                    return result;
                }
            }
            else
            {
                return "";
            }
        }
    }
}
