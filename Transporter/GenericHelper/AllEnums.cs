﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericHelper
{
    public static class AllEnums
    {
        public enum UserType
        {
            Agent = 1,
            Transporter =2,
            EndCustomer =3,
            AgentAndTransporter = 4
        }
    }
}
