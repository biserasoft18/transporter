﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.IO;
using System.Net;

namespace GenericHelper
{
    public class FileSystem
    {

        // TODO: Can be moved to web.config
        public const int ImageMinBytesSize = 512;
        public const int ImageMaxBytesSize = 1048576; // 1 MB = 1048576 bytes
        private readonly string ftpUserName = ConfigurationManager.AppSettings["FTPUserName"].ToString();
        private readonly string ftpPassword = ConfigurationManager.AppSettings["FTPPassWord"].ToString();
        private readonly string ftpUrl = ConfigurationManager.AppSettings["FTPUrl"].ToString();
        private readonly string ftpTempPath = ConfigurationManager.AppSettings["FTPTempUserfolder"].ToString();
        private readonly string ftpUserPath = ConfigurationManager.AppSettings["FTPUserfolder"].ToString();

        public bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        private Boolean CreateUserDir(string ftpFolderUrl)
        {
            try
            {
                WebRequest request = WebRequest.Create(ftpFolderUrl);
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(ftpUserName, ftpPassword);
                WebResponse response = request.GetResponse();

                return true;
            }
            catch(Exception ex)
            {
                return false;
                throw ex;
            }            
        }

        public Boolean UploadFileToFTP(string source, int UserID)
        {
            try
            {
                string folderPath = ftpUrl + ftpUserPath + "/" + UserID.ToString();
                if (CreateUserDir(folderPath))
                {
                    string filename = Path.GetFileName(source);
                    string ftpfullpath = folderPath + "/" + filename;
                    FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(ftpfullpath);
                    ftp.Credentials = new NetworkCredential(ftpUserName, ftpPassword);

                    ftp.KeepAlive = true;
                    ftp.UseBinary = true;
                    ftp.Method = WebRequestMethods.Ftp.UploadFile;

                    FileStream fs = File.OpenRead(source);
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    fs.Close();

                    Stream ftpstream = ftp.GetRequestStream();
                    ftpstream.Write(buffer, 0, buffer.Length);
                    ftpstream.Close();
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch
            {
                //throw ex;
                return false;
            }
        }

    }
}
