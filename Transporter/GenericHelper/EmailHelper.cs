﻿using System;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace GenericHelper
{
    public static class EmailHelper
    {
        
        private static readonly string FromAddress = ConfigurationManager.AppSettings["SMTPFromAdd"].ToString(); // "postmaster@fetchmygoods.com";
        private static readonly string FromAddressPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString(); //"B1serasoft";
        private static readonly int MailServerPort = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString()); // 587;
        
        //http://biserasoft-001-site1.itempurl.com/smtptester/ 
        /*
        private static readonly string FromAddress = "noreply@fetchmygoods.com";
        private static readonly string FromAddressPassword = "Th1nkp@d8";
        private static readonly int MailServerPort = 8889;
        */
        //http://biserasoft-001-site1.itempurl.com/smtptester/ 

        public static bool SendEmail(string ToAddress, string MailSubject, string MailBody, ref string OutputStatus)
        {
            bool retValue = false;
            MailMessage m = new MailMessage();
            SmtpClient sc = new SmtpClient("mail.fetchmygoods.com");
            m.From = new MailAddress(FromAddress);
            m.To.Add(ToAddress);
            m.Subject = MailSubject;
            m.Body = MailBody;
            sc.Host = "mail.fetchmygoods.com";
            try
            {
                sc.Port = MailServerPort;
                sc.Credentials = new System.Net.NetworkCredential(FromAddress, FromAddressPassword);
                sc.EnableSsl = false;
                sc.Send(m);
                retValue = true;
                OutputStatus = "Email Send successfully";
            }
            catch (Exception ex)
            {
                OutputStatus = "Error sending email." + ex.Message;
                retValue = false;
            }
            
            return retValue;
        }

        public static bool SendEmailB(string ToAddress, string MailSubject, string MailBody, ref string OutputStatus)
        {
            bool retValue = false;
            if (ConfigurationManager.AppSettings["AllowEMail"].ToString().ToUpper() == "YES")
            {                
                MailMessage m = new MailMessage();
                SmtpClient sc = new SmtpClient();
                m.From = new MailAddress(FromAddress);
                m.To.Add(ToAddress);
                m.Subject = MailSubject;
                m.Body = MailBody;
                sc.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString(); //"mail.fetchmygoods.com";
                try
                {
                    sc.Port = MailServerPort;
                    sc.Credentials = new System.Net.NetworkCredential(FromAddress, FromAddressPassword);
                    sc.EnableSsl = false;
                    sc.Send(m);
                    retValue = true;
                    OutputStatus = "Email Sent successfully";
                }
                catch (Exception ex)
                {
                    OutputStatus = "Error sending email." + ex.Message;
                    retValue = false;
                }
            }
            else
            {
                retValue = true;
                OutputStatus = "Email sending not allowed, contact admin";
            }           
            
            return retValue;
        }
    }
}
