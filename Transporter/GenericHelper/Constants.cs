﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericHelper
{
    public class Constants
    {
        public class SPName
        {
            #region "Master tables"
            public const string GetGoodsType = "GetGoodsType_SP";
            public const string GetGoodsWeight = "GetGoodsWeight_SP";
            public const string GetBookingStatus = "GetBookingStatus_SP";
            public const string GetPostingStatus = "GetPostingStatus_SP";
            public const string GetUserType = "GetUserType_SP";
            public const string GetTruckType = "GetTruckType_SP";
            public const string GetPackageType = "GetPackageType_SP";
            public const string GetSecretQuestion = "GetSecretQuestion_SP";
            public const string GetCity = "GetCity_SP";
            public const string GetState = "GetState_SP";
            public const string GetTown = "GetTown_SP";
            public const string GetCountry = "GetCountry_SP";
            public const string GetCityByFilter = "GetCityByFilter_SP";
            public const string GetPaymentType = "GetPaymentType_SP";
            #endregion

            public const string ValidateUser = "CheckUserLogin_SP";

            public const string GetPasswordforUser = "GetUserPassword_SP";

            public const string GetBookingPageDetails = "GetBookingPageDetails_SP";

            public const string GetRegisterDetails = "GetRegisterDetails_SP";

            public const string GetStateDetails = "GetStateName_SP";

            public const string GetTownDetails = "GetTownDetails_SP";

            public const string GetCityDetails = "GetCityDetails_SP";

            public const string GetUserTypeDetails = "GetUserType_SP";

            public const string ValidateLoginID = "Validate_LoginID_SP";

            public const string GetSearchDetails = "GetSearchDetails_SP";

            public const string GetTransSearchDetails = "GetTransportSearchDetails_SP";

            public const string GetDashBoardPageDetails = "GetUserDashboard_SP";

            public const string GetAgentSearchPageDetails = "GetLoadAvailabiltyforOtherAgents_SP";

            public const string GetPostingPageDetails = "GetPostingPageDetails_SP";


            public const string GetTransporterSearchPageDetails = "GetLoadAvailabiltyforTransporters_SP";

            public const string GetPostingResponseFromAgent = "GetPostingResponseFromAgent_SP";

            public const string GetBookingResponseFromTransporter = "GetBookingResponseFromTransporter_SP";

            public const string GetDriverDetailsForTransp = "GetDriverDetailForTransporter_SP";

            public const string GetGenericReportDetails = "GetGenericReportDetails_SP";

            public const string GetUserPaymentDetails = "GetUserPaymentDetails_SP";

            public const string GetBusinessType = "GetBusinessType_SP";
        }

        public class PageTexts
        {
            public const string DropdownDefaultSelect = "--Select List--";
        }

        public class AgentBookingStatus
        {
            public const int Created=100;
            public const int BidsReceived = 200;
            public const int BidsAccepted = 300;
            public const int GoodsPickedUp = 400;
            public const int GoodsDelivered = 500;
        }
        public class TransporterPostingStatus
        {
            public const int PostingCreated = 100;
            public const int PostingOpen = 200;
            public const int BookingsInProgress = 300;
            public const int BookingsClosed = 400;
            public const int TripSheetPrepared = 500;
            public const int TripStarted = 600;
            public const int TripCompleted = 700;
        }
    }
}
