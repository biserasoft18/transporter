﻿function PrintLineBreakWithRow() {    
    document.write("<div class='row'>");
    document.write("    <div class='col-md-12 col-sm-12 text-left'>&nbsp;</div>");
    document.write("</div>");
}

function PrintLineBreak() {
    document.write("    <div class='col-md-12 col-sm-12 text-left'>&nbsp;</div>");
}

function Print1Button(buttonName, onclickMethodName, buttonIcon, buttonClass) {
    /*
    <div class="row">
            <div class="col-md-6 col-sm-12 align-items-end">
                <div class="form-group row">
                    <div class="col-md-4 pull-left">&nbsp;</div>
                    <div class="input-group col-md-8 cpd-group">
                        <button type="button" class="btn btn-success " onclick="CreateBookingData();"><i class="fa fa-plus"></i>SAVE </button>
                        <br />
                        <br />
                    </div>
                </div>                
            </div>
        </div>

        Example: Print1Button("SAVE", "CreateBookingData", "fa-plus");
    */
    document.write("<div class='row'>");
    document.write("    <div class='col-md-12 col-sm-12 align-items-end'>");
    document.write("        <div class='form-group row'>");
    document.write("            <div class='col-md-9 pull-left'>&nbsp;</div>");
    document.write("            <div class='input-group col-md-3 cpd-group'>");
    document.write("            <button type='button' class='btn " + buttonClass + "' onclick='" + onclickMethodName + "();'><i class='fa " + buttonIcon + "'></i> " + buttonName + "</button>");
    document.write("            </div>");   
    document.write("        </div>");
    document.write("    </div>");
    document.write("</div>");
}

function PrintPageMenu() {
    if (isLoggedInUser == "true") {
        document.write("<div class='menublock'>");
        document.write("<div class='menuItem'>");
        document.write("    <button class='menuText' onclick='document.location = logoutPage;'>Logout</button>");
        document.write("</div> ");
        document.write("</div>");
    }
}

function LogAndShowAjaxError(thrownError, errorStatus) {
    modalPopup.SetText('Error', '', thrownError);
    console.log('error', errorStatus, thrownError);
}

function LogAjaxError(thrownError, showError) {
    if (showError) {
        modalPopup.SetText('Error', '', thrownError);
    }
    console.log('error: ' + thrownError);
}

function DoLogout() {
    window.location.href = logoutPage;
    //$("#MobileLoginText").text("Login");
    //$("#LoginText").text("Login");
}

function DoLogin() {
    var username = $('#data-username').val();
    var password = $('#data-password').val();
    console.log("Sign In clicked");
    $.ajax({
        type: "POST",
        url: '/Home/ValidateLogin/',
        data: JSON.stringify({ UserName: username, Password: password }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: successFunc,
        error: errorFunc
    });
    function successFunc(data, status) {
        if (data.isRedirect) {
            isLoggedInUser = true;
            window.location.href = data.redirectUrl;
        } else {
            modalPopup.SetText('Login Error', '', data);
        }
    }
    function errorFunc(result) {
        LogAjaxError(result, true);
    }
}

function RegisterMe() {
    $.ajax({
        type: "POST",
        url: '/Home/Register/',
        data: JSON.stringify(),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: successFunc,
        error: errorFunc
    });
    function successFunc(data, status) {
        if (data.isRedirect) {
            isLoggedInUser = true;
            window.location.href = data.redirectUrl;
        } else {
            //alert(data);
            //$('#LoginErrorMessage').text(data);
            //$('#LoginErrorMessage').addClass('isa_error');
            modalPopup.SetText('Login Error', '', data);
        }
    }
    function errorFunc(result) {
        LogAjaxError(result, true);
    }
}

function supportsImports() {
    return 'import' in document.createElement('link');
}

function handleLoad(e) {
    console.log('Loaded import: ' + e.target.href);
}

function handleError(e) {
    console.log('Error loading import: ' + e.target.href);
}

function SetAutoComplete(textboxToSet, hdnTextboxToSet) {
    var autoComp = new autoComplete({
        selector: textboxToSet,
        minChars: 1,
        source: function (request, suggest) {
            term = request.toLowerCase();

            var ParamData = '/' + term;
            var SearchUrl = '../api/Generic/GetCitiesByName' + ParamData;
            var suggestions = [];
            //begin source
            $.ajax({
                url: SearchUrl, //'../api/Generic/FilterCities/',
                type: 'GET',
                cache: false,
                data: { value: request.term },
                dataType: 'json',
                success: function (jsonData) {
                    suggest($.map(jsonData, function (item) {
                        var itemLabel = "";
                        var itemValue = "";
                        itemLabel = itemLabel + item.CityNameWithState + "";
                        itemValue = itemValue + item.CityID + "";
                        return {
                            label: itemLabel,
                            value: itemValue
                        }
                    }));
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    LogAndShowAjaxError(thrownError, xhr.status);
                }
            });
        },
        minChars: 3,
        onSelect: function (e, term, item) {
            $(textboxToSet).val($(item).text());
            $(hdnTextboxToSet).val($(item).attr("data-val"));
            return false;
        }
    });
    return autoComp;
}

function CheckNullInString(input) {
    if (input) {
        return input;
    } else {
        return "";
    }
}

function CheckNullInAddressObj(inObj) {
    if (inObj == null) {
        return "";
    }
    if (inObj == "") {
        return "";
    } else {
        return inObj + ", ";
    }
}

function ProcessAjaxResult(result, fieldToSetWithValue) {
    var dataUniqueID = result.UniqueId;
    var IsSuccess = result.IsSuccess;
    var msg = result.Message;
    var url = result.RedirectURL;
    if (IsSuccess) {
        if (fieldToSetWithValue != undefined && fieldToSetWithValue != "") {
            $('#' + fieldToSetWithValue).text(dataUniqueID);
        }
        modalPopup.SetText('Success', '', msg, url);
    } else {
        modalPopup.SetText('Failure', '', msg);
    }
}

function CheckStringNull(inputVal) {
    if (inputVal) {
        //inputVal = "";
        return inputVal;
    } else {
        return "";
    }
}

var getUrlParameter = function getUrlParameter(sParam) {
    /*
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
    */
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sPageURL = sPageURL.replace("returnURL=", "");
    return sPageURL;
};

function ShowContactInfo(CurrentID) {
    // event.preventDefault(); To avoid scroll to page top
    var ajaxData = { CAgentID: CurrentID };
    var ajaxUrl = '../User/ShowContactInfo';
    ShowContentInPopup(ajaxData, ajaxUrl);
}

function ShowContentInPopup(inData, inUrl) {
    debugger;
    $.ajax({
        data: inData,
        datatype: "text/plain",
        type: "GET",
        url: inUrl, //'../Booking/ShowBookingInfo',
        cache: false,
        success: function (data) {
            $("#main_popupContent").html(data);
            OpenMainHTMLPopup(data, "#main_popupContent");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            LogAndShowAjaxError(thrownError, xhr.status);
        }
    });
}

function ShowBookingInfo(bookingID) {
    // event.preventDefault(); To avoid scroll to page top
    var ajaxData = { bookingId: bookingID};
    var ajaxUrl = '../Booking/ViewBookingPopup';
    ShowContentInPopup(ajaxData, ajaxUrl);
}

function ShowPostingInfo(postingID) {
    // event.preventDefault(); To avoid scroll to page top
    var ajaxData = { postingId: postingID };
    var ajaxUrl = '../Posting/ViewPostingPopup';
    ShowContentInPopup(ajaxData, ajaxUrl);
}

function ShowStaticPagePopup(popupPageName) {
    $.ajax({
        //data: { DriverID: selectedVal },
        datatype: "text/plain",
        type: "GET",
        url: '/Static/' + popupPageName,
        cache: false,
        success: function (data) {
            $("#main_popupContent").html(data);
            OpenMainHTMLPopup(data, "#main_popupContent");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            LogAndShowAjaxError(thrownError, xhr.status);
        }
    });
    //$e.preventDefault();
}

function ClearAllFormInputFields() {
    $("input[type=text]").val("");
    $("input[type=hidden]").val("");
    $('form select').each(function () {
        $(this).find('option').removeAttr('selected');
        $(this).find('option:first').attr('selected', 'selected');
    });
}