﻿var modalPopup = (function () {

    function setHeader(headerText) {
        $("div.modal-header h2").html(headerText);
    }

    function setFooter(footerText) {
        $("div.modal-footer h2").html(footerText);
    }

    function setContent(contentText) {
        $("div.modal-body p").html(contentText);
    }

    function setText(headerText, footerText, contentText, redirectURL) {
        $("div.modal-header h2").html(headerText);
        $("div.modal-footer h2").html(footerText);
        if (contentText.message != null) {
            $("div.modal-body p").html(contentText.message);
        } else {
            $("div.modal-body p").html(contentText);
        }
        
        $("#closeButtonRedirectURL").val(redirectURL);
        popupModal.style.display = "block";
    }
    function setMessageType() {
        console.log('nothing to write here');
    }

    return {
        SetHeader: setHeader,
        SetContent: setContent,
        SetMessageType: setMessageType,
        SetFooter: setFooter,
        SetText: setText
    }
}) (window);
