﻿using System.Web;
using System.Web.Optimization;

namespace Transporter
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js")
                        .Include(
                        "~/Scripts/jquery-ui-1.10.0.js")
                        .Include(
                        "~/Scripts/bootstrap-select.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/bootstrap-select.min.css"));

            bundles.Add(new StyleBundle("~/Content/DropDownSpecialCSS").Include(
          "~/Content/jquery.mobile-1.2.0.min.css",
          "~/Content/jquery.selectBoxIt.css"));

            bundles.Add(new ScriptBundle("~/Scripts/DropDownSpecialJS")
               .Include("~/Scripts/jquery.selectBoxIt.min.js")
              );


            bundles.Add(new StyleBundle("~/Content/TransporterCSS").Include(
                    "~/Content/themes/base/jquery-ui.css",
                    "~/Content/jquery.jqGrid/ui.jqgrid.css",
                    "~/Content/site.css", 
                    "~/Content/TransporterMain.css",
                    "~/Content/ModalPopup.css",
                    "~/Content/header-fixed.css",
                    "~/Content/pageMenu.css",
                    "~/Content/pageContentGrid.css"
                    )
                );
            bundles.Add(new ScriptBundle("~/Scripts/TransporterJS")
                .Include("~/Scripts/Common.js")
                .Include("~/Scripts/ModalPopup.js")
               );


            ////////////new UI bundles///////////////////

            bundles.Add(new StyleBundle("~/Content/newUIcss").Include(
                       "~/Content/font-awesome.css",
                       "~/Content/bootstrap-datetimepicker.css",
                   "~/Content/bootstrap.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/newUIjs")
                .Include("~/Scripts/jquery.js")
                .Include("~/Scripts/bootstrap.min.js")
                .Include("~/Scripts/moment.js")
                .Include("~/Scripts/bootstrap-datetimepicker.js")
                .Include("~/Scripts/jquery.mmenu.js")
               );
        }
    }
}
