﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransporterModel;
using TransporterCore;
using InstamojoAPI;
using GenericHelper;
using System.Configuration;
using System.Web.Script.Serialization;

namespace Transporter.Controllers
{
    public class SubscriptionController : BaseController
    {
        // GET: Subsription
        public ActionResult Index()
        {
            UserSubscription returnObj = new UserSubscription();
            returnObj.StartDate = ((User)Session["User"]).ValidFrom.ToString("dd-MMM-yyyy");
            returnObj.EndDate = ((User)Session["User"]).ValidTo.ToString("dd-MMM-yyyy");
            //returnObj.Type = ((User)Session["User"]).UserTypeID.ToString();
            returnObj.Type = Enum.GetName(typeof(AllEnums.UserType), ((User)Session["User"]).UserTypeID).ToString();
            return View(returnObj);
        }

        private ClientPaymentInitiate InitPaymentObj(int Duration, int Type)
        {
            ClientPaymentInitiate Model = new ClientPaymentInitiate();
            User refObj = (User)Session["User"];

            Model.UserFullName = (refObj.FirstName.ToString() + " " + (refObj.LastName.ToString()));
            Model.UserEmail = refObj.Email.ToString();
            Model.UserID = refObj.UserID;
            Model.UserTypeID = refObj.UserTypeID;
            Model.UserMobileNumber = refObj.MobileNumber.ToString();
            Model.PaymentTransactionID = refObj.UserID.ToString() + DateTime.Now.ToString("yyyymmddhhmmss"); // Guid.NewGuid().ToString();
            Model.PaymentInitiateDateFrom = DateTime.Now.ToString("dd-mmm-yyyy");  //("dd-mmm-yyyy h:mm tt");
            Model.PaymentInitiateDateTo = DateTime.Now.AddMonths(Duration).ToString("dd-mmm-yyyy");
            Model.PaymentAmount = ((Duration == 1) ? 10 : 12).ToString();//((Duration == 1) ? 1000 : 10000).ToString(); // TODO: This is for test the payment gateway purposes, will need to revert to 1000 and 10000 
            Model.PaymentCurrency = "INR";
            Model.PaymentInitiateTypeID = Convert.ToInt32(Type.ToString() + Duration.ToString());
            return Model;
        }

        public ActionResult ShowPayment(int Duration, int Type)
        {
            return PartialView("_ShowPayment", InitPaymentObj(Duration, Type));
        }

        [HttpPost]
        public JsonResult InitPayment(int Duration, int Type)
        {
            AjaxReturnData returnVal = new AjaxReturnData();
            returnVal.UniqueId = 0;
            returnVal.IsSuccess = false;
            returnVal.Message = "";

            try
            {
                int InternalPaymentID = 0;
                string successPrompt = "You will be now redirected to a 3rd party website for payment, please do not close the browser until the payment is complete!";
                ClientPaymentInitiate Model = InitPaymentObj(Duration, Type);
                string Insta_client_id = ConfigurationManager.AppSettings["PayClientID"].ToString();
                string Insta_client_secret = ConfigurationManager.AppSettings["PayClientSecret"].ToString();
                string Insta_Endpoint = ConfigurationManager.AppSettings["PayEndPoint"].ToString();
                string Insta_Auth_Endpoint = ConfigurationManager.AppSettings["PayAuthEndPoint"].ToString();
                string Insta_redirectURL = ConfigurationManager.AppSettings["PaymentRedirect"].ToString();
                string Insta_webhook = ConfigurationManager.AppSettings["PaymentWebHook"].ToString();

                Instamojo objClass = InstamojoImplementation.getApi(Insta_client_id, Insta_client_secret, Insta_Endpoint, Insta_Auth_Endpoint);

                PaymentOrder objPaymentRequest = new PaymentOrder();
                //Required POST parameters
                objPaymentRequest.name = Model.UserFullName;
                objPaymentRequest.email = Model.UserEmail;
                objPaymentRequest.phone = Model.UserMobileNumber;
                objPaymentRequest.amount = Convert.ToDouble(Model.PaymentAmount);
                objPaymentRequest.currency = Model.PaymentCurrency; //"Unsupported"
                objPaymentRequest.transaction_id = Model.PaymentTransactionID;

                objPaymentRequest.redirect_url = Insta_redirectURL;
                objPaymentRequest.webhook_url = Insta_webhook + objPaymentRequest.transaction_id.ToString();

                SubscriptionBL updObj = new SubscriptionBL();
                updObj.UpsertPaymentDetails(((User)Session["User"]).UserID, ((User)Session["User"]).ValidFrom.ToString("dd-MMM-yyyy"), ((User)Session["User"]).ValidTo.ToString("dd-MMM-yyyy"), (Decimal)objPaymentRequest.amount,
                            objPaymentRequest.transaction_id, DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(),
                            DateTime.Now.ToString(), DateTime.Now.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), 1, DBNull.Value.ToString(), ref InternalPaymentID);


                if (objPaymentRequest.validate())
                {
                    try
                    {
                        CreatePaymentOrderResponse objPaymentResponse = objClass.createNewPaymentRequest(objPaymentRequest);
                        returnVal.Message = successPrompt;
                        returnVal.RedirectURL = objPaymentResponse.payment_options.payment_url.ToString();
                        returnVal.IsSuccess = true;

                        // Code to write the request and response object to the DB table:

                        updObj.UpsertPaymentDetails(((User)Session["User"]).UserID, ((User)Session["User"]).ValidFrom.ToString("dd-MMM-yyyy"), ((User)Session["User"]).ValidTo.ToString("dd-MMM-yyyy"), (Decimal)objPaymentRequest.amount,
                            objPaymentRequest.transaction_id, DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(),
                            DateTime.Now.ToString(), DateTime.Now.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), 1, DBNull.Value.ToString(), ref InternalPaymentID);

                        return Json(returnVal, JsonRequestBehavior.AllowGet);
                        //MessageBox.Show("Order Id = " + objPaymentResponse.order.id);
                    }
                    //catch (ArgumentNullException ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}
                    //catch (WebException ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}
                    //catch (IOException ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}
                    //catch (InvalidPaymentOrderException ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}
                    //catch (ConnectionException ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}
                    //catch (BaseException ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}
                    catch (Exception ex)
                    {
                        //MessageBox.Show("Error:" + ex.Message);
                        throw ex;
                    }
                }
                else
                {
                    try
                    {
                        CreatePaymentOrderResponse objPaymentResponse = objClass.createNewPaymentRequest(objPaymentRequest);
                        returnVal.IsSuccess = true;
                        returnVal.Message = successPrompt;
                        returnVal.RedirectURL = objPaymentResponse.payment_options.payment_url.ToString();
                        updObj.UpsertPaymentDetails(((User)Session["User"]).UserID, ((User)Session["User"]).ValidFrom.ToString("dd-MMM-yyyy"), ((User)Session["User"]).ValidTo.ToString("dd-MMM-yyyy"), (Decimal)objPaymentRequest.amount,
                            objPaymentRequest.transaction_id, objPaymentResponse.order.id.ToString(), objPaymentRequest.redirect_url.ToString(), objPaymentResponse.order.webhook_url.ToString(),
                            objPaymentResponse.payment_options.payment_url.ToString(), DateTime.Now.ToString(), DateTime.Now.ToString(), DBNull.Value.ToString()
                            , DBNull.Value.ToString(), DBNull.Value.ToString(), 1, DBNull.Value.ToString(), ref InternalPaymentID);

                        return Json(returnVal, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                returnVal.Message = ex.Message;
                returnVal.IsSuccess = false;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult PaymentAcknowledgement(string id, string transaction_id, string payment_id)
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult PaymentHistory()
        {
            UserPaymentHistory Model = new UserPaymentHistory();
            SubscriptionBL returnObj = new SubscriptionBL();
            string retVal = "";
            Model = returnObj.GetPaymentDetails(((User)Session["User"]).UserID, ((User)Session["User"]).UserID, ref retVal);
            if (retVal.ToUpper().Contains("ERROR"))
            {
                throw new Exception(retVal);
            }
            return View(Model);
        }


        //    [HttpPost]
        //    public JsonResult PaymentConfirmation([FromBody] PaymentResponse value, int pID)
        //    {
        //        string result = "Failure";
        //        AjaxReturnData returnVal = new AjaxReturnData();
        //        returnVal.UniqueId = 0;
        //        returnVal.IsSuccess = false;
        //        returnVal.Message = result;
        //        try
        //        {
        //            int retVal = 1;
        //            SubscriptionBL paymentObj = new SubscriptionBL();
        //            var json = new JavaScriptSerializer().Serialize(value);
        //            log.Info("From Webhook:" + json);
        //            paymentObj.UpsertPaymentDetails(0, DBNull.Value.ToString(), DBNull.Value.ToString(), 0,
        //                        DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(),
        //                        DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString()
        //                        , DBNull.Value.ToString(), DBNull.Value.ToString(), 1, json, ref retVal);

        //            returnVal.IsSuccess = true;
        //            returnVal.Message = "Success";
        //            return Json(returnVal, JsonRequestBehavior.AllowGet);
        //        }
        //        catch (Exception ex)
        //        {
        //            returnVal.IsSuccess = false;
        //            returnVal.Message = ex.Message;
        //            return Json(returnVal, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //}

        public class PaymentResponse
        {
            public Decimal amount { get; set; }
            public string buyer { get; set; }
            public string buyer_name { get; set; }
            public Int64 buyer_phone { get; set; }
            public string currency { get; set; }
            public Decimal fees { get; set; }
            public string longurl { get; set; }
            public string mac { get; set; }
            public string payment_id { get; set; }
            public string payment_request_id { get; set; }
            public string purpose { get; set; }
            public string shorturl { get; set; }
            public string status { get; set; }
        }
    }
}