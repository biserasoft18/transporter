﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransporterCore;
using TransporterModel;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using GenericHelper;
using System.Threading.Tasks;
using System.Configuration;


namespace Transporter.Controllers
{
    public class RegistrationController : BaseController
    {
        // GET: Registration
        public ActionResult Index()
        {
            UserBL RegisterObj = new UserBL();
            RegistrationPage Model = new RegistrationPage();
            Model = RegisterObj.GetRegisterDetails(0);
            ViewBag.Title = "Registration";
            Session["UserFilePath"] = null;
            return View(Model);
            // test comment from Thani
        }
        // TODO: Can be moved to web.config
        public const int ImageMinBytesSize = 512;
        public const int ImageMaxBytesSize = 1048576; // 1 MB = 1048576 bytes
        readonly string UplPath = System.Web.HttpContext.Current.Server.MapPath("~/UserUploads/").ToString();

        private bool IsImage(HttpPostedFileBase postedFile, ref string errorMessage)
        {
            //-------------------------------------------
            //  Check the image mime types
            //-------------------------------------------
            if (postedFile.ContentType.ToLower() != "image/jpg" &&
                        postedFile.ContentType.ToLower() != "image/jpeg" &&
                        postedFile.ContentType.ToLower() != "image/pjpeg" &&
                        postedFile.ContentType.ToLower() != "image/gif" &&
                        postedFile.ContentType.ToLower() != "image/x-png" &&
                        postedFile.ContentType.ToLower() != "image/png")
            {
                errorMessage = "Invalid file type: Allowed file types: jpg, jpeg, gif, png";
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            if (Path.GetExtension(postedFile.FileName).ToLower() != ".jpg"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".png"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".gif"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".jpeg")
            {
                errorMessage = "Invalid file type: Allowed file types: jpg, jpeg, gif, png";
                return false;
            }

            //-------------------------------------------
            //  Attempt to read the file and check the first bytes
            //-------------------------------------------
            try
            {
                if (!postedFile.InputStream.CanRead)
                {
                    errorMessage = "Invalid file format error";
                    return false;
                }

                if (postedFile.ContentLength < ImageMinBytesSize)
                {
                    errorMessage = "Invalid file type";
                    return false;
                }
                if (postedFile.ContentLength > ImageMaxBytesSize)
                {
                    errorMessage = "File size more than 1 MB";
                    return false;
                }

                byte[] buffer = new byte[512];
                postedFile.InputStream.Read(buffer, 0, 512);
                string content = System.Text.Encoding.UTF8.GetString(buffer);
                if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            //-------------------------------------------
            //  Try to instantiate new Bitmap, if .NET will throw exception
            //  we can assume that it's not a valid image
            //-------------------------------------------

            try
            {
                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                {
                }
            }
            catch (Exception)
            {
                errorMessage = "Invalid file type";
                return false;
            }
            finally
            {
                postedFile.InputStream.Position = 0;
            }

            return true;
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            FileUploadData retVal = new FileUploadData();
            retVal.RetMessage = "Error";
            retVal.FilePath = "";
            string UserPath = "";
            if (Session["UserFilePath"] != null)
            {
                UserPath = Session["UserFilePath"].ToString();
            }            
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //First clean up the folder for any old temp files/folders uploaded and abandoned
                    var tempFolderPath = UplPath + "temp/";
                    DeleteDirectories(tempFolderPath);

                    Guid g = Guid.NewGuid();
                    string TempFileFolder = g.ToString();
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = UplPath.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        var fileErrorMsg = "";
                        if (!IsImage(file, ref fileErrorMsg))
                        {
                            //return Json("Error uploading file: " + fileErrorMsg);
                            retVal.RetMessage = "Error uploading: " + fileErrorMsg;
                            retVal.FilePath = UserPath;
                            return Json(retVal, JsonRequestBehavior.AllowGet);
                        }
                        
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //string UplPath = AppDomain.CurrentDomain.BaseDirectory + "~/UserUploads/";
                        var newFolderPath = UplPath + "temp/";
                        if (UserPath!= "" && UserPath != null)
                        {                            
                            TempFileFolder = UserPath;
                            newFolderPath = newFolderPath + TempFileFolder;
                            fname = newFolderPath + "/" + fname;
                        }
                        else
                        {
                            newFolderPath = newFolderPath + TempFileFolder;
                            Directory.CreateDirectory(newFolderPath);
                            fname = newFolderPath + "/" + fname;
                        }
                        
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    retVal.RetMessage = "File Uploaded Successfully!";
                    retVal.FilePath = TempFileFolder;
                    Session["UserFilePath"] = TempFileFolder;
                    return Json(retVal, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    //return Json("Error occurred. Error details: " + ex.Message);
                    retVal.RetMessage = ex.Message;
                    retVal.FilePath = UserPath;
                    return Json(retVal, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        private void DeleteDirectories(string strBaseDirPath)
        {
            //string strBaseDirPath = "D:\\files\\backup\\";
            DirectoryInfo baseDir = new DirectoryInfo(strBaseDirPath);
            if (baseDir.Exists)
            {
                try
                {
                    DirectoryInfo[] subDirectories = baseDir.GetDirectories();
                    if (subDirectories != null && subDirectories.Length > 0)
                    {
                        for (int j = subDirectories.Length - 1; j >= 0; j--)
                        {
                            DirectoryInfo d = subDirectories[j];
                            if (d.CreationTime < DateTime.Now.AddDays(-7))
                                d.Delete(true);
                        }
                    }
                }catch(Exception ex)
                {
                    throw ex;
                }                
            }            
        }

        [HttpGet]
        public ActionResult RemoveFile(string fileName)
        {
            string result = "Failure";
            AjaxReturnData returnVal = new AjaxReturnData();
            returnVal.IsSuccess = false;
            returnVal.Message = result;
            try
            {
                string UserPath = Session["UserFilePath"].ToString();
                string FilePath = UplPath + UserPath + "/" + fileName;

                System.IO.File.Delete(@FilePath);
                returnVal.Message= "File removed!";
                returnVal.IsSuccess = true;
                returnVal.RedirectURL = "";

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                returnVal.IsSuccess = false;
                returnVal.Message = "File deletion error : " + ex.Message.ToString();
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
        [HttpPost]
        public JsonResult RegistrationBooking(LoadRegistration UIData)
        {
            string result = "Failure";
            //AjaxReturnData returnVal = new AjaxReturnData();
            AjaxReturnData returnVal = InitializeJSONReturnObj(UIData.UserID, result);
            try
            {
                UserBL RegisterObj = new UserBL();
                int UserID = UIData.UserID;
                UIData.IsDriver = false;
                UIData.IsAdmin = 0;
                UIData.IsPaidUser = Convert.ToInt32(ConfigurationManager.AppSettings["CreateUserAsPaidUser"].ToString()); //  0 or 1;
                //TODO: TO move all code that reads from config entry - into a public static method in generic helper layer

                if (Session["UserFilePath"] == null)
                {
                    returnVal.IsSuccess = false;
                    returnVal.Message = "Failure: Please upload supporting image files for PAN, TIN";
                    return Json(returnVal, JsonRequestBehavior.AllowGet);
                }
                string UserPath = Session["UserFilePath"].ToString();
                if (System.IO.Directory.Exists(UplPath + UserPath))
                {
                    if (IsDirectoryEmpty(UplPath + UserPath))
                    {
                        returnVal.IsSuccess = false;
                        returnVal.Message = "Failure: Please upload supporting image files for PAN, TIN";
                        return Json(returnVal, JsonRequestBehavior.AllowGet);
                    }
                }

                result = RegisterObj.SetRegisterData(UIData, ref UserID);

                string tempFolderPath = UplPath + "temp/" + UserPath;
                string userFolderPath = UplPath + "UserFiles/" + UserID.ToString();
                // Move uploaded files to permanent User folders
                if (!Directory.Exists(userFolderPath))
                {
                    Directory.CreateDirectory(userFolderPath);
                }
                if (Directory.Exists(tempFolderPath))
                {
                    string[] files = System.IO.Directory.GetFiles(tempFolderPath);

                    // Copy the files and overwrite destination files if they already exist. 
                    foreach (string s in files)
                    {
                        // Use static UplPath methods to extract only the file name from the path.
                        string tempfileName = System.IO.Path.GetFileName(s);
                        string destFile = System.IO.Path.Combine(userFolderPath, tempfileName);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }
                if (Directory.Exists(tempFolderPath))
                {
                    Directory.Delete(tempFolderPath, true);
                }

                SetJSONReturnObj(UserID, result, true, "/Home/Dashboard", ref returnVal);
                try
                {
                    string commResult = SendCommunication("UserRegistration", UIData.EmailID, UIData.MobileNumber, "Fetchmygoods.com User Created", "UserRegistration", UserID, UIData.LoginID, UIData.Password, "", "UserID", UserID);
                }
                catch
                {
                    returnVal.Message = result + ". But, error in sending communicating";
                }               

                Session["UserFilePath"] = null;

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.IsSuccess = false;
                returnVal.Message = ex.Message.ToString();
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult GetStateDetails(int CountryID)
        {
            UserBL StateObj = new UserBL();
            DataTable dt = StateObj.GetStateDetails(CountryID);
            List<Dictionary<string, object>> lststates = GetTableRows(dt);
            return Json(lststates, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetCityDetails(int StateID)
        {
            UserBL StateObj = new UserBL();
            DataTable dt = StateObj.GetCityDetails(StateID);
            List<Dictionary<string, object>> lstCity = GetTableRows(dt);
            return Json(lstCity, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetTownDetails(int CityID)
        {
            UserBL StateObj = new UserBL();
            DataTable dt = StateObj.GetTownDetails(CityID);
            List<Dictionary<string, object>> lstTown = GetTableRows(dt);
            return Json(lstTown, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ValidateLoginID(string LoginID)
        {
            string output = "";

            UserBL UserLayer = new UserBL();
            output = UserLayer.ValidateLoginID(LoginID);
            if (output.ToUpper() == "VALID")
            {
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public List<Dictionary<string, object>> GetTableRows(DataTable dtData)
        {
            List<Dictionary<string, object>>
            lstRows = new List<Dictionary<string, object>>();
            Dictionary<string, object> dictRow = null;

            foreach (DataRow dr in dtData.Rows)
            {
                dictRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtData.Columns)
                {
                    dictRow.Add(col.ColumnName, dr[col]);
                }
                lstRows.Add(dictRow);
            }
            return lstRows;
        }

        [HttpPost]
        public JsonResult LoginPage()
        {
            return Json(new
            {
                redirectUrl = Url.Action("Index", "Home"),
                isRedirect = true
            });
        }

    }
}