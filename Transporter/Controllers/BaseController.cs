﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Transporter.Controllers
{
    public class BaseController : Controller
    {
        public Boolean IsLoggedIn = false;

        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #region "Base Methods"
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewData["ClientIP"] = GetClientIPAddress(filterContext);
            ViewData["UserEnvironment"] = GetUserEnvironment(filterContext.HttpContext.Request);

            if (filterContext.HttpContext.Session["User"] == null)
            {
                ViewData["IsLoggedIn"] = "false";
                CheckRequest(ref filterContext, filterContext.HttpContext.Request.IsAjaxRequest());                
            }
            else
                {
                ViewData["IsLoggedIn"] = "true";
                //base.Execute(filterContext.RequestContext);
            }
        }

        public void CheckRequest(ref ActionExecutingContext filterContext, Boolean IsAjaxCheck)
                    {
            var curExecPath = ((System.Web.HttpRequestWrapper)((System.Web.HttpContextWrapper)filterContext.HttpContext).Request).CurrentExecutionFilePath.ToString();
            List<string> excludedPaths = IsAjaxCheck ? ConfigurationManager.AppSettings["NonAuthenticatedAjaxCalls"].ToString().Split(',').ToList() : ConfigurationManager.AppSettings["NonAuthenticatedPages"].ToString().Split(',').ToList();
            var filteredList = excludedPaths.Where(p => p.Contains(curExecPath));

            if (filteredList.Count() == 0)
            {
                if (IsAjaxCheck)
                {
                        filterContext.HttpContext.Response.StatusCode = 403;
                        filterContext.Result = new JsonResult { Data = "LogOut", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                    }
                else
                {
                    var replaceStr = ((System.Web.HttpRequestWrapper)((System.Web.HttpContextWrapper)filterContext.HttpContext).Request).Url.PathAndQuery.ToString();
                    //replaceStr = replaceStr.Replace("/", "~BS~");
                    filterContext.Result = RedirectToAction("LoginExpired", "Home", new { @returnURL = replaceStr });
                }
                
            }
            }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            var model = new HandleErrorInfo(filterContext.Exception, "Controller", "Action");

            log.Error("---Start---");
            if (filterContext.HttpContext.Session["User"] != null)
            {
                log.Error("LoggedInUserID: " + ((TransporterModel.User)filterContext.HttpContext.Session["User"]).UserID);
                log.Error("LoggedInUserName: " + ((TransporterModel.User)filterContext.HttpContext.Session["User"]).FirstName + " " + ((TransporterModel.User)filterContext.HttpContext.Session["User"]).LastName);
            }
            
            log.Error("Inner Exception:" + ex.InnerException);
            log.Error("StackTrace:" + ex.StackTrace);
            log.Error("---End---");

            filterContext.Result = new ViewResult()
            {
                ViewName = "~/Views/Shared/Error.cshtml",
                ViewData = new ViewDataDictionary(model)
            };
            filterContext.ExceptionHandled = true;
        }


        #endregion

        #region "Generic reusable functionality across controllers"

        public string SendCommunication(string MsgType, string EmailID, long toNumber, string subject, string resourceKey, int refID1, string refID2, string refID3, string refID4, string refType, int loggedInUserID)
        {
            string EmailStatus = "";
            log.Info("Send Communication status for userID " + loggedInUserID.ToString() + " Started '" + MsgType + "'");
            string retVal = TransporterCore.CommunicatorBL.SendEmail(MsgType, EmailID, subject, Message.ResourceManager.GetString(MsgType + "Email").ToString().Replace("{0}", refID1.ToString()).Replace("{1}", refID2.ToString()).Replace("{2}", refID3.ToString()).Replace("{3}", refID4.ToString()), loggedInUserID, refID1, refType, ref EmailStatus);
            log.Info("Send Communication status for userID " + loggedInUserID.ToString() + " on action '" + MsgType + "Email" + "' : " + EmailStatus);
            retVal = TransporterCore.CommunicatorBL.SendSMS(MsgType, toNumber, subject, Message.ResourceManager.GetString(MsgType + "SMS").ToString().Replace("{0}", refID1.ToString()).Replace("{1}", refID2.ToString()).Replace("{2}", refID3.ToString()).Replace("{3}", refID4.ToString()), loggedInUserID, refID1, refType);
            log.Info("Send Communication status for userID " + loggedInUserID.ToString() + " on action '" + MsgType + "SMS" + "' : " + retVal);
            return retVal;
        }

        public static TransporterModel.AjaxReturnData InitializeJSONReturnObj( int uniqueID, string defaultMsg)
        {
            TransporterModel.AjaxReturnData returnVal = new TransporterModel.AjaxReturnData();
            returnVal.UniqueId = uniqueID;
            returnVal.IsSuccess = false;
            returnVal.Message = defaultMsg;
            return returnVal;
        }

        public static void SetJSONReturnObj(int uniqueID, string defaultMsg, Boolean isSuccess, string redirectURL, ref TransporterModel.AjaxReturnData refObj)
        {
            refObj.UniqueId = uniqueID;
            refObj.IsSuccess = isSuccess;
            refObj.Message = defaultMsg;
            refObj.RedirectURL = redirectURL;
        }

        public static TransporterModel.AjaxReturnData CheckErrorInSP(string result, int UniqueID, string RedirectURL)
        {
            TransporterModel.AjaxReturnData returnVal = new TransporterModel.AjaxReturnData();
            if (result.ToUpper().Contains("ERROR"))
            {
                SetJSONReturnObj(UniqueID, result, false, "", ref returnVal);
            }
            else
            {
                SetJSONReturnObj(UniqueID, result, true, RedirectURL, ref returnVal);
            }
            return returnVal;
        }

        #endregion

        #region "Base controller private methods"
        private static string GetClientIPAddress(ActionExecutingContext filter)
        {
            return filter.HttpContext.Request.UserHostAddress;
        }

        private static string GetClientIpAddress(HttpRequestBase request)
        {
            try
            {
                var userHostAddress = request.UserHostAddress;

                // Attempt to parse.  If it fails, we catch below and return "0.0.0.0"
                // Could use TryParse instead, but I wanted to catch all exceptions
                IPAddress.Parse(userHostAddress);

                var xForwardedFor = request.ServerVariables["X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(xForwardedFor))
                    return userHostAddress;

                // Get a list of public ip addresses in the X_FORWARDED_FOR variable
                var publicForwardingIps = xForwardedFor.Split(',').Where(ip => !IsPrivateIpAddress(ip)).ToList();

                // If we found any, return the last one, otherwise return the user host address
                return publicForwardingIps.Any() ? publicForwardingIps.Last() : userHostAddress;
            }
            catch (Exception)
            {
                // Always return all zeroes for any failure (my calling code expects it)
                return "0.0.0.0";
            }
        }

        private static bool IsPrivateIpAddress(string ipAddress)
        {
            // http://en.wikipedia.org/wiki/Private_network
            // Private IP Addresses are: 
            //  24-bit block: 10.0.0.0 through 10.255.255.255
            //  20-bit block: 172.16.0.0 through 172.31.255.255
            //  16-bit block: 192.168.0.0 through 192.168.255.255
            //  Link-local addresses: 169.254.0.0 through 169.254.255.255 (http://en.wikipedia.org/wiki/Link-local_address)

            var ip = IPAddress.Parse(ipAddress);
            var octets = ip.GetAddressBytes();

            var is24BitBlock = octets[0] == 10;
            if (is24BitBlock) return true; // Return to prevent further processing

            var is20BitBlock = octets[0] == 172 && octets[1] >= 16 && octets[1] <= 31;
            if (is20BitBlock) return true; // Return to prevent further processing

            var is16BitBlock = octets[0] == 192 && octets[1] == 168;
            if (is16BitBlock) return true; // Return to prevent further processing

            var isLinkLocalAddress = octets[0] == 169 && octets[1] == 254;
            return isLinkLocalAddress;
        }

        private String GetUserEnvironment(HttpRequestBase request)
        {
            var browser = request.Browser;
            var platform = GetUserPlatform(request);
            return string.Format("{0} {1} / {2}", browser.Browser, browser.Version, platform);
        }

        private String GetUserPlatform(HttpRequestBase request)
        {
            var ua = request.UserAgent;

            if (ua.Contains("Android"))
                return string.Format("Android {0}", GetMobileVersion(ua, "Android"));

            if (ua.Contains("iPad"))
                return string.Format("iPad OS {0}", GetMobileVersion(ua, "OS"));

            if (ua.Contains("iPhone"))
                return string.Format("iPhone OS {0}", GetMobileVersion(ua, "OS"));

            if (ua.Contains("Linux") && ua.Contains("KFAPWI"))
                return "Kindle Fire";

            if (ua.Contains("RIM Tablet") || (ua.Contains("BB") && ua.Contains("Mobile")))
                return "Black Berry";

            if (ua.Contains("Windows Phone"))
                return string.Format("Windows Phone {0}", GetMobileVersion(ua, "Windows Phone"));

            if (ua.Contains("Mac OS"))
                return "Mac OS";

            if (ua.Contains("Windows NT 5.1") || ua.Contains("Windows NT 5.2"))
                return "Windows XP";

            if (ua.Contains("Windows NT 6.0"))
                return "Windows Vista";

            if (ua.Contains("Windows NT 6.1"))
                return "Windows 7";

            if (ua.Contains("Windows NT 6.2"))
                return "Windows 8";

            if (ua.Contains("Windows NT 6.3"))
                return "Windows 8.1";

            if (ua.Contains("Windows NT 10"))
                return "Windows 10";

            //fallback to basic platform:
            return request.Browser.Platform + (ua.Contains("Mobile") ? " Mobile " : "");
        }

        private String GetMobileVersion(string userAgent, string device)
        {
            var temp = userAgent.Substring(userAgent.IndexOf(device) + device.Length).TrimStart();
            var version = string.Empty;

            foreach (var character in temp)
            {
                var validCharacter = false;
                int test = 0;

                if (Int32.TryParse(character.ToString(), out test))
                {
                    version += character;
                    validCharacter = true;
                }

                if (character == '.' || character == '_')
                {
                    version += '.';
                    validCharacter = true;
                }

                if (validCharacter == false)
                    break;
            }

            return version;
        }
        #endregion

        public static string ReplaceCommContent (string inContent)
        {
            string retVal = "";

            return retVal;
        }
    }
}