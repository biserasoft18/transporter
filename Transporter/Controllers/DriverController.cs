﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransporterCore;
using TransporterModel;
using System.Data;

namespace Transporter.Controllers
{
    public class DriverController : BaseController
    {
        #region "Driver details related controllers"
        
        [HttpGet]
        public ActionResult ManageDrivers()
        {
            if (Request["returnURL"] != null)
            {
                ViewBag.ReturnURL = Request["returnURL"].ToString();
            }
            return ShowAllDriverList();
        }

        private ActionResult ShowAllDriverList()
        {
            AllDriverDetails Model = new AllDriverDetails();
            Model = GetDriverDetails(0);
            ViewBag.UserID = ((User)Session["User"]).UserID;
            ViewBag.UserName = ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName;
            return View("ManageDrivers", Model);
        }

        private AllDriverDetails GetDriverDetails(int driverID)
        {
            UserBL UserObj = new UserBL();
            AllDriverDetails Model = new AllDriverDetails();
            Model.DriversInfo = new List<DriverShell>();
            DataTable dt = new DataTable();
            dt = UserObj.GetDriverDetails(driverID, ((User)Session["User"]).UserID);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DriverShell refObj = new DriverShell();
                    refObj.DriverID = (int)dt.Rows[i]["DriverID"];
                    refObj.StateID = (int)dt.Rows[i]["State"];
                    refObj.CityID = (int)dt.Rows[i]["City"];
                    refObj.CountryID = (int)dt.Rows[i]["Country"];
                    refObj.PinCode = (int)dt.Rows[i]["PinCode"];
                    refObj.TransporterID = (int)dt.Rows[i]["TransporterID"];
                    refObj.MobileNumber = (Int64)dt.Rows[i]["MobileNumber"];

                    refObj.DriverMobileAppID = ((int)dt.Rows[i]["DriverMobileAppID"] == 1);
                    refObj.FirstName = dt.Rows[i]["FirstName"].ToString();
                    refObj.LastName = dt.Rows[i]["LastName"].ToString();
                    refObj.LicenseNumber = dt.Rows[i]["LicenseNumber"].ToString();
                    refObj.Address1 = dt.Rows[i]["Address1"].ToString();
                    refObj.Address2 = dt.Rows[i]["Address2"].ToString();
                    refObj.CityName = dt.Rows[i]["CityName"].ToString();
                    refObj.StateName = dt.Rows[i]["StateName"].ToString();
                    refObj.CountryName = dt.Rows[i]["CountryName"].ToString();
                    refObj.TransporterName = dt.Rows[i]["TransporterName"].ToString();
                    Model.DriversInfo.Add(refObj);
                }
            }
            return Model;
        }

        [HttpPost]
        public ActionResult ManageDriver(DriverDetailsPage driverData)
        {
            string result = "Failure";
            AjaxReturnData returnVal = new AjaxReturnData();
            returnVal.IsSuccess = false;
            returnVal.Message = result;
            try
            {
                DriverShell DriverObj = new DriverShell();
                if (Request["hdnDriverID"] == null)
                {
                    DriverObj.DriverID = 0;
                }
                else
                {
                    DriverObj.DriverID = Convert.ToInt32(Request["hdnDriverID"].ToString());
                }
                DriverObj.TransporterID = Convert.ToInt32(Request["DriverInfo.TransporterID"].ToString());

                DriverObj.FirstName = Request["txtFirstName"].ToString();
                DriverObj.LastName = Request["txtLastName"].ToString();
                DriverObj.Address1 = Request["txtAddress1"].ToString();
                DriverObj.Address2 = Request["txtAddress2"].ToString();
                DriverObj.LicenseNumber = Request["txtLicenseNumber"].ToString();
                DriverObj.ClearAppID = Convert.ToBoolean(Request.Form.GetValues("chkClearAppID")[0]);
                DriverObj.CountryID = Convert.ToInt32(Request["DriverInfo.CountryID"].ToString());
                DriverObj.StateID = Convert.ToInt32(Request["hdnStateID"].ToString());
                DriverObj.CityID = Convert.ToInt32(Request["hdnCityID"].ToString());
                DriverObj.PinCode = Convert.ToInt32(Request["txtPincode"].ToString());
                DriverObj.MobileNumber = Convert.ToInt64(Request["txtMobileNumber"].ToString());

                UserBL tempObj = new UserBL();
                returnVal.Message = tempObj.SetDriverDetails(DriverObj, ((User)Session["User"]).UserID);

                returnVal.IsSuccess = true;
                returnVal.RedirectURL = "/Driver/ManageDrivers";
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = "Error: " + ex.Message.ToString();
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult AddDriver()
        {
            ViewBag.CreateMode = true;
            ViewBag.UserID = ((User)Session["User"]).UserID;
            ViewBag.UserName = ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName;
            DriverShell tempObj = new DriverShell();
            tempObj.TransporterName = ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName;
            tempObj.TransporterID = ((User)Session["User"]).UserID;

            List<Country> countryObj = new List<Country>();
            PopulateMasters tempCountryObj = new PopulateMasters();
            DriverDetailsPage Model = new DriverDetailsPage();
            Model.Country = tempCountryObj.FetchCountry(0);  // 1 - India            
            Model.DriverInfo = tempObj;
            return PartialView("_ManageDriver", Model);

        }

        [HttpGet]
        public ActionResult EditDriver(int driverID)
        {

            ViewBag.CreateMode = false;
            ViewBag.UserID = ((User)Session["User"]).UserID;
            ViewBag.UserName = ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName;
            AllDriverDetails tempObj = new AllDriverDetails();
            tempObj = GetDriverDetails(driverID);
            DriverShell returnObj = new DriverShell();
            returnObj = tempObj.DriversInfo[0];
            List<Country> countryObj = new List<Country>();
            PopulateMasters tempCountryObj = new PopulateMasters();
            DriverDetailsPage Model = new DriverDetailsPage();
            Model.Country = tempCountryObj.FetchCountry(0);// 1 - India 
            Model.State = tempCountryObj.FetchState(0); // 
            Model.City = tempCountryObj.FetchCity(0);
            Model.DriverInfo = returnObj;
            return PartialView("_ManageDriver", Model);
        }

        #endregion
    }
}