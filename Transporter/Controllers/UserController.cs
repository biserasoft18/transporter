﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransporterModel;
using TransporterCore;
using GenericHelper;
using System.Data;

namespace Transporter.Controllers
{
    public class UserController : BaseController
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ShowContactInfo(int CAgentID)
        {
            int LogUserID = ((User)Session["User"]).UserID;
            ViewUserInfo retValue = new ViewUserInfo();
            if (((User)Session["User"]).IsPaidUser)
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                try
                {
                    PostingCreateBL PostingObj = new PostingCreateBL();
                    ds = PostingObj.ShowcontactMessage(CAgentID, LogUserID);
                    if(ds.Tables.Count> 0)
                    {
                        if(ds.Tables[0].Rows.Count > 0)
                        {
                            retValue.UserName = ds.Tables[0].Rows[0]["UserName"].ToString();
                            retValue.Uservalidity = ds.Tables[0].Rows[0]["Uservalidity"].ToString();
                            retValue.Address1 = ds.Tables[0].Rows[0]["Address1"].ToString();
                            retValue.Address2 = ds.Tables[0].Rows[0]["Address2"].ToString();
                            retValue.EmailID = ds.Tables[0].Rows[0]["EmailID"].ToString();
                            retValue.CountryName = ds.Tables[0].Rows[0]["CountryName"].ToString();
                            retValue.StateName = ds.Tables[0].Rows[0]["StateName"].ToString();
                            retValue.CityName = ds.Tables[0].Rows[0]["CityName"].ToString();
                            retValue.TownName = ds.Tables[0].Rows[0]["TownName"].ToString();

                            retValue.MobileNumber = Convert.ToInt64(ds.Tables[0].Rows[0]["MobileNumber"].ToString());
                            retValue.PhoneNumber = Convert.ToInt64(ds.Tables[0].Rows[0]["PhoneNumber"].ToString());
                            retValue.IsDriver = ds.Tables[0].Rows[0]["IsDriver"].ToString();
                            retValue.IsPaidUser = ds.Tables[0].Rows[0]["IsPaidUser"].ToString();
                        }
                    }
                    //List<Dictionary<string, object>> AgentDet = Common.GetTableRows(dt);
                    //return Json(AgentDet, JsonRequestBehavior.AllowGet);

                    return PartialView("_UserContactInfo", retValue);
                }
                catch
                {
                    return Json(dt, JsonRequestBehavior.AllowGet); ;
                }
            }
            return PartialView("_UserContactInfo", retValue);
        }
    }
}