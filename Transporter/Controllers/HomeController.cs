﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web.Mvc;
using TransporterCore;
using TransporterModel;
using GenericHelper;
using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Transporter.Controllers
{
    public class HomeController : BaseController
    {
        
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TestPage()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Main()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UnAuthorized()
        {
            return View();
        }

        public JsonResult SetUserGeoLogs(string GeoTags)
        {
            API.UserTrackingController UserTracking = new API.UserTrackingController();
            var retVal = UserTracking.SetUserGeoLogs(GeoTags, ((User)Session["User"]).UserID);
            return Json(retVal, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ValidateLogin(string UserName, string Password, string RedirectUrl)
        {
            string output = "";
            Session["UserReportMenu"] = null;
            Session["AdminReportMenu"] = null;

            UserBL UserLayer = new UserBL();
            List<User> UserObj = new List<User>();

            List<ReportsPageMenu> ReportMenu = new List<ReportsPageMenu>();
            
            output = UserLayer.ValidateLogin(UserName, Password, ref UserObj, ref ReportMenu);
            
            if (output.ToUpper() == "VALID")
            {
                log.Info("User " + UserName + " logging in");
                Session["User"] = UserObj[0];
                Session["AdminReportMenu"] = ReportMenu.Where(m => m.IsAdminReport == true).ToList();
                Session["UserReportMenu"] = ReportMenu.Where(m => m.IsUserReport == true).ToList();

                RedirectUrl = RedirectUrl.TrimStart('/');
                if (RedirectUrl != null && RedirectUrl != "")
                {
                    return Json(new
                    {
                        redirectUrl = "../" + RedirectUrl, //Url.Action(ActionName, ControllerName,),
                        isRedirect = true
                    });
                }
                else
                {
                    //API.UserTrackingController UserTracking = new API.UserTrackingController();
                    //UserTracking.TrackLoggedInUserDetails(UserObj[0].UserID, ViewData["ClientIP"].ToString(), ViewData["UserEnvironment"].ToString());

                    return Json(new
                    {
                        redirectUrl = Url.Action("DashBoard", "Home"),
                        isRedirect = true
                    });

                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Register()
        {
            return Json(new
            {
                redirectUrl = Url.Action("Index", "Registration"),
                isRedirect = true

            });
        }

        [HttpPost]
        public JsonResult GetForgotPassword(string username, string EmailID,string secpasw)
        {
            UserBL UserLayer = new UserBL();
            passwordrecovery UserObj = new passwordrecovery();
            DataTable dt = new DataTable();
            dt = UserLayer.GetUserPassword(username, EmailID, secpasw);

            if (dt.Rows.Count > 0)
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                UserObj.Uservalidity = dt.Rows[j]["Uservalidity"].ToString();

                if (UserObj.Uservalidity == "Valid")
                {
                    UserObj.password = dt.Rows[j]["Password"].ToString();
                    UserObj.EmailID = dt.Rows[j]["Email"].ToString();
                    UserObj.MobileNumber = Convert.ToInt64(dt.Rows[j]["MobileNumber"].ToString());
                    UserObj.UserID = Convert.ToInt32(dt.Rows[j]["UserID"].ToString());

                    try
                    {
                        string commResult = SendCommunication("ForgetPassword", UserObj.EmailID, (long)UserObj.MobileNumber, "Fetchmygoods.com Password changed", "ForgetPassword", UserObj.UserID, UserObj.password, "", "", "UserID", UserObj.UserID);
                    }
                    catch
                    {
                        UserObj.Message = UserObj.password + ". But, error in communicating to the user";
                    }
                }
            }
            return Json(UserObj, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DoLogout()
        {
            Session["User"] = null;
            Session["AdminReportMenu"] = null;
            Session["UserReportMenu"] = null;
            ViewData["IsLoggedIn"] = "false";
            return View("Main");
        }

        public ActionResult LoginExpired()
        {
            return View("Main");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Dashboard()
        {
            DashBoardBL DashboardObj = new DashBoardBL();
            DashBoardPage Model = new DashBoardPage();
            Model = DashboardObj.GetDashBoardPageDetails(((User)Session["User"]).UserID);
            Model.DashBoardDetails.AgentUserID = ((User)Session["User"]).UserID;
            ViewBag.UserID = ((User)Session["User"]).UserID;
            ViewBag.UserName = ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName;
            ViewBag.LoadAvailabilityOtherAgentsA = Model.DashBoardDetails.LoadAvailabilityOtherAgentsA;
            ViewBag.LoadAvailabilityLogedInAgentsD = Model.DashBoardDetails.LoadAvailabilityLogedInAgentsD;
            ViewBag.TruckAvailabilityOtherAgentsB = Model.DashBoardDetails.TruckAvailabilityOtherAgentsB;
            ViewBag.TruckAvailabilityLogedInAgentsC = Model.DashBoardDetails.TruckAvailabilityLogedInAgentsC;
            
            return View(Model);
        }

        [HttpPost]
        public JsonResult AgentLoadAvailability()
        {
            return Json(new
            {
                redirectUrl = Url.Action("Create", "Booking"),
                isRedirect = true

            });
        }

        [HttpPost]
        public JsonResult TransportLoadAvailability()
        {
            return Json(new
            {
                redirectUrl = Url.Action("Search", "Posting"),
                isRedirect = true

            });
        }

        [HttpPost]
        public JsonResult AgentSearch()
        {
            return Json(new
            {
                redirectUrl = Url.Action("Search", "Booking"),
                isRedirect = true

            });
        }

        [HttpPost]
        public JsonResult TransportSearch()
        {
            return Json(new
            {
                redirectUrl = Url.Action("Create", "Posting"),
                isRedirect = true

            });
        }

        [HttpPost]
        public JsonResult AcceptedTransUsers(int UserID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = new AjaxReturnData();
            returnVal.IsSuccess = false;
            returnVal.Message = result;

            DataTable dt = new DataTable();


            try
            {
                UserBL SearchObj = new UserBL();
                dt = SearchObj.GetPostingResponseFromAgent(UserID);

                List<LoadTransporterUserInbox> SearchList = new List<LoadTransporterUserInbox>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    LoadTransporterUserInbox TransporterUserInbox = new LoadTransporterUserInbox();
                    TransporterUserInbox.TransporterName = dt.Rows[i]["TransporterName"].ToString();
                    TransporterUserInbox.BookingID = Convert.ToInt32(dt.Rows[i]["BookingID"]);
                    TransporterUserInbox.PostingID = Convert.ToInt32(dt.Rows[i]["PostingID"]);
                    TransporterUserInbox.SourceCity = dt.Rows[i]["SourceCity"].ToString();
                    TransporterUserInbox.DestinationCity = dt.Rows[i]["DestinationCity"].ToString();
                    TransporterUserInbox.DeliveryDate = dt.Rows[i]["DeliveryDate"].ToString();
                    TransporterUserInbox.LinkURL = dt.Rows[i]["LinkURL"].ToString();
                    SearchList.Add(TransporterUserInbox);
                }
                returnVal.IsSuccess = true;
                returnVal.Message = "Success";


                return Json(SearchList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(result + ":" + ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }

        }

        public String CheckBookingUserType()
        {
            String sReturn = "../Booking/Create";
            try
            {
                if (((User)Session["User"])?.UserTypeID.ToString() == "1" || ((User)Session["User"])?.UserTypeID.ToString() == "3" || ((User)Session["User"])?.UserTypeID.ToString() == "4")
                {
                    sReturn = "../Booking/Create";
                }
                else
                {
                    sReturn = "javascript:modalPopup.SetText('Usertype error', '', 'You are not configured to create a load availability booking, please contact support');";
                }
                return sReturn;
            }
            catch
            {
                sReturn = "";
                return sReturn;
            }
            
            
        }

        public String CheckPostingUserType()
        {
            String sReturn = "../Posting/Create";
            try
            {
                if (((User)Session["User"])?.UserTypeID.ToString() == "2" || ((User)Session["User"])?.UserTypeID.ToString() == "3" || ((User)Session["User"])?.UserTypeID.ToString() == "4")
                {
                    sReturn = "../Posting/Create";
                }
                else
                {
                    sReturn = "javascript:modalPopup.SetText('Usertype error', '', 'You are not configured to create a truck availability posting, please contact support');";
                }
                return sReturn;
            }
            catch
            {
                return sReturn;
            }
        }

        [HttpPost]
        public JsonResult AcceptedAgentUsers(int UserID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = new AjaxReturnData();
            returnVal.IsSuccess = false;
            returnVal.Message = result;

            DataTable dt = new DataTable();


            try
            {
                UserBL SearchObj = new UserBL();
                dt = SearchObj.GetBookingResponseFromTransporter(UserID);

                List<LoadAgentUserInbox> SearchList = new List<LoadAgentUserInbox>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    LoadAgentUserInbox UserInbox = new LoadAgentUserInbox();
                    UserInbox.AgentName = dt.Rows[i]["AgentName"].ToString();
                    UserInbox.PostingID = Convert.ToInt32(dt.Rows[i]["PostingID"]);
                    UserInbox.BookingID = Convert.ToInt32(dt.Rows[i]["BookingID"]);
                    UserInbox.SourceCity = dt.Rows[i]["SourceCity"].ToString();
                    UserInbox.DestinationCity = dt.Rows[i]["DestinationCity"].ToString();
                    UserInbox.DeliveryDate = dt.Rows[i]["DeliveryDate"].ToString();
                    UserInbox.LinkURL = dt.Rows[i]["LinkURL"].ToString();
                    SearchList.Add(UserInbox);
                }
                returnVal.IsSuccess = true;
                returnVal.Message = "Success";


                return Json(SearchList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(result + ":" + ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult EditTrans(int BookingID)
        {
            ViewBag.CreateMode = false;
            return View();
        }

        [HttpGet]
        public ActionResult EditAgent(int PostingID)
        {
            ViewBag.CreateMode = false;
            return View();
        }
    }
}