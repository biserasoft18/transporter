﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransporterModel;

namespace Transporter.Admin.Controllers
{
    public class BaseAdminController : Transporter.Controllers.BaseController
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session["User"] == null)
            {
                ViewData["IsLoggedIn"] = "false";
                CheckRequest(ref filterContext, filterContext.HttpContext.Request.IsAjaxRequest());                
            }
            else
            {
                ViewData["IsLoggedIn"] = "true";
                //Commented Becacuse Reports have been splitted as Admin and User Reports
                //base.Execute(filterContext.RequestContext);
                //if (!((User)filterContext.HttpContext.Session["User"]).IsAdmin)
                //{
                //    var replaceStr = ((System.Web.HttpRequestWrapper)((System.Web.HttpContextWrapper)filterContext.HttpContext).Request).Url.PathAndQuery.ToString();
                //    filterContext.Result = RedirectToAction("UnAuthorized", "Home", new { @returnURL = replaceStr });
                //}
            }
        }

    }
}