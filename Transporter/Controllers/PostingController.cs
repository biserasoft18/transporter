﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web.Mvc;
using TransporterCore;
using TransporterModel;
using GenericHelper;
using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Resources;

namespace Transporter.Controllers
{
    public class PostingController : BaseController
    {
        // GET: Posting
        public ActionResult ViewBookingID(int bookingID)
        {
            BookingPage Model = new BookingPage();

            return PartialView("~/Views/Booking/_Create", Model);
        }

        [HttpPost]
        public ActionResult CreateTripSheet(int postingID, List<BookingSortInfo> sortData, int driverID, string truckNumber)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(postingID, result);
            try
            {
                PostingCreateBL PostingObj = new PostingCreateBL();
                
                List<TripBookingList> AgentInfo = new List<TripBookingList>();

                result = PostingObj.SetTripSheetData(postingID, sortData, driverID, truckNumber, ((User)Session["User"]).UserID, ref AgentInfo);

                SetJSONReturnObj(postingID, result, true, "/Posting/Edit?PostingID=" + postingID, ref returnVal);
                
                string commResult = SendCommunication("TripSheetCreated", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "TripSheet Created for your posting", "TripSheetCreated", postingID, "","","", "PostingID", ((User)Session["User"]).UserID);

                // Send message to all agents who are in accepted status for this postingID
                foreach(var agent in AgentInfo)
                {
                    var agentEmail = agent.AgentEmail;
                    var agentMobile = agent.AgentNumber;
                    commResult = SendCommunication("TripSheetCreatedToAgent", agentEmail, Convert.ToInt64(agentMobile), "TripSheet Created for your booking", "TripSheetCreatedToAgent", postingID, agent.BookingID.ToString(), "", "", "PostingID", ((User)Session["User"]).UserID);
                }

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult OpenTripSheet(int postingID)
        {
            ViewBag.CreateMode = true;
            return GetTripSheet(postingID);
        }

        public ActionResult ViewTripSheet(int postingID)
        {
            ViewBag.CreateMode = false;
            return GetTripSheet(postingID);
        }

        public ActionResult GetTripSheet(int postingID)
        {
            TripSheet Model = new TripSheet();
            PostingCreateBL TripObj = new PostingCreateBL();
            int loggedInUser = ((User)Session["User"]).UserID;
            Model = TripObj.GetTripData(postingID,0, loggedInUser);

            if(Model.PostingID == 0)
            {
                throw new InvalidOperationException("Page Info not found!");
                //throw new Exception("Page info not found!");
            }

            // Might need to change the CreateMode to false if the status of posting is not appropriate
            return View("TripSheet", Model);
        }

        public ActionResult Create()
        {
            ViewBag.CreateMode = true;
            return View(FetchPostingDetails(0));
        }
        
       
        private PostingPage FetchPostingDetails(int postingID)
        {            
            PostingCreateBL PostingObj = new PostingCreateBL();
            PostingPage Model = new PostingPage();
            Model = PostingObj.GetPostingPageDetails(postingID);
            
            if (ViewBag.CreateMode)
            {
                Model.PostingDetails.UserID = ((User)Session["User"]).UserID;
                ViewBag.UserID = ViewBag.LoggedInUserID = ((User)Session["User"]).UserID;
                ViewBag.UserName = ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName;
                ViewBag.IsPostingOwner = true;
                Model.PostingDetails.OfficePhone = ((User)Session["User"]).PhoneNumber;
                Model.PostingDetails.MobileNumber = ((User)Session["User"]).MobileNumber;
                Model.PostingDetails.Email = ((User)Session["User"]).Email;
                Model.PostingDetails.CreatedByName = ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName;
            }
            else
            {
                Model.PostingDetails.UserID = Model.PostingDetails.CreatedBy;
                ViewBag.UserID = Model.PostingDetails.CreatedBy;
                ViewBag.UserName = Model.PostingDetails.CreatedByName;
                ViewBag.IsPostingOwner = Model.PostingDetails.CreatedBy == ((User)Session["User"]).UserID;
                ViewBag.LoggedInUserID = ((User)Session["User"]).UserID;
                int countbookingID = 0;
                
                if (!ViewBag.IsPostingOwner)
                {
                    //ViewBag.MessageToTransporter = PostingObj.GetMessageToTransporter(postingID, ((User)Session["User"]).UserID, ref linkedBookingId);
                    DataTable dt = new DataTable();
                    dt = PostingObj.GetMessageToTransporter(postingID, ((User)Session["User"]).UserID, ref countbookingID);
                    List<ExpressInterest> UserObj = new List<ExpressInterest>();
                    
                    if (dt.Rows.Count > 0)
                        
                    for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            ExpressInterest ExpressInt = new ExpressInterest();
                            ExpressInt.BookingID = Convert.ToInt32(dt.Rows[j]["BookingID"].ToString());
                            ExpressInt.SourceCity = dt.Rows[j]["SourceCity"].ToString();
                            ExpressInt.DestinationCity = dt.Rows[j]["DestinationCity"].ToString();
                            ExpressInt.TruckTypeName = dt.Rows[j]["TruckType"].ToString();
                            ExpressInt.GoodsWeightName = dt.Rows[j]["GoodsWeight"].ToString();
                            UserObj.Add(ExpressInt);
                        }
                    Model.AllExpressInterest = UserObj;                   
                }
                else
                {
                    Model.AllPostingMessage = PostingObj.GetAllMessageToTransporter(postingID, ((User)Session["User"]).UserID);
                    Model.AllMessageToAgent = PostingObj.GetAllMessageToAgent(postingID, ((User)Session["User"]).UserID);
                }

                if (countbookingID > 0)
                {   
                    ViewBag.AssociatedBookingIDMessage = "You have already expressed interest with below bookingID";
                }
                else
                {
                    ViewBag.AssociatedBookingIDMessage = "You have not yet expressed any interest on this postingID";
                }
            }
            
            ViewBag.CurDateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm");
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;
            var ddlPostingTruckType = (
                from result in Model.TruckType
                select new SelectListItem()
                {
                    Text = result.TruckTypeName,
                    Value = result.TruckTypeID.ToString(),
                    Selected = result.TruckTypeID == Model.PostingDetails.TruckTypeID
                }).ToList();
            ViewBag.ddlPostingTruckType = ddlPostingTruckType;

            var ddlPostingGoodsWeight = (
                from result in Model.GoodsWeight
                select new SelectListItem()
                {
                    Text = result.GoodsWeightName,
                    Value = result.GoodsWeightID.ToString(),
                    Selected = result.GoodsWeightID == Model.PostingDetails.GoodsWeightID
                }).ToList();
            ViewBag.ddlPostingGoodsWeight = ddlPostingGoodsWeight;

            var ddlPostingIsCargoInsured = (
                from result in Model.IsCargoInsured
                select new SelectListItem()
                {
                    Text = result.DisplayName,
                    Value = result.Id.ToString(),
                    Selected = result.Id == (Model.PostingDetails.IsCargoInsured ==true ? 1 : 0)
                }).ToList();
            ViewBag.ddlPostingIsCargoInsured = ddlPostingIsCargoInsured;

            return Model;
        }

        [HttpGet]
        public ActionResult Edit(int PostingId)
        {
            ViewBag.CreateMode = false;
            return View("Create", FetchPostingDetails(PostingId));
        }

        [HttpGet]
        public ActionResult ViewPostingPopup(int postingId)
        {
            ViewBag.CreateMode = false;
            ViewBag.ShowButtons = false;
            return PartialView("_Create", FetchPostingDetails(postingId));
        }
        [HttpPost]
        public JsonResult CreatePosting(LoadPosting UIData, List<City> ViaCitiesData)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(0, result);

            try
            {
                if (!(((User)Session["User"]).UserTypeID.ToString() == "2" || ((User)Session["User"]).UserTypeID.ToString() == "3" || ((User)Session["User"]).UserTypeID.ToString() == "4"))
                {
                    returnVal.Message = "You are not configured for posting a truck availability, please contact support";
                    return Json(returnVal, JsonRequestBehavior.AllowGet);
                }
                PostingCreateBL PostingObj = new PostingCreateBL();
                UIData.UserID = ((User)Session["User"]).UserID;
                UIData.PostingStatus = Constants.TransporterPostingStatus.PostingCreated;
                int postingID = UIData.PostingID;
                if (ViaCitiesData == null)
                {
                    ViaCitiesData = new List<City>();
                }
                result = PostingObj.SetPostingData(UIData, ViaCitiesData, ref postingID, ((User)Session["User"]).UserID, Constants.TransporterPostingStatus.PostingCreated);

                if(postingID != 0)
                {
                    SetJSONReturnObj(postingID, result, true, "", ref returnVal);
                }
                else{
                    SetJSONReturnObj(postingID, result, false, "", ref returnVal);
                }
                
                if (Request.QueryString["returnURL"] != "")
                {
                    returnVal.RedirectURL = Request.QueryString["returnURL"].ToString();
                }
                else
                {
                    returnVal.RedirectURL = "/Posting/Search";
                }

                if (returnVal.IsSuccess)
                {
                    //string MessageToSend = "Your truck availability posting is successfully posted in www.fetchmygoods.com. Posting ID : " + postingID.ToString() + ", We will keep you informed of any response from Agents with load availability";

                    //string MessageToSend = Message.ResourceManager.GetString("PostingCreatedEmail").ToString().Replace("{0}", postingID.ToString());
                    string commResult = SendCommunication("PostingCreated", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "Posting created", "PostingCreated", postingID, UIData.SourceCityName.ToString(), UIData.DestinationCityName.ToString(), DateTime.Now.ToString("dd-MMM-yyyy"), "PostingID", ((User)Session["User"]).UserID);
                }
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }
        
        #region "Search Page methods"

        public ActionResult Search()
        {
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;
            return View(Search(0, false));
        }

        private TransportSearchPage Search(int userID, Boolean needAllSearchData)
        {
            PostingCreateBL TransSearchObj = new PostingCreateBL();
            TransportSearchPage Model = new TransportSearchPage();
            Model = TransSearchObj.GetTransportSearchDetails(userID, needAllSearchData);
            return Model;
        }

        [HttpGet]
        public ActionResult SearchPostingByUserID()
        {
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;
            return View("Search", Search(((User)Session["User"]).UserID, false));
        }

        private ActionResult SearchAction(LoadTransportSearch data, int LoggedInUserID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(0, result);

            DataTable dt = new DataTable();
            int SourceCityID = data.SourceCityID;
            int DestinationCityID = data.DestinationCityID;
            DateTime DeliveryDate = data.DeliveryDate;
            int GoodsWeightID = data.GoodsWeightID;
            decimal AvlCapacity = data.AvailableCapacity;
            int TruckTypeID = data.TruckTypeID;
            int PostingStatusID = data.PostingStatusID;
            Boolean OnlyMyPosting = data.MyPostingsOnly;

            int PostingID = data.PostingID;
            //int LoggedInUserID = 0;
            if (OnlyMyPosting)
            {
                LoggedInUserID = ((User)Session["User"]).UserID;
            }

            if (DeliveryDate == DateTime.MinValue)
            {
                DeliveryDate = DateTime.ParseExact("01/01/1900", "MM/dd/yyyy", CultureInfo.InvariantCulture);
            }

            try
            {
                PostingCreateBL searchobj = new PostingCreateBL();
                dt = searchobj.SearchforTransport(SourceCityID, DestinationCityID, DeliveryDate, GoodsWeightID, TruckTypeID, LoggedInUserID, AvlCapacity, PostingStatusID, PostingID);

                List<LoadTransporterSearchDetail> SearchList = new List<LoadTransporterSearchDetail>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    LoadTransporterSearchDetail LoadSearchAgentDet = new LoadTransporterSearchDetail();
                    LoadSearchAgentDet.PostingID = Convert.ToInt32(dt.Rows[i]["PostingID"]);
                    LoadSearchAgentDet.SourceCity = dt.Rows[i]["SourceCity"].ToString();
                    LoadSearchAgentDet.DestinationCity = dt.Rows[i]["DestinationCity"].ToString();
                    LoadSearchAgentDet.DeliveryDate = dt.Rows[i]["DeliveryDate"].ToString();
                    LoadSearchAgentDet.GoodsWeightName = dt.Rows[i]["GoodsWeightName"].ToString();
                    LoadSearchAgentDet.TruckTypeName = dt.Rows[i]["TruckTypeName"].ToString();
                    LoadSearchAgentDet.AvailableTonnage = (decimal)dt.Rows[i]["AvailableTonnage"];
                    LoadSearchAgentDet.PostingStatus = dt.Rows[i]["PostingStatus"].ToString();
                    SearchList.Add(LoadSearchAgentDet);
                }
                returnVal.IsSuccess = true;
                returnVal.Message = "Success";
                
                return Json(SearchList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(result + ":" + ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SearchTransportBooking(LoadTransportSearch data)
        {
            return SearchAction(data, 0);
        }

        [HttpPost]
        public ActionResult SearchPostingPage()
        {
            return Json(new
            {
                redirectUrl = Url.Action("Search", "Posting"),
                isRedirect = true
            });
        }

        [HttpGet]
        public ActionResult SearchAllPosting()
        {
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;
            return View("Search", Search(0, true));
        }

        #endregion

        [HttpPost]
        public ActionResult BackToHomePage()
        {
            return Json(new
            {
                redirectUrl = Url.Action("DashBoard", "Home"),
                isRedirect = true
            });
        }

        [HttpGet]
        public JsonResult ClosePosting(int postingID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(postingID, result);
            try
            {
                PostingCreateBL PostingObj = new PostingCreateBL();
                result = PostingObj.ClosePosting(postingID, ((User)Session["User"]).UserID);

                SetJSONReturnObj(postingID, result, true, "/Posting/Edit?PostingId=" + postingID, ref returnVal);

                string commResult = SendCommunication("PostingClosed", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "Posting closed", "PostingClosed", postingID, "", "", "", "PostingID", ((User)Session["User"]).UserID);

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SendMessage(string messageToTransporter, int postingID, int bookingID, string transporterName)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(postingID, result);
            try
            {
                PostingCreateBL PostingObj = new PostingCreateBL();
                string transporterEmail = "";
                long transporterMobileNumber = 0;
                result = PostingObj.SetPostingMessage(messageToTransporter, postingID, bookingID, ((User)Session["User"]).UserID, ref transporterEmail, ref transporterMobileNumber);

                returnVal = CheckErrorInSP(result, bookingID, "/Posting/Edit?PostingId=" + postingID.ToString());

                if (returnVal.IsSuccess)
                {
                    string commResult = SendCommunication("PostingExpressInterest", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "Expressed Interest on truck availability posting", "PostingExpressInterest", postingID, bookingID.ToString(), transporterName, "", "PostingID", ((User)Session["User"]).UserID);

                    // Send message to transporter emailID also here
                    commResult = SendCommunication("PostingReceiveInterest", transporterEmail, transporterMobileNumber, "Interest received on your posting", "PostingReceiveInterest", postingID, bookingID.ToString(), transporterName, "", "PostingID", ((User)Session["User"]).UserID);
                }

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult AcceptDeal(int agentID, int bookingID, int postingID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(postingID, result);
            try
            {
                PostingCreateBL PostingObj = new PostingCreateBL();
                string agentEmail = "";
                long agentMobileNumber = 0;
                result = PostingObj.AcceptDeal(agentID, bookingID, postingID, ((User)Session["User"]).UserID, ref agentEmail, ref agentMobileNumber);

                returnVal = CheckErrorInSP(result, bookingID, "/Posting/Edit?PostingId=" + postingID.ToString());
                
                if (returnVal.IsSuccess) {
                    string commResult = SendCommunication("TransporterAcceptDeal", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "Transporter Accept Deal", "TransporterAcceptDeal", postingID, bookingID.ToString(),"","", "PostingID", ((User)Session["User"]).UserID);

                    // Send message to booking agent here that his earlier interest is accepted
                    commResult = SendCommunication("TransporterAcceptDealToAgent", agentEmail, agentMobileNumber, "Transporter Accepted Deal to pickup your goods", "TransporterAcceptDealToAgent", postingID, bookingID.ToString(), ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName, "", "PostingID", ((User)Session["User"]).UserID);
                }
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public JsonResult CancelPosting(int postingID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(postingID, result);
            try
            {
                PostingCreateBL postingObj = new PostingCreateBL();
                result = postingObj.CancelPosting(postingID, ((User)Session["User"]).UserID);

                SetJSONReturnObj(postingID, result, true, "/posting/Search", ref returnVal);
                
                string commResult = SendCommunication("PostingCancelled", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "Your posting is cancelled now", "PostingCancelled", postingID, "", "", "", "PostingID", ((User)Session["User"]).UserID);

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }
    }
}