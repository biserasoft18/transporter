﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransporterCore;
using TransporterModel;
using GenericHelper;
using System.Data;
using System.Globalization;
using System.Threading.Tasks;

namespace Transporter.Controllers
{
    public class BookingController : BaseController
    {
        // GET: Booking        
        public ActionResult ViewPostingID(int PostingID)
        {
            PostingPage Model = new PostingPage();
            return PartialView("~/Views/Posting/_Create", Model);
        }
        
        public ActionResult Create()
        {
            ViewBag.CreateMode = true;
            return View(FetchBookingDetails(0));
        }

        private BookingPage FetchBookingDetails(int bookingID)
        {
            BookingCreateBL BookingObj = new BookingCreateBL();
            BookingPage Model = new BookingPage();
            Model = BookingObj.GetBookingPageDetails(bookingID);
            if (ViewBag.CreateMode)
            {
                Model.BookingDetails.UserID = ((User)Session["User"]).UserID;
                ViewBag.UserID = ((User)Session["User"]).UserID;
                ViewBag.UserName = ((User)Session["User"]).FirstName + " "  + ((User)Session["User"]).LastName;
                ViewBag.IsbookingOwner = true;
                Model.BookingDetails.ContactPersonPhoneNumber = ((User)Session["User"]).PhoneNumber;
                Model.BookingDetails.ContactPersonMobileNumber = ((User)Session["User"]).MobileNumber;
                Model.BookingDetails.ContactPersonEmail = ((User)Session["User"]).Email;
                Model.BookingDetails.ContactPersonName = ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName;
                Model.AllAddress = new BookingAddress();
                Model.AllAddress.PickupAddress = new Address();
                Model.AllAddress.DropAddress = new Address();
            }
            else
            {
                Model.BookingDetails.UserID = Model.BookingDetails.CreatedBy;
                ViewBag.UserID = ((User)Session["User"]).UserID; // Model.BookingDetails.CreatedBy;
                ViewBag.UserName = Model.BookingDetails.CreatedByName;
                ViewBag.IsBookingOwner = Model.BookingDetails.CreatedBy == ((User)Session["User"]).UserID;
                int linkedPostingId = 0;

                if (!ViewBag.IsBookingOwner)
                {
                    ViewBag.MessageToTransporter = BookingObj.GetMessageToAgent(bookingID, ((User)Session["User"]).UserID, ref linkedPostingId);
                }
                else
                {
                    Model.AllbookingMessage = BookingObj.GetAllMessageToAgent(bookingID, ((User)Session["User"]).UserID, false);
                    Model.AllPostingRequest = BookingObj.GetAllMessageToAgent(bookingID, ((User)Session["User"]).UserID, true);
                }
                if (linkedPostingId > 0)
                {
                    ViewBag.AssociatedBookingIDMessage = "You have already expressed interest with a postingID: <a href='#' onclick='ShowPostingInfo(" + linkedPostingId.ToString() + ")' >" + linkedPostingId.ToString() + "</a>";
                }
                else
                {
                    ViewBag.AssociatedBookingIDMessage = "You have not yet expressed any interest on this postingID";
                }
            }


            //Model.BookingDetails.AgentUserID = ((User)Session["User"]).UserID;
            //ViewBag.UserID = ((User)Session["User"]).UserID;
            //ViewBag.UserName = ((User)Session["User"]).FirstName;
            ViewBag.CurDateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm");
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;

            var ddlBookingTruckType = (
                from result in Model.TruckType
                select new SelectListItem()
                {
                    Text = result.TruckTypeName,
                    Value = result.TruckTypeID.ToString(),
                    Selected = result.TruckTypeID == Model.BookingDetails.TruckTypeID
                }).ToList();
            
            ViewBag.ddlBookingTruckType = ddlBookingTruckType;

            var ddlBookingGoodsWeight = (
                from result in Model.GoodsWeight
                select new SelectListItem()
                {
                    Text = result.GoodsWeightName,
                    Value = result.GoodsWeightID.ToString(),
                    Selected = result.GoodsWeightID == Model.BookingDetails.GoodsWeightID
                }).ToList();
            ViewBag.ddlBookingGoodsWeight = ddlBookingGoodsWeight;

            var ddlBookingGoodsType = (
                from result in Model.GoodsType
                select new SelectListItem()
                {
                    Text = result.GoodsTypeName,
                    Value = result.GoodsTypeID.ToString(),
                    Selected = result.GoodsTypeID == Model.BookingDetails.GoodsTypeID
                }).ToList();
            ViewBag.ddlBookingGoodsType = ddlBookingGoodsType;

            var ddlBookingPackageType = (
                from result in Model.PackageType
                select new SelectListItem()
                {
                    Text = result.PackageTypeName,
                    Value = result.PackageTypeID.ToString(),
                    Selected = result.PackageTypeID == Model.BookingDetails.PackageTypeID
                }).ToList();
            ViewBag.ddlBookingPackageType = ddlBookingPackageType;

            var ddlBookingPaymentType = (
                from result in Model.PaymentType
                select new SelectListItem()
                {
                    Text = result.PaymentTypeName,
                    Value = result.PaymentTypeID.ToString(),
                    Selected = result.PaymentTypeID == Model.BookingDetails.PaymentTypeID
                }).ToList();
            ViewBag.ddlBookingPaymentType = ddlBookingPaymentType;

            return Model;
        }

        [HttpGet]
        public ActionResult Edit(int bookingId)
        {
            ViewBag.CreateMode = false;
            return View("Create", FetchBookingDetails(bookingId));
        }

        [HttpGet]
        public ActionResult ViewBookingPopup(int bookingId)
        {
            ViewBag.CreateMode = false;
            ViewBag.ShowButtons = false;
            return PartialView("_Create", FetchBookingDetails(bookingId));
        }

        [HttpPost]
        public JsonResult CreateBooking(LoadBooking UIData, BookingAddress AddressData)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(UIData.BookingID, result);
            try
            {
                if (!(((User)Session["User"]).UserTypeID.ToString() == "1" || ((User)Session["User"]).UserTypeID.ToString() == "3" || ((User)Session["User"]).UserTypeID.ToString() == "4"))
                {
                    returnVal.Message = "You are not configured for creating a load availability, please contact support";
                    return Json(returnVal, JsonRequestBehavior.AllowGet);
                }
                BookingCreateBL BookingObj = new BookingCreateBL();
                UIData.UserID = ((User)Session["User"]).UserID;
                UIData.BookingStatus = Constants.AgentBookingStatus.Created;
                int bookingID = UIData.BookingID;

                result = BookingObj.SetBookingData(UIData, AddressData, ref bookingID, ((User)Session["User"]).UserID);

                if (bookingID != 0)
                {
                    SetJSONReturnObj(bookingID, result, true, "", ref returnVal);
                }
                else
                {
                    SetJSONReturnObj(bookingID, result, false, "", ref returnVal);
                }
                returnVal.UniqueId = bookingID;
                returnVal.IsSuccess = true;
                returnVal.Message = result;
                if (Request.QueryString["returnURL"] != "")
                {
                    returnVal.RedirectURL = Request.QueryString["returnURL"].ToString();
                }
                else
                {
                    returnVal.RedirectURL = "/Booking/Search";
                }

                if (returnVal.IsSuccess)
                {
                    string commResult = SendCommunication("BookingCreated", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "Load Booking created", "BookingCreated", bookingID, UIData.SourceCityName.ToString(), UIData.DestinationCityName.ToString(), DateTime.Now.ToString("dd-MMM-yyyy"), "BookingID", ((User)Session["User"]).UserID);
                }
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ":" + ex.Message.ToString();
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Search()
        {
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;
            return View(SearchPage(0, false));
        }

        private AgentSearchPage SearchPage(int userID, Boolean needSearchData)
        {
            BookingCreateBL SearchObj = new BookingCreateBL();
            AgentSearchPage Model = new AgentSearchPage();
            Model = SearchObj.GetAgentSearchDetails(userID, needSearchData);
            return Model;
        }

        [HttpGet]
        public ActionResult SearchBookingByUserID()
        {
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;
            return View("Search", SearchPage(((User)Session["User"]).UserID, false));
        }

        [HttpGet]
        public ActionResult SearchAllBooking()
        {
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;
            return View("Search", SearchPage(0, true));
        }

        private List<LoadSearchAgentDet> SearchPageWithDetails(LoadSearchPage data)
        {
            DataTable dt = new DataTable();
            List<LoadSearchAgentDet> SearchList = new List<LoadSearchAgentDet>();
            try
            {
                int SourceCityID = data.SourceCityID;
                int DestinationCityID = data.DestinationCityID;
                DateTime DeliveryDate = data.DeliveryDate;
                int GoodsWeightID = data.GoodsWeightID;
                int TruckTypeID = data.TruckTypeID;
                int StatusID = data.BookingStatusID;
                Boolean OnlyMyBookings = data.MyBookingsOnly;
                int BookingID = data.BookingID;
                int LoggedInUserID = 0;
                if (OnlyMyBookings)
                {
                    LoggedInUserID = ((User)Session["User"]).UserID;
                }

                BookingCreateBL SearchObj = new BookingCreateBL();
                if (DeliveryDate == DateTime.MinValue)
                {
                    DeliveryDate = DateTime.ParseExact("01/01/1900", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                }
                dt = SearchObj.SearchforAgents(SourceCityID, DestinationCityID, DeliveryDate, GoodsWeightID, TruckTypeID, StatusID, LoggedInUserID, BookingID);
                
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    LoadSearchAgentDet LoadSearchAgentDet = new LoadSearchAgentDet();
                    LoadSearchAgentDet.BookingID = Convert.ToInt32(dt.Rows[i]["BookingID"]);
                    LoadSearchAgentDet.SourceCity = dt.Rows[i]["SourceCityName"].ToString();
                    LoadSearchAgentDet.DestinationCity = dt.Rows[i]["DestinationCityName"].ToString();
                    LoadSearchAgentDet.DeliveryDate = dt.Rows[i]["DeliveryDate"].ToString();
                    LoadSearchAgentDet.GoodsWeightName = dt.Rows[i]["GoodsWeightName"].ToString();
                    LoadSearchAgentDet.TruckTypeName = dt.Rows[i]["TruckTypeName"].ToString();
                    LoadSearchAgentDet.BookingStatus = dt.Rows[i]["BookingStatus"].ToString();
                    SearchList.Add(LoadSearchAgentDet);
                }

                return SearchList;
            }
            catch
            {
                throw;
            }
        }
        
        [HttpPost]
        public ActionResult SearchAgentBooking(LoadSearchPage data)
        
        {
            string result = "Failure";

            List<LoadSearchAgentDet> SearchList = new List<LoadSearchAgentDet>();
            
            try
            {
                SearchList = SearchPageWithDetails(data);

                return Json(SearchList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(result + ":" + ex.Message.ToString());
            }


        }

        private ActionResult SearchAction(LoadSearchPage data, int LoggedInUserID)
        {
            string result = "Failure";
            DataTable dt = new DataTable();
            int SourceCityID = data.SourceCityID;
            int DestinationCityID = data.DestinationCityID;
            DateTime DeliveryDate = data.DeliveryDate;
            int GoodsWeightID = data.GoodsWeightID;
            int TruckTypeID = data.TruckTypeID;
            int StatusID = data.BookingStatusID;
            int BookingID = data.BookingID;
            Boolean OnlyMyBookings = data.MyBookingsOnly;
            int UserID = 0;
            if (OnlyMyBookings)
            {
                UserID = ((User)Session["User"]).UserID;
            }
            try
            {
                BookingCreateBL SearchObj = new BookingCreateBL();
                if (DeliveryDate == DateTime.MinValue)
                {
                    DeliveryDate = DateTime.ParseExact("01/01/1900", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                }
                dt = SearchObj.SearchforAgents(SourceCityID, DestinationCityID, DeliveryDate, GoodsWeightID, TruckTypeID, StatusID, UserID, BookingID);
                List<LoadSearchAgentDet> SearchList = new List<LoadSearchAgentDet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    LoadSearchAgentDet LoadSearchAgentDet = new LoadSearchAgentDet();
                    LoadSearchAgentDet.BookingID = Convert.ToInt32(dt.Rows[i]["BookingID"]);
                    LoadSearchAgentDet.SourceCity = dt.Rows[i]["SourceCity"].ToString();
                    LoadSearchAgentDet.DestinationCity = dt.Rows[i]["DestinationCity"].ToString();
                    LoadSearchAgentDet.DeliveryDate = dt.Rows[i]["DeliveryDate"].ToString();
                    LoadSearchAgentDet.GoodsWeightName = dt.Rows[i]["GoodsWeightName"].ToString();
                    LoadSearchAgentDet.TruckTypeName = dt.Rows[i]["TruckTypeName"].ToString();
                    SearchList.Add(LoadSearchAgentDet);
                }

                return Json(SearchList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(result + ":" + ex.Message.ToString());
            }
        }

        [HttpPost]
        public ActionResult SearchBookingPage()
        {
            return Json(new
            {
                redirectUrl = Url.Action("Search", "Booking"),
                isRedirect = true

            });
        }

        [HttpPost]
        public ActionResult BackToHomePage()
        {
            return Json(new
            {
                redirectUrl = Url.Action("DashBoard", "Home"),
                isRedirect = true

            });
        }

        [HttpPost]
        public JsonResult SendMessage(string messageToAgent, int bookingID, int PostingID, string agentName)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(bookingID, result);
            try
            {
                BookingCreateBL bookingObj = new BookingCreateBL();
                string agentEmail = "";
                long agentMobileNumber = 0;

                result = bookingObj.SetbookingMessage(messageToAgent, bookingID, PostingID, ((User)Session["User"]).UserID, ref agentEmail, ref agentMobileNumber);

                returnVal = CheckErrorInSP(result, bookingID, "/booking/Edit?bookingId=" + bookingID.ToString());
                
                if (returnVal.IsSuccess)
                {
                    // This is to inform the transporter who is now viewing the booking that his expressInterest is communicated to agent 
                    string commResult = SendCommunication("BookingExpressInterest", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "You Interest on load availability booking is communicated to agent", "BookingExpressInterest", bookingID, PostingID.ToString(), agentName, "", "BookingID", ((User)Session["User"]).UserID);

                    // Send message to agent userID/email informnig him of this interest received
                    commResult = SendCommunication("BookingReceiveInterest", agentEmail, agentMobileNumber, "Interest received on your load availability booking", "BookingReceiveInterest", bookingID, PostingID.ToString(), ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName, "", "BookingID", ((User)Session["User"]).UserID);
                }
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AcceptDeal(int TransporterID, int bookingID, int PostingID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(bookingID, result);
            try
            {
                BookingCreateBL bookingObj = new BookingCreateBL();
                string transporterEmail = "";
                long transporterMobileNumber = 0;
                result = bookingObj.AcceptDeal(TransporterID, bookingID, PostingID, ((User)Session["User"]).UserID, ref transporterEmail, ref transporterMobileNumber);

                returnVal = CheckErrorInSP(result, bookingID, "/booking/Edit?bookingId=" + bookingID.ToString());

                if (returnVal.IsSuccess)
                {
                    string commResult = SendCommunication("AgentAcceptBookingDeal", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "Deal accepted on your load availability", "AgentAcceptBookingDeal", bookingID, PostingID.ToString(),"","", "BookingID", ((User)Session["User"]).UserID);

                    // Send message to transporter that his earlier expressInterest is now accepted by agent
                    commResult = SendCommunication("AgentAcceptDealToTransporter", transporterEmail, transporterMobileNumber, "Agent has accepted your interest to pickup goods request", "AgentAcceptDealToTransporter", bookingID, PostingID.ToString(), ((User)Session["User"]).FirstName + " " + ((User)Session["User"]).LastName, "", "BookingID", ((User)Session["User"]).UserID);
                }                

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }

        [Obsolete]
        public List<Dictionary<string, object>> GetTableRows(DataTable dtData)
        {
            List<Dictionary<string, object>>
            lstRows = new List<Dictionary<string, object>>();
            Dictionary<string, object> dictRow = null;

            foreach (DataRow dr in dtData.Rows)
            {
                dictRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtData.Columns)
                {
                    dictRow.Add(col.ColumnName, dr[col]);
                }
                lstRows.Add(dictRow);
            }
            return lstRows;
        }

        [HttpPost]
        public JsonResult CancelBooking(int bookingID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = InitializeJSONReturnObj(bookingID, result);
            try
            {
                BookingCreateBL bookingObj = new BookingCreateBL();
                result = bookingObj.CancelBooking(bookingID, ((User)Session["User"]).UserID);

                SetJSONReturnObj(bookingID, result, true, "/booking/Search", ref returnVal);
                
                string commResult = SendCommunication("BookingCancelled", ((User)Session["User"]).Email, ((User)Session["User"]).MobileNumber, "You have cancelled your booking", "BookingCancelled", bookingID, "", "", "", "BookingID", ((User)Session["User"]).UserID);

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }
    }    
}