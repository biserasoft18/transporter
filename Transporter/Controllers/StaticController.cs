﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Transporter.Controllers
{
    public class StaticController : Controller
    {
        public ActionResult Disclaimer()
        {
            return PartialView("_Disclaimer");
        }
        public ActionResult FAQ()
        {
            return PartialView("_FAQ");
        }

        public ActionResult Support()
        {
            return PartialView("_Support");
        }
        public ActionResult PrivacyPolicy()
        {
            return PartialView("_PrivacyPolicy");
        }

        public ActionResult AddressAutoComplete(string AddressType)
        {
            return PartialView("_AddressAutoComplete",(object)AddressType);
        }
    }
}