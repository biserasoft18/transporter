﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransporterCore;
using TransporterModel;

namespace Transporter.Controllers
{
    public class TrackingController : BaseController
    {
        // GET: Tracking
        public ActionResult Index()
        {
            ViewBag.TrackingStatus = "100"; // 100- trip yet to start, 200-tripstarted, 300-load pickedup, 400-load intransit, 500-load dropped at dest, 600-tripcompleted
            ViewBag.TrackingMessage = "Trip sheet created but trip yet to start";

            ViewBag.CreateMode = false;

            int postingID = 0;
            int bookingID = 0;

            // Send loggedinUserID as SP param, so as to check if the logged in user is in any way associated to the trip
            if (Request["BookingID"] != null)
            {
                bookingID = Convert.ToInt32(Request["BookingID"].ToString());
            }

            if (Request["PostingID"] != null)
            {
                postingID = Convert.ToInt32(Request["PostingID"].ToString());
            }

            if(bookingID ==0 && postingID == 0)
            {
                return View();
            }

            TripSheet Model = new TripSheet();
            PostingCreateBL TripObj = new PostingCreateBL();
            int loggedInUser = ((User)Session["User"]).UserID;
            Model = TripObj.GetTripData(postingID, bookingID, loggedInUser);

            return View(Model);
        }

        public JsonResult GetTripGeoLocData(int postingID)
        {
            string result = "Failure";
            AjaxReturnData returnVal = new AjaxReturnData();
            returnVal.UniqueId = postingID;
            returnVal.IsSuccess = false;
            returnVal.Message = result;
            try
            {
                PostingCreateBL TripObj = new PostingCreateBL();
                TripTracking retObj = new TripTracking();
                retObj = TripObj.GetTripGeoLocData(postingID, ((User)Session["User"]).UserID);

                returnVal.UniqueId = postingID;
                returnVal.IsSuccess = true;
                returnVal.Message = Json(retObj).ToString();

                return Json(retObj, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                returnVal.Message = returnVal.Message + ex.Message;
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }
    }
}