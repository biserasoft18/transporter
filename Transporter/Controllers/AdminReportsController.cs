﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransporterModel;
using TransporterCore;
using GenericHelper;

namespace Transporter.Admin.Controllers
{
    public class AdminReportsController : BaseAdminController
    {
        // GET: AdminReports
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetUserRegistReport()
        {
            return View();
        }
        public ActionResult GetLoginTracking()
        {
            return GetGenericReportDetails("UserLogin report");
        }

        public ActionResult GetAReport(string reportName)
        {
            return GetGenericReportDetails(reportName);
        }

        private ActionResult GetGenericReportDetails(string reportName)
        {
           ReportSchema rpt = GetReportObj(reportName);
            FillDropDownsInViewBag(rpt);
            ViewBag.ReportName = reportName;
            ViewBag.DropDownDefault = Constants.PageTexts.DropdownDefaultSelect;

            return View("GenericReport", rpt);
        }

        private void FillDropDownsInViewBag(ReportSchema rpt)
        {
            PopulateMasters DDLMaster = new PopulateMasters();
            foreach(InputField inFld in rpt.InFields)
            {
                if(inFld.ValueType.ToUpper() == "DROPDOWN")
                {
                    if(inFld.ValueName.ToUpper() == "USERTYPE")
                    {
                        var ddlUserType = (
                            from result in DDLMaster.FetchUserType(0)
                            select new SelectListItem()
                            {
                                Text = result.UserTypeName,
                                Value = result.UserTypeID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlUserType.Add(new SelectListItem() {Text ="Select All", Value="" });
                        ViewBag.ddlUserType = ddlUserType;
                    }
                    if (inFld.ValueName.ToUpper() == "GOODSTYPE")
                    {
                        var ddlGoodsType = (
                            from result in DDLMaster.FetchGoodsType(0)
                            select new SelectListItem()
                            {
                                Text = result.GoodsTypeName,
                                Value = result.GoodsTypeID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlGoodsType.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlGoodsType = ddlGoodsType;
                    }
                    if (inFld.ValueName.ToUpper() == "GOODSWEIGHT")
                    {
                        var ddlGoodsWeight = (
                            from result in DDLMaster.FetchGoodsWeight(0)
                            select new SelectListItem()
                            {
                                Text = result.GoodsWeightName,
                                Value = result.GoodsWeightID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlGoodsWeight.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlGoodsWeight = ddlGoodsWeight;
                    }
                    if (inFld.ValueName.ToUpper() == "PACKAGETYPE")
                    {
                        var ddlPackageType = (
                            from result in DDLMaster.FetchPackageType(0)
                            select new SelectListItem()
                            {
                                Text = result.PackageTypeName,
                                Value = result.PackageTypeID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlPackageType.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlPackageType = ddlPackageType;
                    }
                    if (inFld.ValueName.ToUpper() == "TRUCKTYPE")
                    {
                        var ddlTruckType = (
                            from result in DDLMaster.FetchTruckType(0)
                            select new SelectListItem()
                            {
                                Text = result.TruckTypeName,
                                Value = result.TruckTypeID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlTruckType.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlTruckType = ddlTruckType;
                    }
                    if (inFld.ValueName.ToUpper() == "PAYMENTTYPE")
                    {
                        var ddlPaymentType = (
                            from result in DDLMaster.FetchPaymentType(0)
                            select new SelectListItem()
                            {
                                Text = result.PaymentTypeName,
                                Value = result.PaymentTypeID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlPaymentType.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlPaymentType = ddlPaymentType;
                    }
                    if (inFld.ValueName.ToUpper() == "CITYNAME")
                    {
                        var ddlCityName = (
                            from result in DDLMaster.FetchCity(0)
                            select new SelectListItem()
                            {
                                Text = result.CityName,
                                Value = result.CityID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlCityName.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlCityName = ddlCityName;
                    }
                    if (inFld.ValueName.ToUpper() == "STATENAME")
                    {
                        var ddlStateName = (
                            from result in DDLMaster.FetchState(0)
                            select new SelectListItem()
                            {
                                Text = result.StateName,
                                Value = result.StateID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlStateName.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlStateName = ddlStateName;
                    }
                    if (inFld.ValueName.ToUpper() == "CITYNAME1")
                    {
                        var ddlCityName1 = (
                            from result in DDLMaster.FetchState(0)
                            select new SelectListItem()
                            {
                                Text = result.StateName,
                                Value = result.StateID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlCityName1.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlCityName1 = ddlCityName1;
                    }
                    if (inFld.ValueName.ToUpper() == "BOOKINGSTATUS")
                    {
                        var ddlBookingStatus = (
                            from result in DDLMaster.FetchBookingStatus(0)
                            select new SelectListItem()
                            {
                                Text = result.StatusName,
                                Value = result.StatusID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlBookingStatus.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlBookingStatus = ddlBookingStatus;
                    }
                    if (inFld.ValueName.ToUpper() == "POSTINGSTATUS")
                    {
                        var ddlPostingStatus = (
                            from result in DDLMaster.FetchPostingStatus(0)
                            select new SelectListItem()
                            {
                                Text = result.StatusName,
                                Value = result.StatusID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlPostingStatus.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlPostingStatus = ddlPostingStatus;
                    }
                    if (inFld.ValueName.ToUpper() == "BUSINESSTYPE")
                    {
                        var ddlBusinessType = (
                            from result in DDLMaster.FetchBusinessType(0)
                            select new SelectListItem()
                            {
                                Text = result.BusinessTypeName,
                                Value = result.BusinessTypeID.ToString(),
                                Selected = false
                            }
                        ).ToList();
                        ddlBusinessType.Add(new SelectListItem() { Text = "Select All", Value = "" });
                        ViewBag.ddlBusinessType = ddlBusinessType;
                    }
                }
                
            }
        }

        private ReportSchema GetReportObj (string rptName)
        {
            ReportSchema rpt;
            ReportBL rptObj = new ReportBL();
            rpt = rptObj.GetGenericReportDetails(rptName, ((User)Session["User"]).UserID);
            return rpt;
        }

        [HttpPost]
        public ActionResult GetReportData(DictionaryModel inputFields, string reportName)
        {
            string result = "Failure";
            AjaxReturnData returnVal = new AjaxReturnData();
            returnVal.UniqueId = 0;
            returnVal.IsSuccess = false;
            returnVal.Message = result;

            try
            {
                //string reportName = ViewBag.ReportName;
                // 1. Get the report param for the reportname
                ReportSchema rpt = GetReportObj(reportName); // TODO: Can this be instead stored in tempdata or session and fetched for data fetch?

                // 2. Construct the report params from the report obj and send it to Data access layer
                ReportParamCollection rptParamList = new ReportParamCollection();
                rptParamList.ReportParams = new List<ReportParameters>();
                for (int i=0;i< rpt.InFields.Count; i++)
                {
                    ReportParameters rptParam = new ReportParameters();
                    rptParam.ParamName = rpt.InFields[i].SPParamName.ToString();
                    rptParam.ParamDataType = GetSPParamDataType(rpt.InFields[i].SPParamDataType.ToString());
                    if(rpt.InFields[i].ValueType.ToUpper() != "DROPDOWN")
                    {
                        rptParam.ParamSize = Convert.ToInt32(rpt.InFields[i].SPParamDataSize);                        
                    }
                    rptParam.ParamData = inputFields.dict.ToList().Find(m => m.Key == rpt.InFields[i].ValueID).Value.ToString().Trim();

                    rptParamList.ReportParams.Add(rptParam);
                }

                // All admin report SPs shoud have the @loggedInUser as input parameter to each search SP
                ReportParameters rptLoginParam = new ReportParameters();
                rptLoginParam.ParamName = "@loggedInUser";
                rptLoginParam.ParamDataType = FieldType.String;
                rptLoginParam.ParamData = ((User)Session["User"]).UserID.ToString();

                rptParamList.ReportParams.Add(rptLoginParam);


                // 3. Send the param collection to BL and DAL so as to execute the SP
                ReportBL reportObj = new ReportBL();
                returnVal.Message= reportObj.GetGenericReportData(rpt.SearchSPName, rptParamList);
                // 4. Convert the dataTable to JSON string in <TR><TD> format for each row

                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                returnVal.Message = returnVal.Message + ":" + ex.Message.ToString();
                return Json(returnVal, JsonRequestBehavior.AllowGet);
            }
        }

        private FieldType GetSPParamDataType(string ParamDataType)
        {
            return FieldType.String; // TODO: Write logic to fitler the actual data type from incoming data using switch case statement.
        }

        public ActionResult GetDriverDetails()
        {
            return View();
        }

    }
}