﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TransporterModel
{
    [XmlRoot("Report")]
    public class ReportSchema
    {
        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("InputField")]
        public List<InputField> InFields { get; set; }

        [XmlElement("OutputFields")]
        public OutFields OutFields { get; set; }

        [XmlElement("SearchSP")]
        public string SearchSPName { get; set; }

        [XmlElement("OutJSONSearchData")]
        public string OutJSONSearchData { get; set; }        
    }

    public class OutFields
    {
        [XmlElement("OutFieldName")]
        public List<string> OutFieldName { get; set; }
    }

    public class OutputFields
    {
        [XmlElement("OutFieldName")]
        public string OutFieldName { get; set; }
    }

    public class InputField
    {
        [XmlElement("LabelID")]
        public string LabelID { get; set; }

        [XmlElement("LabelText")]
        public string LabelText { get; set; }

        [XmlElement("ValueID")]
        public string ValueID { get; set; }

        [XmlElement("ValueType")]
        public string ValueType { get; set; }

        [XmlElement("ValueName")]
        public string ValueName { get; set; }

        [XmlElement("HiddenID")]
        public string HiddenID { get; set; }

        [XmlElement("ValueFieldPlaceholder")]
        public string ValueFieldPlaceholder { get; set; }

        [XmlElement("SPParamName")]
        public string SPParamName { get; set; }

        [XmlElement("SPParamDataType")]
        public string SPParamDataType { get; set; }

        [XmlElement("SPParamDataSize")]
        public string SPParamDataSize { get; set; }


    }

    public enum FieldType
    {
        String,
        DateTime,
        DropDown,
        CitySelect,
        Integer,
        StateSelect
    };
    
    /// ///////////////////
    
    public class DictionaryModel
    {
        public Dictionary<string, string> dict { get; set; }

    }

    public class ReportParamCollection
    {
        public List<ReportParameters> ReportParams { get; set; }
    }
    public class ReportParameters
    {
        public string ParamName { get; set; }
        public FieldType ParamDataType { get; set; }
        public int ParamSize { get; set; }
        public string ParamData { get; set; }
    }

}
