﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
//using System.ComponentModel.DataAnnotations;

namespace TransporterModel
{
    public class LoadBooking
    {
        public int BookingID { get; set; }
        //[Required]
        public int UserID { get; set; }
        
        public int SourceCityID { get; set; }
        //[Required]
        public string SourceCityName { get; set; }
        
        public int DestinationCityID { get; set; }
        //[Required]
        public string DestinationCityName { get; set; }

        public string DeliveryDate { get; set; }
        public int GoodsTypeID { get; set; }
        public int GoodsWeightID { get; set; }
        public int PackageTypeID { get; set; }
        public int TruckTypeID { get; set; }
        public int PaymentTypeID { get; set; }
        public Int64 ContactPersonPhoneNumber { get; set; }
        public Int64 ContactPersonMobileNumber { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string PhotoPath { get; set; }

        public int CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }

        public string CreatedByName { get; set; }
        public string UpdatedByName { get; set; }
        public int BookingStatus { get; set; }        
        public string LRNumber { get; set; }
        public string BookingStatusName { get; set; }

        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

    }

}
