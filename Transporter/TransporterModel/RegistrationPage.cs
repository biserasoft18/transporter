﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class RegistrationPage
    {
        public LoadRegistration RegistrationDetails { get; set; }

        public List<UserType> UserType { get; set; }
        public List<Country> Country { get; set; }
        public List<State> State{ get; set; }
        public List<City> City { get; set; }
        public List<Town> Town{ get; set; }
        public List<SecretQuestion> SecretQuestion { get; set; }
    }
}
