﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class DriverTripData
    {
        public TripData TripMainDetails { get; set; }
        public List<PickAndDropDetails> TripDetails { get; set; }
    }

    public class TripData
    {
        public int PostingID { get; set; }
        public string TruckNumber { get; set; }
        public string SourceCity { get; set; }
        public string DestinationCity { get; set; }
        public string StartDate { get; set; }
        public string DeliveryDate { get; set; }
        public string GoodsWeightName { get; set; }
        public string AvailableTonnage { get; set; }
        public int DriverID { get; set; }
        public string DriverName { get; set; }
        public string SourceCityLat { get; set; }
        public string SourceCityLon { get; set; }
        public string DestinationCityLat { get; set; }
        public string DestinationCityLon { get; set; }
        public string ViaRoutes { get; set; }
    }

    public class PickAndDropDetails
    {
        public int PostingID { get; set; }
        public string BookingID { get; set; }
        public string LRNumber { get; set; }
        public string TruckNumber { get; set; }
        public string AgentName { get; set; }
        public string AgentNumber { get; set; }
        public int StartCityID { get; set; }
        public string StartCityName { get; set; }
        public int DestinationCityID { get; set; }
        public string DestinationCityName { get; set; }
        public int BookingSortOrder { get; set; }
        public string PickupLat { get; set; }
        public string PickupLon { get; set; }
        public string DropLat { get; set; }
        public string DropLon { get; set; }
        public string SourceCityLat { get; set; }
        public string SourceCityLon { get; set; }
        public string DestinationCityLat { get; set; }
        public string DestinationCityLon { get; set; }
        public string GoodsWeight { get; set; }
        public string GoodsType { get; set; }
        public string PickupAddress { get; set; }
        public string DropAddress { get; set; }
        public string AgentEmail { get; set; }
    }
}
