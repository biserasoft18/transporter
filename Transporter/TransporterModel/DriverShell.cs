﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class DriverShell
    {
        public int DriverID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Int64 MobileNumber{ get; set; }
        public string LicenseNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int CityID { get; set; }
        public int StateID { get; set; }
        public int PinCode { get; set; }
        public int CountryID { get; set; }
        public Boolean DriverMobileAppID { get; set; }
        public int TransporterID { get; set; }
        public Boolean ClearAppID { get; set; }

        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }

        public string TransporterName { get; set; }
    }

    public class AllDriverDetails
    {
        public List<DriverShell> DriversInfo { get; set; }
    }

    public class DriverDetailsPage
    {
        public DriverShell DriverInfo { get; set; }
        public List<Country> Country { get; set; }
        public List<State> State { get; set; }
        public List<City> City { get; set; }
    }
}
