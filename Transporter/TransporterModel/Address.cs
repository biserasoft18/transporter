﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class Address
    {
        public int AddressID { get; set; }
        public int AddressTypeID { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
    }

    public enum AddressTypeID
    {
        Pickup = 1,
        Drop = 2
    }

    public class BookingAddress
    {
        public Address PickupAddress { get; set; }
        public Address DropAddress { get; set; }
    }
}
