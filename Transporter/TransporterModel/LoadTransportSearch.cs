﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
   public  class LoadTransportSearch
    {
        public int SourceCityID { get; set; }
        //[Required]
        public string SourceCityName { get; set; }

        public int DestinationCityID { get; set; }
        //[Required]
        public string DestinationCityName { get; set; }

        public DateTime DeliveryDate { get; set; }

        public int GoodsWeightID { get; set; }

        public int TruckTypeID { get; set; }

        public int PostingStatusID { get; set; }

        public decimal AvailableCapacity { get; set; }

        public int PostingID { get; set; }

        public Boolean MyPostingsOnly { get; set; }
    }
}
