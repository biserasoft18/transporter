﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class TripSheet
    {
        public int PostingID { get; set; }
        public string StartDate { get; set; }
        public int DriverID { get; set; }
        public string TruckNumber { get; set; }
        public int PostingStatus { get; set; }
        public string PostingStatusName { get; set; }
        public string ViaRoutes { get; set; }

        public List<TripDriverList> TripDriverData { get; set; }
        public List<TripBookingList> TripBookingData { get; set; }

        public TripTracking TruckTrackingDetails { get; set; }

    }

    public class TripDriverList
    {
        public int DriverID { get; set; }
        public string DriverName { get; set; }
    }

    public class TripBookingList
    {
        public int BookingID { get; set; }
        public string LRNumber { get; set; }
        public string AgentName { get; set; }
        public string StartCityName { get; set; }
        public string DestinationCityName { get; set; }
        public int BookingSortOrder { get; set; }
        public int StartCityID { get; set; }
        public int DestinationCityID { get; set; }
        public string AgentNumber { get; set; }
        public string PickupTime { get; set; }
        public string DropTime { get; set; }
        public string WhereTruck { get; set; }
        public string AgentEmail { get; set; }
    }

    public class BookingSortInfo
    {
        public int StartCityID { get; set; }
        public int DestinationCityID { get; set; }
        public int BookingID { get; set; }
        public int BookingSortOrder { get; set; }
    }

    public class TrackingDetails
    {
        public int DriverID { get; set; }
        public Int64 PostingID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CapturedOn { get; set; }
    }
}