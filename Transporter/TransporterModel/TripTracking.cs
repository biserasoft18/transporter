﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class TripTracking
    {
        public List<TripViaRoutes> TripData { get; set; }
        public List<TruckTracking> TruckTrackingData { get; set; }
        public string Result { get; set; }
    }

    public class TripViaRoutes
    {
        public Decimal lat { get; set; }
        public Decimal lng { get; set; }
    }

    public class TruckTracking
    {
        public Decimal lat { get; set; }
        public Decimal lng { get; set; }
        public string msg { get; set; }
    }

    public class TripDetailFromPage
    {
        public TripTracking TripTrackingDetails { get; set; }
        public int PostingID { get; set; }
    }
}
