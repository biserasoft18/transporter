﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class UserSubscription
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Type { get; set; }
    }

    public class ClientPaymentInitiate
    {
        public string UserFullName { get; set; }
        public string UserEmail { get; set; }
        public string UserMobileNumber { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentCurrency { get; set; }
        public string PaymentTransactionID { get; set; }
        public int UserID { get; set; }
        public int UserTypeID { get; set; }
        public string PaymentInitiateDateFrom { get; set; }
        public string PaymentInitiateDateTo { get; set; }
        public int PaymentInitiateTypeID { get; set; }
    }

    public class UserPaymentDetails
    {
        public int UserID { get; set; }
        public int IntPaymentID { get; set; }
        public string CurrentExpiryDate { get; set; }
        public string RequestedExpiryDate { get; set; }
        public Decimal PaymentAmount{ get; set; }
        public string ExtTransactionID { get; set; }
        public string ExtPaymentID { get; set; }
        public string RedirectURL { get; set; }
        public string WebHookURL { get; set; }
        public string PaymentURL { get; set; }
        public string InitiatedOn { get; set; }
        public string PaymentAcknowledgementReceivedOn { get; set; }
        public string ResponseReceivedOn { get; set; }
        public string ExtMacID { get; set; }
        public string ExtPaymentResponseID { get; set; }
        public int PaymentStatusID { get; set; }
        public string WebhookResponse { get; set; }
        public string PaymentStatusName { get; set; }
        public string UserName { get; set; }
        public string UserTypeName { get; set; }
        public string MemberSince { get; set; }
    }

    public class UserPaymentHistory
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string MemberSince { get; set; }
        public string LastPaymentAttemptedOn { get; set; }
        public string MemberStatus { get; set; }
        public List<UserPaymentDetails> PaymentDetails { get; set; }
        public string GetPaymentUpdateURL { get; set; }
        public string UserType { get; set; }
        public string Message { get; set; }
    }

    public class WebHookResponse
    {
        public string amount { get; set; }
        public string buyer { get; set; }
        public string buyer_name { get; set; }
        public string buyer_phone { get; set; }
        public string currency { get; set; }
        public string fees { get; set; }
        public string longurl { get; set; }
        public string mac { get; set; }
        public string payment_id { get; set; }
        public string payment_request_id { get; set; }
        public string purpose { get; set; }
        public string shorturl { get; set; }
        public string status { get; set; }
    }
}
