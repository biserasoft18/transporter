﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class TransportSearchPage
    {
        public LoadTransportSearch TransportSearchDetails { get; set; }

        public List<GoodsWeight> GoodsWeight { get; set; }
        public List<GoodsType> GoodsType { get; set; }
        public List<TruckType> TruckType { get; set; }
        public List<PostingStatus> PostingStatus { get; set; }

        public List<LoadTransporterSearchDetail> TransportSearchData { get; set; }
    }
}
