﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class LoadSearchAgentDet
    {
        public int BookingID { get; set; }

        public string SourceCity { get; set; }

        public string DestinationCity { get; set; }

        public string DeliveryDate { get; set; }

        public string GoodsWeightName { get; set; }

        public string TruckTypeName { get; set; }

        public string BookingStatus { get; set; }
    }
}
