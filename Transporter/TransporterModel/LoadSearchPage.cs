﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class LoadSearchPage
    {
        public int BookingID { get; set; }

        public int SourceCityID { get; set; }
        //[Required]
        public string SourceCityName { get; set; }

        public int DestinationCityID { get; set; }
        //[Required]
        public string DestinationCityName { get; set; }

        public DateTime DeliveryDate { get; set; }

        public int GoodsWeightID { get; set; }
        public string GoodsWeightName { get; set; }

        public int TruckTypeID { get; set; }
        public string TruckTypeName { get; set; }

        public int BookingStatusID { get; set; }

        public string BookingStatus { get; set; }
        
        public Boolean MyBookingsOnly { get; set; }
    }
}
