﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class AgentSearchPage
    {
        public LoadSearchPage SearchDetails { get; set; }

        public List<GoodsWeight> GoodsWeight { get; set; }
        public List<TruckType> TruckType { get; set; }
        public List<BookingStatus> BookingStatus { get; set; }
        public List<LoadSearchPage> SearchData { get; set; }

    }
}
