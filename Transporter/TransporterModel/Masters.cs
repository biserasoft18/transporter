﻿using System;
using System.Collections.Generic;
using System.Data;

namespace TransporterModel
{
    public class GoodsType
    {
        public int GoodsTypeID { get; set; }
        public string GoodsTypeName { get; set; }
        public string GoodsTypeCode { get; set; }
    }

    public class GoodsWeight
    {
        public int GoodsWeightID { get; set; }
        public string GoodsWeightName { get; set; }
        public string GoodsWeightCode { get; set; }
    }

    public class PackageType
    {
        public int PackageTypeID { get; set; }
        public string PackageTypeName { get; set; }
        public string PackageTypeCode { get; set; }
    }

    public class TruckType
    {
        public int TruckTypeID { get; set; }
        public string TruckTypeName { get; set; }
        public string TruckTypeCode { get; set; }
    }

    public class PaymentType
    {
        public int PaymentTypeID { get; set; }
        public string PaymentTypeName { get; set; }
        public string PaymentTypeCode { get; set; }
    }

    public class City
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public string CityNameWithState { get; set; }        
    }

    public class Town
    {
        public int TownID { get; set; }
        public string TownName { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string TownNameWithCity { get; set; }
    }


    public class State
    {
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
    }

    public class Country
    {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
    }

    public class UserType
    {
        public int UserTypeID { get; set; }
        public string UserTypeName { get; set; }
        public string UserTypeCode { get; set; }
    }

    public class BookingStatus
    {
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public string StatusCode { get; set; }
    }

    public class PostingStatus
    {
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public string StatusCode { get; set; }
    }

    public class SecretQuestion
    {
        public int SecretQuestionID { get; set; }
        public string SecretQuestionText { get; set; }
        public string SecretQuestionCode { get; set; }
    }

    public class YesNo
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
    }

    public class BusinessTypeMaster
    {
        public int BusinessTypeID { get; set; }
        public string BusinessTypeName { get; set; }
    }
}
