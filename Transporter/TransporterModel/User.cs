﻿using System;

namespace TransporterModel
{
    public class User
    {
        public int UserID { get; set; }
        public int UserTypeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int InvalidLoginID { get; set; }
        public string LoginID { get; set; }
        public string ParentCompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Email { get; set; }
        public Int64 PhoneNumber { get; set; }
        public Int64 MobileNumber { get; set; }
        public string Website { get; set; }
        public string PANNumber { get; set; }
        public string TINNumber { get; set; }
        public DateTime ValidFrom{ get; set; }
        public DateTime ValidTo { get; set; }
        public Boolean IsAdmin { get; set; }
        public Boolean IsPaidUser { get; set; }
    }

    public class passwordrecovery
    {
        public string EmailID { get; set; }
        public string Uservalidity { get; set; }
        public string password { get; set; }
        public string Message { get; set; }
        public Int64 MobileNumber { get; set; }
        public int UserID { get; set; }
    }

    public class ViewUserInfo
    {
        public string Uservalidity { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string TownName { get; set; }
        public Int64 MobileNumber { get; set; }
        public Int64 PhoneNumber { get; set; }
        public string IsPaidUser { get; set; }
        public string IsDriver { get; set; }
    }

    public class UserLog
    {
        public int UserID { get; set; }
        public string IPAddress { get; set; }
        public string IPType { get; set; }
        public string ContinentCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string CityName { get; set; }
        public string ZipCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Environment { get; set; }
        public string FullTrace { get; set; }
        public string LastLoginAt { get; set; }
    }

    public class ReportsPageMenu
    {
        public Boolean IsAdminReport { get; set; }
        public Boolean IsUserReport { get; set; }
        public string ReportName { get; set; }
        public int ReportID { get; set; }
    }

    
}
