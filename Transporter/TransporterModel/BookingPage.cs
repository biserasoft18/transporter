﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class BookingPage
    {
        public LoadBooking BookingDetails { get; set; }

        public List<GoodsType> GoodsType { get; set; }
        public List<GoodsWeight> GoodsWeight { get; set; }
        public List<TruckType> TruckType { get; set; }
        public List<PackageType> PackageType{ get; set; }
        public List<PaymentType> PaymentType { get; set; }

        

        public List<BookingMessage> AllbookingMessage { get; set; }

        public List<BookingMessage> AllPostingRequest { get; set; }

        public BookingAddress AllAddress { get; set; }

    }
}
