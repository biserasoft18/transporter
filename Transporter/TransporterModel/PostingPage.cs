﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class PostingPage
    {
        public LoadPosting PostingDetails { get; set; }

        public List<GoodsWeight> GoodsWeight { get; set; }
        public List<TruckType> TruckType { get; set; }
        public List<YesNo> IsCargoInsured { get; set; }
        public List<City> ViaCities { get; set; }

        public List<PostingMessage> AllPostingMessage { get; set; }

        public List<ExpressInterest> AllExpressInterest { get; set; }

        public List<MessageToOtherAgents> AllMessageToAgent { get; set; }
    }

    



}
