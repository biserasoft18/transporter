﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class BookingMessage
    {
        public int BookingID { get; set; }
        public int PostingID { get; set; }
        public string UserMessage { get; set; }
        public int TransporterID { get; set; }
        public int IsAccepted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime AcceptedOn { get; set; }
        public string AgentName { get; set; }
        public int UserID { get; set; }
    }
}
