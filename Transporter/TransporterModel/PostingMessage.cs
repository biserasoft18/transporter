﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class PostingMessage
    {
        public int PostingID { get; set; }
        public int BookingID { get; set; }
        public string AgentMessage { get; set; }
        public int UserID { get; set; }
        public int IsAccepted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime AcceptedOn { get; set; } 
        public string AgentName { get; set; }
        public int PackageWeight { get; set; }
        public string AcceptorIgnore { get; set; }

    }

    public class ExpressInterest
    {
        public string GoodsWeightName { get; set; }
        public string TruckTypeName { get; set; }
        public string DestinationCity { get; set; }
        public string SourceCity { get; set; }
        public int BookingID { get; set; }
        public int linkedBookingId { get; set; }
    }

    public class MessageToOtherAgents
    {
        public int PostingID { get; set; }
        public int BookingID { get; set; }
        public string UserMessage { get; set; }
        public string DestinationCity { get; set; }
        public string SourceCity { get; set; }
        public int IsAccepted { get; set; }
        public int UserID { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime AcceptedOn { get; set; }
        public string AgentName { get; set; }
        public string DeliveryDate { get; set; }
        public string GoodsWeightName { get; set; }
        public string GoodsTypeName { get; set; }
        public string AcceptorIgnore { get; set; }
    }

    


}
