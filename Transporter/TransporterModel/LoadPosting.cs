﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;

namespace TransporterModel
{
    public class LoadPosting
    {
        public int PostingID { get; set; }
        //[Required]
        public int UserID { get; set; }

        public int SourceCityID { get; set; }
        //[Required]
        public string SourceCityName { get; set; }

        public int DestinationCityID { get; set; }
        //[Required]
        public string DestinationCityName { get; set; }

        public string StartDate { get; set; }
        public string DeliveryDate { get; set; }
        public int TruckTypeID { get; set; }
        public string TruckNumber { get; set; }
        public int GoodsWeightID { get; set; }
        public decimal AvailableTonnage { get; set; }
        public Int64 MobileNumber { get; set; }
        public Int64 OfficePhone { get; set; }
        public string Email { get; set; }
        public string PhotoPath { get; set; }
        public bool IsCargoInsured { get; set; }
        public bool IsSMSSent { get; set; }
        public bool IsEmailSent { get; set; }

        public int CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }

        public string CreatedByName { get; set; }
        public string UpdatedByName { get; set; }
        public int DriverID { get; set; }
        public int PostingStatus { get; set; }
        public string PostingStatusName { get; set; }
        public string DriverName { get; set; }

        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
    }
}
