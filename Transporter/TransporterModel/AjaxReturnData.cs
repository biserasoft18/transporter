﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class AjaxReturnData
    {
            public int UniqueId { get; set; }
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
            public string RedirectURL { get; set; }
    }

    public class FileUploadData
    {
        public string RetMessage { get; set; }
        public string FilePath { get; set; }
    }
}
