﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransporterModel
{
    public class LoadAgentUserInbox
    {
        public string AgentName { get; set; }

        public int PostingID { get; set; }

        public int BookingID { get; set; }

        public string SourceCity { get; set; }

        public string DestinationCity { get; set; }

        public string DeliveryDate { get; set; }

        public string LinkURL { get; set; }

        
    }
}
