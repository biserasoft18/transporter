﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace TransporterData
{
    public class Posting
    {
        SqlHelper DBObj = new SqlHelper();

        public string SetTripSheetData(DataTable dtSortInfo, int postingID, int driverID, string truckNumber,  int userID, ref DataTable agentContactInfo)
        {
            //int returnData = 0;
            DataSet ds = new DataSet();
            SqlParameter[] allParams = new SqlParameter[7];
            SqlParameter bookingData = new SqlParameter("@sortData", SqlDbType.Structured)
            {
                TypeName = "dbo.udt_PostingEnquirySortData",
                Value = dtSortInfo
            };
            allParams[0] = bookingData;

            allParams[1] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[2] = DBObj.CreateSQLParam(userID, "@userID");
            allParams[3] = DBObj.CreateSQLParam(driverID, "@driverID");
            allParams[4] = DBObj.CreateSQLParam(truckNumber, 15, "@truckNum");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[5] = returnMessageParam;
            SqlParameter returnAgentContactParam = DBObj.CreateOutSQLParam(8000, "@returnAgentContact");
            allParams[6] = returnAgentContactParam;

            //returnData = DBObj.ExecuteNonQuery("SetTripSheetData_SP", allParams);

            DBObj.Fill(ds, "SetTripSheetData_SP", allParams);
            agentContactInfo = ds.Tables[0];

            return (string)returnMessageParam.Value;
        }

        public string SetPostingData(DataTable dt, DataTable dtCity, ref int postingID, int userID, int postingStatus)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[6];
            SqlParameter bookingData = new SqlParameter("@postingData", SqlDbType.Structured)
            {
                TypeName = "dbo.udt_PostingData",
                Value = dt
            };
            allParams[0] = bookingData;

            SqlParameter viaCityData = new SqlParameter("@cityData", SqlDbType.Structured)
            {
                TypeName = "dbo.udt_ViaCityData",
                Value = dtCity
            };
            allParams[1] = viaCityData;

            SqlParameter postingIDParam = new SqlParameter();
            postingIDParam.ParameterName = "@postingID";
            postingIDParam.DbType = DbType.Int32;
            postingIDParam.Direction = ParameterDirection.InputOutput;
            postingIDParam.Value = postingID;
            allParams[2] = postingIDParam;

            allParams[3] = DBObj.CreateSQLParam(userID, "@userID");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[4] = returnMessageParam;

            allParams[5] = DBObj.CreateSQLParam(postingStatus, "@postingStatus");


            returnData = DBObj.ExecuteNonQuery("SetPostingData_SP", allParams);

            postingID = (int)postingIDParam.Value;

            //return returnMessageParam.Value.ToString();

            return (string)returnMessageParam.Value;
        }

        public List<int> GetPostingIDList(int curUserID)
        {
            List<int> returnObj = new List<int>();
            DataSet ds = new DataSet();

            SqlParameter[] allParams = new SqlParameter[1];
            

            SqlParameter userIDParam = new SqlParameter();
            userIDParam.ParameterName = "@loggedInUserID";
            userIDParam.DbType = DbType.Int32;
            userIDParam.Direction = ParameterDirection.Input;
            userIDParam.Value = curUserID;
            allParams[0] = userIDParam;


            DBObj.Fill(ds, "GetPostingIDList_SP", allParams);
            if(ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for(int i=0;i<ds.Tables[0].Rows.Count; i++)
                    {
                        returnObj.Add(Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString()));
                    }
                }
            }
            return returnObj;
        }

        public DataSet GetMessageToTransporter(int postingID, int curUserID, ref int countbookingID)
        {
            //int returnData;

            
            DataSet ds = new DataSet();
            //SqlParameter[] allParams = new SqlParameter[4];
            SqlParameter[] allParams = new SqlParameter[3];

            SqlParameter postingIDParam = new SqlParameter();
            postingIDParam.ParameterName = "@postingID";
            postingIDParam.DbType = DbType.Int32;
            postingIDParam.Direction = ParameterDirection.Input;
            postingIDParam.Value = postingID;
            allParams[0] = postingIDParam;

            SqlParameter userIDParam = new SqlParameter();
            userIDParam.ParameterName = "@loggedInUserID";
            userIDParam.DbType = DbType.Int32;
            userIDParam.Direction = ParameterDirection.Input;
            userIDParam.Value = curUserID;
            allParams[1] = userIDParam;

            SqlParameter returnBookingID = new SqlParameter();
            returnBookingID.ParameterName = "@countbookingID";
            returnBookingID.DbType = DbType.Int32;
            returnBookingID.Direction = ParameterDirection.InputOutput;
            returnBookingID.Value = 0;
            allParams[2] = returnBookingID;

           
            DBObj.Fill(ds, "GetMessageToTransporterForCurUser_SP", allParams);
            countbookingID = (int) returnBookingID.Value;
            return ds;

            
        }

        public DataSet GetMessageToAgent(int postingID, int curUserID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] allParams = new SqlParameter[2];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[1] = DBObj.CreateSQLParam(curUserID, "@loggedInUserID");
                        
            DBObj.Fill(ds, "GetMessageToAgentByTransporter_SP", allParams);
            return ds;
        }

        public DataSet GetTripGeoLocData(int postingID, int curUserID, ref string result)
        {
            DataSet ds = new DataSet();

            SqlParameter[] allParams = new SqlParameter[3];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[1] = DBObj.CreateSQLParam(curUserID, "@loggedInUserID");
            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[2] = returnMessageParam;
            
            DBObj.Fill(ds, "GetTripGeoLocData_SP", allParams);
            result = returnMessageParam.Value.ToString();
            return ds;
        }

        public DataSet GetAllMessageToTransporter(int postingID, int curUserID)
        {
            DataSet ds = new DataSet();

            SqlParameter[] allParams = new SqlParameter[2];

            SqlParameter postingIDParam = new SqlParameter();
            postingIDParam.ParameterName = "@postingID";
            postingIDParam.DbType = DbType.Int32;
            postingIDParam.Direction = ParameterDirection.Input;
            postingIDParam.Value = postingID;
            allParams[0] = postingIDParam;

            SqlParameter userIDParam = new SqlParameter();
            userIDParam.ParameterName = "@loggedInUserID";
            userIDParam.DbType = DbType.Int32;
            userIDParam.Direction = ParameterDirection.Input;
            userIDParam.Value = curUserID;
            allParams[1] = userIDParam;


            DBObj.Fill(ds, "GetAllMessagesToTransporter_SP", allParams);
            return ds;
        }

        public string ClosePosting(int postingID, int postingOwnerID)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[3];

            SqlParameter postingIDParam = new SqlParameter();
            postingIDParam.ParameterName = "@postingID";
            postingIDParam.DbType = DbType.Int32;
            postingIDParam.Direction = ParameterDirection.Input;
            postingIDParam.Value = postingID;
            allParams[0] = postingIDParam;

            SqlParameter userIDParam = new SqlParameter();
            userIDParam.ParameterName = "@userID";
            userIDParam.DbType = DbType.Int32;
            userIDParam.Direction = ParameterDirection.Input;
            userIDParam.Value = postingOwnerID;
            allParams[1] = userIDParam;

            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[2] = returnMessageParam;

            returnData = DBObj.ExecuteNonQuery("SetPostingClosed_SP", allParams);

            return (string)returnMessageParam.Value;
        }

        public string SetPostingMessage(string Message, int postingID, int bookingID, int userID, ref string transEmail, ref long transMobileNumber)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[7];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[1] = DBObj.CreateSQLParam(userID, "@userID");
            allParams[2] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            allParams[3] = DBObj.CreateSQLParam(Message, 4000, "@messageToTransporter");
            
            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[4] = returnMessageParam;


            SqlParameter returnTranEmail = new SqlParameter();
            returnTranEmail.ParameterName = "@returnTransEmail";
            returnTranEmail.DbType = DbType.String;
            returnTranEmail.Size = 200;
            returnTranEmail.Direction = ParameterDirection.Output;
            allParams[5] = returnTranEmail;

            SqlParameter returnTransMobile = new SqlParameter();
            returnTransMobile.ParameterName = "@returnTransMobile";
            returnTransMobile.DbType = DbType.Int64;
            returnTransMobile.Direction = ParameterDirection.Output;
            allParams[6] = returnTransMobile;
            
            returnData = DBObj.ExecuteNonQuery("SetPostingMessage_SP", allParams);

            transEmail = (string)returnTranEmail.Value;
            transMobileNumber = (long)returnTransMobile.Value;

            return (string)returnMessageParam.Value;

        }

        public string AcceptDeal(int agentID, int bookingID, int postingID, int loggedInUserId, ref string agentEmail, ref long agentMobileNumber)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[7];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[1] = DBObj.CreateSQLParam(loggedInUserId, "@userID");
            allParams[2] = DBObj.CreateSQLParam(agentID, "@agentID");
            allParams[3] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            
            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[4] = returnMessageParam;


            SqlParameter returnAgentEmail = new SqlParameter();
            returnAgentEmail.ParameterName = "@returnAgentEmail";
            returnAgentEmail.DbType = DbType.String;
            returnAgentEmail.Size = 200;
            returnAgentEmail.Direction = ParameterDirection.Output;
            allParams[5] = returnAgentEmail;

            SqlParameter returnAgentMobile = new SqlParameter();
            returnAgentMobile.ParameterName = "@returnAgentMobile";
            returnAgentMobile.DbType = DbType.Int64;
            returnAgentMobile.Direction = ParameterDirection.Output;
            allParams[6] = returnAgentMobile;

            returnData = DBObj.ExecuteNonQuery("SetPostingDeal_SP", allParams);

            agentEmail = (string)returnAgentEmail.Value;
            agentMobileNumber = (long)returnAgentMobile.Value;

            return (string)returnMessageParam.Value;
        }

        public DataSet ShowcontactMessage(int CAgentID, int LogUserID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] allParams = new SqlParameter[2];

            allParams[0] = DBObj.CreateSQLParam(CAgentID, "@CAgentID");
            allParams[1] = DBObj.CreateSQLParam(LogUserID, "@LogUserID");

            DBObj.Fill(ds, "ShowContactInfo_SP", allParams);
            return ds;

           }

        public DataSet GetTripSheetData(int postingID, int bookingID, int loggedInUserID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] allParams = new SqlParameter[3];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[1] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            allParams[2] = DBObj.CreateSQLParam(loggedInUserID, "@loggedInUserID");
            
            DBObj.Fill(ds, "GetTripSheetData_SP", allParams);
            return ds;
        }

        public DataSet GetDriverTripData(int driverUserID, string appID, string userToken, ref string returnVal)
        {
            DataSet ds = new DataSet();
            SqlParameter[] allParams = new SqlParameter[4];

            allParams[0] = DBObj.CreateSQLParam(driverUserID, "@driverUserID");

            allParams[1] = DBObj.CreateSQLParam(appID, 100, "@appID");
            

            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[2] = returnMessageParam;

            allParams[3] = DBObj.CreateSQLParam(userToken, 100, "@userToken");

            DBObj.Fill(ds, "GetDriverTripSheetData_APP_SP", allParams);
            returnVal = (string)returnMessageParam.Value;
            return ds;
        }

        public void SetTripSheetData(int userID, string appID, int postingID, int bookingID, int sortOrder, string latid, string longit, int pickupOrDrop, string userToken, ref string retVal)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[10];

            allParams[0] = DBObj.CreateSQLParam(userID, "@driverID");
            allParams[1] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[2] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            allParams[3] = DBObj.CreateSQLParam(sortOrder, "@sortOrder");
            allParams[4] = DBObj.CreateSQLParam(pickupOrDrop, "@pickupOrDrop");
            allParams[5] = DBObj.CreateSQLParam(appID, 100, "@mobileAppID");
            allParams[6] = DBObj.CreateSQLParam(latid,100, "@latid");
            allParams[7] = DBObj.CreateSQLParam(longit, 100, "@longid");
            allParams[8] = DBObj.CreateSQLParam(userToken, 100, "@userToken");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[9] = returnMessageParam;
            
            returnData = DBObj.ExecuteNonQuery("SetTripData_APP_SP", allParams);

            retVal = returnMessageParam.Value.ToString();            
        }

        public void SetVehicleTrackingInfo(int userID, string appID, int postingID, string latid, string longit, string userToken, ref string retVal)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[7];

            allParams[0] = DBObj.CreateSQLParam(userID, "@driverUserID");
            allParams[1] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[2] = DBObj.CreateSQLParam(appID, 100, "@appID");
            allParams[3] = DBObj.CreateSQLParam(latid, 100, "@latid");
            allParams[4] = DBObj.CreateSQLParam(longit, 100, "@longid");
            allParams[5] = DBObj.CreateSQLParam(userToken, 100, "@userToken");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[6] = returnMessageParam;

            returnData = DBObj.ExecuteNonQuery("SetVehicleCoOrdinates_APP_SP", allParams);

            retVal = returnMessageParam.Value.ToString();
        }
        

        public string EndTrip(int driverID, string appID, int postingID, string userToken)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[5];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[1] = DBObj.CreateSQLParam(appID, 100, "@appID");
            allParams[2] = DBObj.CreateSQLParam(driverID, "@driverID"); 
            allParams[3] = DBObj.CreateSQLParam(userToken, 100, "@userToken"); 

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[4] = returnMessageParam;

            returnData = DBObj.ExecuteNonQuery("EndTrip_APP_SP", allParams);

            return (string)returnMessageParam.Value;
        }

        public string ResetTrip(int postingID)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[2];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[1] = returnMessageParam;

            returnData = DBObj.ExecuteNonQuery("TEST_ResetTrip_SP", allParams);

            return (string)returnMessageParam.Value;
        }

        public DataSet GetTrackingDetails(int postingID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] allParams = new SqlParameter[1];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");
            
            
            DBObj.Fill(ds, "TEST_GetTrackingDetails_SP", allParams);
            return ds;
        }

        public string CancelPosting(int postingID, int loggedInUserID)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[3];

            allParams[0] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[1] = DBObj.CreateSQLParam(loggedInUserID, "@loggedInUserID");
            SqlParameter returnObj = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[2] = returnObj;

            returnData = DBObj.ExecuteNonQuery("SetPostingCancelled_SP", allParams);

            return (string)returnObj.Value;
        }

    }
}

