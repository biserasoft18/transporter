﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using GenericHelper;


namespace TransporterData
{
    public class Masters
    {
        SqlHelper DBObj = new SqlHelper();
        //Constants Const = new Constants();

        private DataTable FetchMasterTableData(string storedProcName, int typeID)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            if (typeID != 0)
            {
                SqlParameter[] objParameter = new SqlParameter[1];
                objParameter[0] = new SqlParameter("@typeID", typeID);
                DBObj.Fill(ds, storedProcName, objParameter);
            }
            else
            {
                DBObj.Fill(ds, storedProcName);
            }
            
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }

        public DataTable FetchGoodType(int typeID)
        {            
            return FetchMasterTableData(Constants.SPName.GetGoodsType, typeID);
        }

        public DataTable FetchGoodWeight(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetGoodsWeight, typeID);
        }

        public DataTable FetchPackageType(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetPackageType, typeID);
        }

        public DataTable FetchPostingStatus(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetPostingStatus, typeID);
        }

        public DataTable FetchBookingStatus(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetBookingStatus, typeID);
        }

        public DataTable FetchUserType(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetUserType, typeID);
        }

        public DataTable FetchCity(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetCity, typeID);
        }

        public DataTable FetchPaymentType(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetPackageType, typeID);
        }

        public DataTable FetchState(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetState, typeID);
        }

        public DataTable FetchTown(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetTown, typeID);
        }

        public DataTable FetchCountry(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetCountry, typeID);
        }

        public DataTable FetchSecretQuestion(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetSecretQuestion, typeID);
        }

        public DataTable FetchTruckType(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetTruckType, typeID);
        }

        public DataTable FilterCity(string cityName)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@cityName", cityName);
            DBObj.Fill(ds, Constants.SPName.GetCityByFilter, objParameter);

            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }

        public DataSet GetBookingPageDetails(int bookingID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@bookingID", bookingID);
            DBObj.Fill(ds, Constants.SPName.GetBookingPageDetails, objParameter);            
            return ds;
        }

        public DataSet GetRegisterDetails(int UserID)
        {
            DataSet ds1 = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@UserID", UserID);
            DBObj.Fill(ds1, Constants.SPName.GetRegisterDetails, objParameter);
            return ds1;
        }


        public DataSet GetStateDetails(int CountryID)
        {
            DataSet ds2 = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@CountryID", CountryID);
            DBObj.Fill(ds2, Constants.SPName.GetStateDetails, objParameter);
            return ds2;
        }

        public DataSet GetBookingSearchDetails(int userID, Boolean needAllSearchData)
        {
            DataSet ds3 = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@LoggedInUserID", userID);
            objParameter[1] = new SqlParameter("@showSearchData", needAllSearchData);
            DBObj.Fill(ds3, Constants.SPName.GetSearchDetails, objParameter);
            return ds3;
        }

        public DataSet GetTransportSearchDetails(int userID, Boolean needAllSearchData)
        {
            DataSet ds3 = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@LoggedInUserID", userID);
            objParameter[1] = new SqlParameter("@showSearchData", needAllSearchData);
            DBObj.Fill(ds3, Constants.SPName.GetTransSearchDetails, objParameter);
            return ds3;
        }

        public DataSet GetDashBoardPageDetails(int UserID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[1];
            //objParameter[0] = new SqlParameter("@UserID", UserID);
            objParameter[0] = DBObj.CreateSQLParam(UserID, "@UserID");
            DBObj.Fill(ds, Constants.SPName.GetDashBoardPageDetails, objParameter);
            return ds;
        }

        public DataSet SearchforAgents(int SourceCityID, int DestinationCityID, DateTime DeliveryDate, int GoodsWeightID, int TruckTypeID, int BookingStatusID, int UserID, int BookingID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[8];
            objParameter[0] = new SqlParameter("@SourceCityID", SourceCityID);
            objParameter[1] = new SqlParameter("@DestinationCityID", DestinationCityID);
            objParameter[2] = new SqlParameter("@DeliveryDate", DeliveryDate);
            objParameter[3] = new SqlParameter("@GoodsWeightID", GoodsWeightID);
            objParameter[4] = new SqlParameter("@TruckTypeID", TruckTypeID);
            objParameter[5] = DBObj.CreateSQLParam(BookingStatusID.ToString(), 100, "@BookingStatusID");
            objParameter[6] = new SqlParameter("@UserID", UserID);
            objParameter[7] = new SqlParameter("@BookingID", BookingID);

            DBObj.Fill(ds, Constants.SPName.GetAgentSearchPageDetails, objParameter);
            return ds;
        }

        public DataSet GetPostingPageDetails(int postingID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@postingID", postingID);
            DBObj.Fill(ds, Constants.SPName.GetPostingPageDetails, objParameter);
            return ds;
        }

        public DataSet SearchforTransporters(int SourceCityID, int DestinationCityID, DateTime DeliveryDate, int GoodsWeightID, int TruckTypeID, int LoggedInUserID, decimal AvlCapacity, int PostingStatusID, int PostingID)
        {
            DataSet ds = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[9];
            objParameter[0] = new SqlParameter("@SourceCityID", SourceCityID);
            objParameter[1] = new SqlParameter("@DestinationCityID", DestinationCityID);
            objParameter[2] = new SqlParameter("@DeliveryDate", DeliveryDate);
            objParameter[3] = new SqlParameter("@GoodsWeightID", GoodsWeightID);
            objParameter[4] = new SqlParameter("@TruckTypeID", TruckTypeID);
            objParameter[5] = new SqlParameter("@UserID", LoggedInUserID);
            objParameter[6] = new SqlParameter("@AvlCapacity", AvlCapacity);
            objParameter[7] = new SqlParameter("@PostingID", PostingID);
            objParameter[8] = DBObj.CreateSQLParam(PostingStatusID.ToString(), 100, "@PostingStatusID");

            DBObj.Fill(ds, Constants.SPName.GetTransporterSearchPageDetails, objParameter);
            return ds;
        }

        public DataSet GetBookingResponseFromTransporter(int UserID)
        {
            DataSet ds1 = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@UserID", UserID);
            DBObj.Fill(ds1, Constants.SPName.GetBookingResponseFromTransporter, objParameter);
            return ds1;
        }

        public DataSet GetPostingResponseFromAgent(int UserID)
        {
            DataSet ds1 = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@UserID", UserID);
            DBObj.Fill(ds1, Constants.SPName.GetPostingResponseFromAgent, objParameter);
            return ds1;
        }

        public DataTable FetchBusinessType(int typeID)
        {
            return FetchMasterTableData(Constants.SPName.GetBusinessType, typeID);
        }
        

    }
}
