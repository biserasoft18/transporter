﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using GenericHelper;

namespace TransporterData
{
    public class Booking
    {
        SqlHelper DBObj = new SqlHelper();
        public string SetBookingData(DataTable dt, ref int bookingID, int userID)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[4];
            SqlParameter bookingData = new SqlParameter("@bookingData", SqlDbType.Structured)
                {
                    TypeName = "dbo.udt_BookingData",
                    Value = dt
                };
            allParams[0] = bookingData;

                      SqlParameter bookingIDParam = new SqlParameter();
            bookingIDParam.ParameterName = "@bookingID";
            bookingIDParam.DbType = DbType.Int32;
            bookingIDParam.Direction = ParameterDirection.InputOutput;
            bookingIDParam.Value = bookingID;
            allParams[1] = bookingIDParam;

            SqlParameter userIDParam = new SqlParameter();
            userIDParam.ParameterName = "@userID";
            userIDParam.DbType = DbType.Int32;
            userIDParam.Direction = ParameterDirection.Input;
            userIDParam.Value = userID;
            allParams[2] = userIDParam;

            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[3] = returnMessageParam;

            returnData = DBObj.ExecuteNonQuery("SetBookingData_SP", allParams);

            bookingID = (int)bookingIDParam.Value;

            //return returnMessageParam.Value.ToString();

            return (string)returnMessageParam.Value;
        }

        public string SetBookingAddress(DataTable dtPickup, DataTable dtDrop, int bookingID, int userID)
        {
            int returnData;

            SqlParameter[] allParams = new SqlParameter[5];

            SqlParameter pickupAddress = new SqlParameter("@pickupAddress", SqlDbType.Structured)
            {
                TypeName = "dbo.udt_BookingAddress",
                Value = dtPickup
            };
            allParams[0] = pickupAddress;

            SqlParameter dropAddress = new SqlParameter("@dropAddress", SqlDbType.Structured)
            {
                TypeName = "dbo.udt_BookingAddress",
                Value = dtDrop
            };
            allParams[1] = dropAddress;

            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[2] = returnMessageParam;

            allParams[3] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            allParams[4] = DBObj.CreateSQLParam(userID, "@userID");

            returnData = DBObj.ExecuteNonQuery("SetBookingAddress_SP", allParams);

            return (string)returnMessageParam.Value;

        }

        public string GetMessageToAgent(int bookingID, int curUserID, ref int PostingID)
        {
            int returnData;

            SqlParameter[] allParams = new SqlParameter[4];

            SqlParameter bookingIDParam = new SqlParameter();
            bookingIDParam.ParameterName = "@bookingID";
            bookingIDParam.DbType = DbType.Int32;
            bookingIDParam.Direction = ParameterDirection.Input;
            bookingIDParam.Value = bookingID;
            allParams[0] = bookingIDParam;

            SqlParameter userIDParam = new SqlParameter();
            userIDParam.ParameterName = "@loggedInUserID";
            userIDParam.DbType = DbType.Int32;
            userIDParam.Direction = ParameterDirection.Input;
            userIDParam.Value = curUserID;
            allParams[1] = userIDParam;

            SqlParameter returnMessage = new SqlParameter();
            returnMessage.ParameterName = "@messageToAgent";
            returnMessage.DbType = DbType.String;
            returnMessage.Size = 300;
            returnMessage.Direction = ParameterDirection.InputOutput;
            returnMessage.Value = "";
            allParams[2] = returnMessage;

            SqlParameter returnPostingID = new SqlParameter();
            returnPostingID.ParameterName = "@PostingID";
            returnPostingID.DbType = DbType.Int32;
            returnPostingID.Direction = ParameterDirection.InputOutput;
            returnPostingID.Value = 0;
            allParams[3] = returnPostingID;

            returnData = DBObj.ExecuteNonQuery("GetMessageToAgentForCurUser_SP", allParams);

            PostingID = Convert.ToInt32(returnPostingID.Value.ToString());

            return (string)returnMessage.Value;
        }

        public DataSet GetAllMessageToAgent(int bookingID, int curUserID, Boolean isReqToTransporter)
        {
            DataSet ds = new DataSet();

            SqlParameter[] allParams = new SqlParameter[3];

            allParams[0] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            allParams[1] = DBObj.CreateSQLParam(curUserID, "@loggedInUserID");
            allParams[2] = DBObj.CreateSQLParam(isReqToTransporter, "@isReqToTransporter");

            DBObj.Fill(ds, "GetAllMessagesToAgent_SP", allParams);
            return ds;
        }

        public string SetbookingMessage(string Message, int bookingID, int postingID, int userID, ref string agentEmailID, ref long agentMobileNumber)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[7];

            allParams[0] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            allParams[1] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[2] = DBObj.CreateSQLParam(userID, "@userID");
            allParams[3] = DBObj.CreateSQLParam(Message, 4000, "@messageToAgent");
            
            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[4] = returnMessageParam;

            SqlParameter returnAgentEmail = new SqlParameter();
            returnAgentEmail.ParameterName = "@returnAgentEmail";
            returnAgentEmail.DbType = DbType.String;
            returnAgentEmail.Size = 200;
            returnAgentEmail.Direction = ParameterDirection.Output;
            allParams[5] = returnAgentEmail;

            SqlParameter returnAgentMobile = new SqlParameter();
            returnAgentMobile.ParameterName = "@returnAgentMobile";
            returnAgentMobile.DbType = DbType.Int64;
            returnAgentMobile.Direction = ParameterDirection.Output;
            allParams[6] = returnAgentMobile;
            
            returnData = DBObj.ExecuteNonQuery("SetbookingMessage_SP", allParams);

            agentEmailID = (string)returnAgentEmail.Value;
            agentMobileNumber = (long)returnAgentMobile.Value;


            return (string)returnMessageParam.Value;

        }

        public string AcceptDeal(int TransporterID, int bookingID, int postingID, int loggedInUserID, ref string transporterEmailID, ref long transporterMobileNumber)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[7];
            
            allParams[0] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            allParams[1] = DBObj.CreateSQLParam(TransporterID, "@userID");
            allParams[2] = DBObj.CreateSQLParam(postingID, "@postingID");
            allParams[3] = DBObj.CreateSQLParam(loggedInUserID, "@loggedInUserID");
            
            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[4] = returnMessageParam;

            SqlParameter returnTranEmail = new SqlParameter();
            returnTranEmail.ParameterName = "@returnTransEmail";
            returnTranEmail.DbType = DbType.String;
            returnTranEmail.Size = 200;
            returnTranEmail.Direction = ParameterDirection.Output;
            allParams[5] = returnTranEmail;

            SqlParameter returnTransMobile = new SqlParameter();
            returnTransMobile.ParameterName = "@returnTransMobile";
            returnTransMobile.DbType = DbType.Int64;
            returnTransMobile.Direction = ParameterDirection.Output;
            allParams[6] = returnTransMobile;

            returnData = DBObj.ExecuteNonQuery("SetbookingDeal_SP", allParams);

            transporterEmailID = (string)returnTranEmail.Value;
            transporterMobileNumber = (long)returnTransMobile.Value;

            return (string)returnMessageParam.Value;
        }

        public List<int> GetBookingIDList(int curUserID)
        {
            List<int> returnObj = new List<int>();
            DataSet ds = new DataSet();

            SqlParameter[] allParams = new SqlParameter[1];


            SqlParameter userIDParam = new SqlParameter();
            userIDParam.ParameterName = "@loggedInUserID";
            userIDParam.DbType = DbType.Int32;
            userIDParam.Direction = ParameterDirection.Input;
            userIDParam.Value = curUserID;
            allParams[0] = userIDParam;


            DBObj.Fill(ds, "GetBookingIDList_SP", allParams);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        returnObj.Add(Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString()));
                    }
                }
            }
            return returnObj;
        }

        public string CancelBooking(int bookingID, int loggedInUserID)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[3];

            allParams[0] = DBObj.CreateSQLParam(bookingID, "@bookingID");
            allParams[1] = DBObj.CreateSQLParam(loggedInUserID, "@loggedInUserID");
            SqlParameter returnObj = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[2] = returnObj;

            returnData = DBObj.ExecuteNonQuery("SetBookingCancelled_SP", allParams);

            return (string)returnObj.Value;
        }

    }


}

