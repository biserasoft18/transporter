﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using GenericHelper;

namespace TransporterData
{
    public class Reports
    {
        SqlHelper DBObj = new SqlHelper();

        public string GetGenericReportDetails(string reportName, int loggedInUserID)
        {
            string returnVal = "";
            DataSet ds1 = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@reportName", reportName);
            objParameter[1] = new SqlParameter("@loggedInUserID", loggedInUserID);
            DBObj.Fill(ds1, Constants.SPName.GetGenericReportDetails, objParameter);
            if(ds1.Tables.Count> 0)
            {
                if(ds1.Tables[0].Rows.Count > 0)
                {
                    returnVal = ds1.Tables[0].Rows[0]["ReportXML"].ToString();
                }
            }
            return returnVal;
        }

        public DataTable GetGenericReportData(string SPName, SqlParameter[] SPparams)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            if (SPName != null || SPName != "")
            {
                DBObj.Fill(ds, SPName, SPparams);
            }

            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }
    }
}
