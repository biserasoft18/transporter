﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace TransporterData
{
    public class PaymentDetails
    {
        SqlHelper DBObj = new SqlHelper();

        public DataSet GetUserPaymentDetails(int userID, int loggedInUserID, ref string outValue)
        {
            DataTable dt = new DataTable();
            SqlParameter[] allParams = new SqlParameter[3];

            allParams[0] = DBObj.CreateSQLParam(userID, "@UserID");
            allParams[1] = DBObj.CreateSQLParam(loggedInUserID,"@LogInUserID");
            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[2] = returnMessageParam;
            
            DataSet ds = new DataSet();
            DBObj.Fill(ds, GenericHelper.Constants.SPName.GetUserPaymentDetails, allParams);
            outValue = (string)returnMessageParam.Value;
            
            return ds;
        }

        public string UpsertPaymentDetails(int userID, string curExpiryDate, string reqExpiryDate, Decimal paymentAmount, string extTransId, string extPaymId, string redirectURL,
            string webhookURL, string paymentURL, string initiateOn, string acknowOn, string finalRespOn, 
            string extMacId, string extPayRespId, int paymentStatus, string webHookResponse, ref int intPaymentId)
        {
            int returnData = 0;

            SqlParameter[] allParams = new SqlParameter[18];

            allParams[0] = DBObj.CreateSQLParam(userID, "@UserID");
            allParams[9] = DBObj.CreateSQLParam(paymentStatus, "@PaymentStatusID");
            //allParams[10] = DBObj.CreateSQLParam(intPaymentId, "@IntPaymentID");
            SqlParameter returnIntPaymentID = new SqlParameter();
            returnIntPaymentID.ParameterName = "@IntPaymentID";
            returnIntPaymentID.DbType = DbType.Int32;
            returnIntPaymentID.Direction = ParameterDirection.InputOutput;
            returnIntPaymentID.Value = intPaymentId;
            allParams[10] = returnIntPaymentID;

            allParams[2] = DBObj.CreateSQLParam(extTransId, 100, "@ExtTransID");
            allParams[3] = DBObj.CreateSQLParam(extPaymId, 100, "@ExtPaymentID");
            allParams[4] = DBObj.CreateSQLParam(redirectURL, 1000, "@RedirectURL");
            allParams[5] = DBObj.CreateSQLParam(webhookURL, 1000, "@WebhookURL");
            allParams[6] = DBObj.CreateSQLParam(paymentURL, 1000, "@PaymentURL");
            allParams[7] = DBObj.CreateSQLParam(extMacId, 200, "@ExtMacID");
            allParams[8] = DBObj.CreateSQLParam(extPayRespId, 1000, "@ExtPayResponseID");
            allParams[11] = DBObj.CreateSQLParam(paymentAmount, "@Amount");
            allParams[12] = DBObj.CreateSQLParam(curExpiryDate, 100, "@CurExpiryDate");
            allParams[13] = DBObj.CreateSQLParam(reqExpiryDate, 100, "@ReqExpiryDate");
            allParams[14] = DBObj.CreateSQLParam(initiateOn, 100, "@InitiatedOn");
            allParams[15] = DBObj.CreateSQLParam(acknowOn, 100, "@PayAckOn");
            allParams[16] = DBObj.CreateSQLParam(finalRespOn, 100, "@FinalResponseRecvOn");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            
            allParams[17] = returnMessageParam;
            allParams[1] = DBObj.CreateSQLParam(webHookResponse, 4000, "@webHookResponse");

            returnData = DBObj.ExecuteNonQuery("UpsertUserPaymentDetails_SP", allParams);
            intPaymentId = (int)returnIntPaymentID.Value;

            return (string)returnMessageParam.Value;
        }
    }
}
