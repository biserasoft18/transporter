﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace TransporterData
{
    public class UserData
    {
        SqlHelper DBObj = new SqlHelper();        

        public DataSet ValidateUser(string Username, string Password)
        {
            DataSet ds = new DataSet();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@Username", Username);
            objParameter[1] = new SqlParameter("@Password", Password);
            DBObj.Fill(ds, GenericHelper.Constants.SPName.ValidateUser, objParameter);
            //if (ds.Tables != null)
            //{
            //    if (ds.Tables.Count > 0)
            //    {
            //        dt = ds.Tables[0];
            //    }
            //}
            return ds;
        }

        public DataTable GetUserPassword(string username, string EmailID, string secpasw)
        {
            DataTable dt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[3];
            objParameter[0] = new SqlParameter("@Username", username);
            objParameter[1] = new SqlParameter("@EmailID", EmailID);
            objParameter[2] = new SqlParameter("@secpasw", secpasw);
            DataSet ds = new DataSet();
            DBObj.Fill(ds, GenericHelper.Constants.SPName.GetPasswordforUser, objParameter);
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }


        public DataTable ValidateLoginID(string LoginID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@LoginID", LoginID);
            DataSet ds = new DataSet();
            DBObj.Fill(ds, GenericHelper.Constants.SPName.ValidateLoginID, objParameter);
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }

        public string SetRegisterData(DataTable dt, ref int UserID)
        {
            int returnData = 0;

            SqlParameter[] allParams = new SqlParameter[3];

            SqlParameter RegisterationData = new SqlParameter("@UserData", SqlDbType.Structured)
            {
                TypeName = "dbo.udt_UserData",
                Value = dt
            };
            allParams[0] = RegisterationData;
            //allParams[0] = DBObj.CreateSQLParam(dt, "dbo.udt_UserData", "@UserData");

            SqlParameter UserIDParam = new SqlParameter();
            UserIDParam.ParameterName = "@UserID";
            UserIDParam.DbType = DbType.Int32;
            UserIDParam.Direction = ParameterDirection.InputOutput;
            UserIDParam.Value = UserID;
            allParams[1] = UserIDParam;

            SqlParameter returnMessageParam = new SqlParameter();
            returnMessageParam.ParameterName = "@returnMessage";
            returnMessageParam.DbType = DbType.String;
            returnMessageParam.Size = 300;
            returnMessageParam.Direction = ParameterDirection.Output;
            allParams[2] = returnMessageParam;

            returnData = DBObj.ExecuteNonQuery("SetRegisterationData_SP", allParams);

            UserID = (int)UserIDParam.Value;

            return (string)returnMessageParam.Value;

            }

        public string SetDriverData(DataTable dt, int userID)
        {
            int returnData = 0;

            SqlParameter[] allParams = new SqlParameter[3];

            allParams[0] = DBObj.CreateSQLParam(dt, "dbo.udt_DriverDetails", "@driverInfo");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");

            allParams[1] = DBObj.CreateSQLParam(userID, "@loggedInUserID");
            allParams[2] = returnMessageParam;

            returnData = DBObj.ExecuteNonQuery("SetDriverData_SP", allParams);

            //driverID = (int)driverIDParam.Value;
            
            return (string)returnMessageParam.Value;
        }

        public DataTable GetDriverDetails(int driverID, int loggedInUserID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[2];
            objParameter[0] = new SqlParameter("@driverID", driverID);
            objParameter[1] = new SqlParameter("@loggedInUserID", loggedInUserID);
            DataSet ds = new DataSet();
            DBObj.Fill(ds, GenericHelper.Constants.SPName.GetDriverDetailsForTransp, objParameter);
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }

        public string ValidateAppLogin(string appID, ref string outVal, ref int driverUserID, ref string userToken)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[4];

            allParams[0] = DBObj.CreateSQLParam(appID, 100, "@appID");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[1] = returnMessageParam;

            SqlParameter outIntparam = new SqlParameter();
            outIntparam.ParameterName = "@driverUserID";
            outIntparam.DbType = DbType.Int32;
            outIntparam.Direction = ParameterDirection.Output;
            outIntparam.Value = driverUserID;

            allParams[2] = outIntparam;

            SqlParameter outTockenParam = new SqlParameter();
            outTockenParam.ParameterName = "@userToken";
            outTockenParam.DbType = DbType.String;
            outTockenParam.Size = 100;
            outTockenParam.Direction = ParameterDirection.Output;
            outTockenParam.Value = userToken;

            allParams[3] = outTockenParam;


            returnData = DBObj.ExecuteNonQuery("CheckAppLogin_SP", allParams);
            userToken = "";
            if (returnMessageParam.Value.ToString().ToUpper() == "SUCCESS")
            {
                driverUserID = (int)outIntparam.Value;
                userToken = (string)outTockenParam.Value;
            }


            outVal = (string)returnMessageParam.Value;


            return outVal;
        }

        public string GenerateDriverRegOTP(int transporterID, Int64 driverMobileNum, ref int OTPNumber)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[4];

            allParams[0] = DBObj.CreateSQLParam(transporterID, "@transporterID");

            allParams[1] = DBObj.CreateSQLParam(driverMobileNum, "@driverMobileNum");
            
            SqlParameter outIntparam = new SqlParameter();
            outIntparam.ParameterName = "@OTPNumber";
            outIntparam.DbType = DbType.Int32;
            outIntparam.Direction = ParameterDirection.Output;
            outIntparam.Value = returnData;

            allParams[2] = outIntparam;

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");

            allParams[3] = returnMessageParam;

            DBObj.ExecuteNonQuery("GenerateDriverRegOTP_SP", allParams);

            OTPNumber = (int)outIntparam.Value;

            return (string)returnMessageParam.Value;
        }

        public string RegisterDriverAPP(string appID, int transporterID, Int64 driverMobileNum, int OTPNumber)
        {
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[5];

            allParams[0] = DBObj.CreateSQLParam(transporterID, "@transporterID");

            allParams[1] = DBObj.CreateSQLParam(driverMobileNum, "@driverMobileNum");

            allParams[2] = DBObj.CreateSQLParam(OTPNumber, "@OTPNumber");

            SqlParameter returnMessageParam = DBObj.CreateOutSQLParam(300, "@returnMessage");

            allParams[3] = returnMessageParam;

            allParams[4] = DBObj.CreateSQLParam(appID, 100, "@appID");

            returnData = DBObj.ExecuteNonQuery("RegisterDriverApp_SP", allParams);
            
            return (string)returnMessageParam.Value;
        }

        public string SetUserLog(DataTable dt, int userID, string environmentDetails)
        {
            int returnData = 0;

            SqlParameter[] allParams = new SqlParameter[4];

            //SqlParameter RegisterationData = new SqlParameter("@UserData", SqlDbType.Structured)
            //{
            //    TypeName = "dbo.udt_UserLogInData",
            //    Value = dt
            //};
            //allParams[0] = RegisterationData;
            allParams[0] = DBObj.CreateSQLParam(dt, "dbo.udt_UserLogInData", "@UserData");

            allParams[1] = DBObj.CreateSQLParam(userID, "@UserID");

            SqlParameter outParam = DBObj.CreateOutSQLParam(300, "@returnMessage");
            allParams[2] = outParam;
            
            allParams[3] = DBObj.CreateSQLParam(environmentDetails, 300, "@environmentDetails");

            returnData = DBObj.ExecuteNonQuery("SetUserLoginData_SP", allParams);            

            return (string)outParam.Value;
        }

    }
}
