﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using GenericHelper;

namespace TransporterData
{
    public class Communicator
    {
        SqlHelper DBObj = new SqlHelper();
        public string LogCommunicationStatus(string MsgCode, string Response, Boolean IsSuccess, string MsgType, int LoggedInUserID, int ReferenceID, string ReferenceType, string MsgContent, string Recepient)
        {
            string retValue = "Failure";
            int returnData = 0;
            SqlParameter[] allParams = new SqlParameter[9];
            allParams[0] = DBObj.CreateSQLParam(LoggedInUserID, "@LoggedInUserID");
            allParams[1] = DBObj.CreateSQLParam(ReferenceID, "@ReferenceID");
            allParams[2] = DBObj.CreateSQLParam(MsgType, 100, "@MsgType");
            allParams[3] = DBObj.CreateSQLParam(MsgCode, 1000, "@MsgCode");
            allParams[4] = DBObj.CreateSQLParam(IsSuccess, "@IsSuccess");
            allParams[5] = DBObj.CreateSQLParam(Response, 8000, "@MsgResponse");
            allParams[6] = DBObj.CreateSQLParam(MsgContent, 8000, "@MsgContent");
            allParams[7] = DBObj.CreateSQLParam(ReferenceType, 100, "@ReferenceIDType");
            allParams[8] = DBObj.CreateSQLParam(Recepient, 100, "@Recepient");

            returnData = DBObj.ExecuteNonQuery("SetMessageLog_SP", allParams);

            retValue = "Success";
            
            return retValue;
        }
    }
}
