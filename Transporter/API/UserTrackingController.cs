﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransporterCore;
using Newtonsoft.Json.Linq;
using TransporterModel;

namespace Transporter.API
{
    [RoutePrefix("api/UserTracking")]
    public class UserTrackingController : BaseAPIController
    {
        [HttpGet]
        [Route("TrackLoggedInUserDetails/{userID}/{ipaddress}/{environmentDet}")]
        public HttpResponseMessage TrackLoggedInUserDetails(int userID, string ipaddress, string environmentDet)
        {
            //var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            //string retVal = "";
            string URL = "http://api.ipstack.com/" + ipaddress + "?access_key=" + ConfigurationManager.AppSettings["GeoLocationApiKey"].ToString();
            //string DATA = @"{
            //    ""name"": ""Component 2"",
            //    ""description"": ""This is a JIRA component"",
            //    ""leadUserName"": ""xx"",
            //    ""assigneeType"": ""PROJECT_LEAD"",
            //    ""isAssigneeTypeValid"": false,
            //    ""project"": ""TP""}";

            try
            {
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                client.BaseAddress = new System.Uri(URL);
                byte[] cred = System.Text.UTF8Encoding.UTF8.GetBytes("username:password");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                System.Net.Http.HttpContent content = new StringContent("", System.Text.UTF8Encoding.UTF8, "application/json");
                HttpResponseMessage messge = client.PostAsync(URL, content).Result;
                string description = string.Empty;
                if (messge.IsSuccessStatusCode)
                {
                    string result = messge.Content.ReadAsStringAsync().Result;
                    description = result;                    
                }

                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(LogUserDetails(description, environmentDet, userID), System.Text.Encoding.UTF8, "application/json");
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message, System.Text.Encoding.UTF8, "application/json");
                return response;
            }
        }
        
        private string LogUserDetails(string IPDetails, string environmentDetails, int userID)
        {
            JObject jObject = JObject.Parse(IPDetails);
            UserLog logObj = new UserLog();
            logObj.IPAddress = (string)jObject["ip"];
            logObj.IPType = (string)jObject["type"];
            logObj.ContinentCode = (string)jObject["continent_code"];
            logObj.CountryCode = (string)jObject["country_code"];
            logObj.CountryName = (string)jObject["country_name"];
            logObj.RegionCode = (string)jObject["region_code"];
            logObj.RegionName = (string)jObject["region_name"];
            logObj.CityName = (string)jObject["city"];
            logObj.ZipCode = (string)jObject["zip"];
            if(logObj.ZipCode == null)
            {
                logObj.ZipCode = (string)jObject["postal"];
            }
            logObj.Latitude = (string)jObject["latitude"];
            logObj.Longitude = (string)jObject["longitude"];
            logObj.Environment = environmentDetails;
            logObj.FullTrace = IPDetails;
            logObj.UserID = userID;


            UserBL User = new UserBL();
            return User.LogUserDetails(logObj, userID, environmentDetails);
        }

        public string SetUserGeoLogs(string GeoTags, int UserID)
        {
            try
            {
                return LogUserDetails(GeoTags, "", UserID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }
    }
}
