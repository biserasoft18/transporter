﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransporterCore;
using TransporterModel;

namespace Transporter.API
{
    [RoutePrefix("api/Posting")]
    public class PostingAPIController : API.BaseAPIController
    {        
        [HttpGet]
        [Route("GetUserPostings/{userID}")]
        public IEnumerable<LoadTransporterSearchDetail> GetUserPostings(int userID)
        {
            PostingCreateBL returnObj = new PostingCreateBL();
            List<int> bookingList = new List<int>();
            DataTable dt = new DataTable();
            DateTime tempDate;
            tempDate = Convert.ToDateTime("01/01/1900");
            dt = returnObj.SearchforTransport(0, 0, tempDate, 0, 0, userID, 0,0, 0);
            List<LoadTransporterSearchDetail> SearchList = new List<LoadTransporterSearchDetail>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                LoadTransporterSearchDetail LoadSearchAgentDet = new LoadTransporterSearchDetail();
                LoadSearchAgentDet.PostingID = Convert.ToInt32(dt.Rows[i]["PostingID"]);
                LoadSearchAgentDet.SourceCity = dt.Rows[i]["SourceCity"].ToString();
                LoadSearchAgentDet.DestinationCity = dt.Rows[i]["DestinationCity"].ToString();
                LoadSearchAgentDet.DeliveryDate = dt.Rows[i]["DeliveryDate"].ToString();
                LoadSearchAgentDet.GoodsWeightName = dt.Rows[i]["GoodsWeightName"].ToString();
                LoadSearchAgentDet.AvailableTonnage = (decimal)dt.Rows[i]["AvailableTonnage"];
                LoadSearchAgentDet.TruckTypeName = dt.Rows[i]["TruckTypeName"].ToString();
                SearchList.Add(LoadSearchAgentDet);
            }
            return SearchList;
        }

        private DriverTripData GetTripDataForApp(int driverID, string appID, string userToken, ref string returnString)
        {
            PostingCreateBL returnObj = new PostingCreateBL();
            DataSet ds = new DataSet();
            ds = returnObj.GetDriverTripData(driverID, appID, userToken, ref returnString);
            DriverTripData TripData = new DriverTripData();
            #region "Fill Trip Data from dataset"

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    // Fill Header details
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    TripData HeaderData = new TripData();
                    HeaderData.PostingID = Convert.ToInt32(dt.Rows[0]["PostingID"]);
                    HeaderData.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
                    HeaderData.TruckNumber = dt.Rows[0]["TruckNumber"].ToString();
                    HeaderData.StartDate = dt.Rows[0]["StartDate"].ToString();
                    HeaderData.DeliveryDate = dt.Rows[0]["DeliveryDate"].ToString();
                    HeaderData.DriverName = dt.Rows[0]["DriverName"].ToString();
                    HeaderData.SourceCity = dt.Rows[0]["SourceCity"].ToString();
                    HeaderData.DestinationCity = dt.Rows[0]["DestinationCity"].ToString();
                    HeaderData.GoodsWeightName = dt.Rows[0]["GoodsWeightName"].ToString();
                    HeaderData.AvailableTonnage = dt.Rows[0]["AvailableTonnage"].ToString();
                    HeaderData.SourceCityLat = dt.Rows[0]["SourceCityLat"].ToString();
                    HeaderData.SourceCityLon = dt.Rows[0]["SourceCityLon"].ToString();
                    HeaderData.DestinationCityLat = dt.Rows[0]["DestinationCityLat"].ToString();
                    HeaderData.DestinationCityLon = dt.Rows[0]["DestinationCityLon"].ToString();
                    HeaderData.ViaRoutes = dt.Rows[0]["ViaRoutes"].ToString();

                    TripData.TripMainDetails = HeaderData;

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        // Fill PickUp and Drop details
                        TripData.TripDetails = new List<PickAndDropDetails>();
                        dt = new DataTable();
                        dt = ds.Tables[1];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            PickAndDropDetails TripDetail = new PickAndDropDetails();
                            TripDetail.PostingID = Convert.ToInt32(dt.Rows[j]["PostingID"]);

                            TripDetail.BookingID = dt.Rows[j]["BookingID"].ToString();
                            TripDetail.StartCityID = Convert.ToInt32(dt.Rows[j]["StartCityID"]);
                            TripDetail.DestinationCityID = Convert.ToInt32(dt.Rows[j]["DestinationCityID"]);
                            TripDetail.BookingSortOrder = Convert.ToInt32(dt.Rows[j]["BookingSortOrder"]);
                            TripDetail.AgentNumber = dt.Rows[j]["AgentNumber"].ToString();
                            TripDetail.LRNumber = dt.Rows[j]["LRNumber"].ToString();
                            TripDetail.TruckNumber = dt.Rows[j]["TruckNumber"].ToString();
                            TripDetail.AgentName = dt.Rows[j]["AgentName"].ToString();
                            TripDetail.StartCityName = dt.Rows[j]["StartCityName"].ToString();
                            TripDetail.DestinationCityName = dt.Rows[j]["DestinationCityName"].ToString();
                            TripDetail.PickupLat = dt.Rows[j]["PickupLat"].ToString();
                            TripDetail.PickupLon = dt.Rows[j]["PickupLon"].ToString();
                            TripDetail.DropLat = dt.Rows[j]["DropLat"].ToString();
                            TripDetail.DropLon = dt.Rows[j]["DropLon"].ToString();
                            TripDetail.SourceCityLat = dt.Rows[j]["SourceCityLat"].ToString();
                            TripDetail.SourceCityLon = dt.Rows[j]["SourceCityLon"].ToString();
                            TripDetail.DestinationCityLat = dt.Rows[j]["DestinationCityLat"].ToString();
                            TripDetail.DestinationCityLon = dt.Rows[j]["DestinationCityLon"].ToString();
                            TripDetail.GoodsWeight = dt.Rows[j]["GoodsWeightName"].ToString();
                            TripDetail.GoodsType = dt.Rows[j]["GoodsTypeName"].ToString();
                            TripDetail.PickupAddress = dt.Rows[j]["PickupAddress"].ToString();
                            TripDetail.DropAddress = dt.Rows[j]["DropAddress"].ToString();
                            TripDetail.AgentEmail = dt.Rows[j]["AgentEmail"].ToString();

                            TripData.TripDetails.Add(TripDetail);
                        }
                    }
                }
            }
            #endregion

            return TripData;
        }

        [HttpGet]
        [Route("GetDriverTripData/{driverID}/{appID}")]
        public HttpResponseMessage GetDriverTripData(int driverID, string appID)
        {
            var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            string retVal = "";

            try
            {
                
                string returnString = string.Empty;

                DriverTripData TripData = GetTripDataForApp(driverID, appID, GetRequestHeader(Request, "UserToken"), ref retVal);

                if ((TripData.TripDetails == null)  || (TripData.TripMainDetails == null)){
                    return SetReturnMessage(false, retVal, "", "");
                }

                //retVal = JsonConvert.SerializeObject(TripData);
                //retVal = retVal.Replace(@"\", "");

                return SetReturnMessage(true, "Success", "", TripData);

                //response = Request.CreateResponse(HttpStatusCode.OK);
                //response.Content = new StringContent(retVal, System.Text.Encoding.UTF8, "application/json");
                //return response;
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }            
        }
        
        [HttpGet]
        [Route("UpdateTripSheet/{driverID}/{appID}/{postingID}/{bookingID}/{sortOrder}/{latid}/{longit}/{PickupOrDrop}")]
        public HttpResponseMessage UpdateTripSheet(int driverID, string appID, int postingID, int bookingID, int sortOrder, string latid, string longit, int PickupOrDrop)
        {
            var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            string retVal = "";

            try
            {
                PostingCreateBL returnObj = new PostingCreateBL();
                DataSet ds = new DataSet();
                string returnString = string.Empty;
                returnObj.SetTripData(driverID, appID, postingID, bookingID, sortOrder, latid, longit, PickupOrDrop, GetRequestHeader(Request, "UserToken"), ref retVal);

                DriverTripData returnData = new DriverTripData();
                returnData = GetTripDataForApp(driverID, appID, GetRequestHeader(Request, "UserToken"), ref retVal);


                if ((returnData.TripDetails == null) || (returnData.TripMainDetails == null))
                {
                    return SetReturnMessage(false, "No data found", "", "");
                }

                try
                {
                    var agentNumber = returnData.TripDetails.Where(m => bookingID.ToString().Equals(m.BookingID)).FirstOrDefault().AgentNumber;
                    var agentEmail = returnData.TripDetails.Where(m => bookingID.ToString().Equals(m.BookingID)).FirstOrDefault().AgentEmail;
                    var pickupAddress= returnData.TripDetails.Where(m => bookingID.ToString().Equals(m.BookingID)).FirstOrDefault().PickupAddress.ToString();
                    var dropAddress = returnData.TripDetails.Where(m => bookingID.ToString().Equals(m.BookingID)).FirstOrDefault().DropAddress.ToString();
                    //PickupOrDrop = 1 (Pickup)
                    //PickupOrDrop = 2 (Drop)
                    if (PickupOrDrop == 1)
                    {
                        string commStatus = SendCommunication("GoodsPickedUp", agentEmail, Convert.ToInt64(agentNumber), "Your goods related to bookingID: " + bookingID.ToString() + " is picked up", "GoodsPickedUp", postingID, bookingID.ToString(), pickupAddress, DateTime.Now.ToString("dd-MMM-yyyy"), "PostingID", driverID);
                    }
                    else
                    {
                        string commStatus = SendCommunication("GoodsDelivered", agentEmail, Convert.ToInt64(agentNumber), "Your goods related to bookingID: " + bookingID.ToString() + " is delivered", "GoodsDelivered", postingID, bookingID.ToString(), dropAddress, DateTime.Now.ToString("dd-MMM-yyyy"), "PostingID", driverID);
                    }
                }
                catch (Exception ex){
                    log.Error("Send communication error from PostingAPI - UpdateTripSheet() method: " + ex.Message.ToString());
                }
                //retVal = JsonConvert.SerializeObject(returnData);
                //retVal = retVal.Replace(@"\", "");

                return SetReturnMessage(true, "Success", "", returnData);

                //response = Request.CreateResponse(HttpStatusCode.OK);
                //response.Content = new StringContent(retVal, System.Text.Encoding.UTF8, "application/json");
                //return response;
            }
            catch(Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

        [HttpGet]
        [Route("EndTrip/{driverID}/{appID}/{postingID}")]
        public HttpResponseMessage EndTrip(int driverID, string appID, int postingID)
        {
            string retVal = "";
            try
            {

                string returnString = string.Empty;
                PostingCreateBL returnObj = new PostingCreateBL();
                retVal = returnObj.EndTrip(driverID, appID, postingID, GetRequestHeader(Request, "UserToken"));
                if(retVal.ToUpper() == "SUCCESS")
                {
                    return SetReturnMessage(true, "This trip is closed", "", "");
                }
                else
                {
                    return SetReturnMessage(false, retVal, "", "");
                }
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

        [HttpGet]
        [Route("ResetTrip/{postingID}")]
        public HttpResponseMessage ResetTrip(int postingID)
        {
            string retVal = "";
            try
            {

                string returnString = string.Empty;
                PostingCreateBL returnObj = new PostingCreateBL();
                retVal = returnObj.ResetTrip(postingID);
                if (retVal.ToUpper() == "SUCCESS")
                {
                    return SetReturnMessage(true, "This trip is reset", "", "");
                }
                else
                {
                    return SetReturnMessage(false, retVal, "", "");
                }
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

        
        [HttpGet]
        [Route("GetTrackingDetails/{postingID}")]
        public HttpResponseMessage GetTrackingDetails(int postingID)
        {
            var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            try
            {
                List<TrackingDetails> retObj = new List<TrackingDetails>();
                string returnString = string.Empty;
                PostingCreateBL returnObj = new PostingCreateBL();
                retObj = returnObj.GetTrackingDetails(postingID);
                return SetReturnMessage(true, "Success", "", retObj);
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

        [HttpGet]
        [Route("SetVehicleTrackingInfo/{driverID}/{appID}/{postingID}/{latid}/{longit}")]
        public HttpResponseMessage SetVehicleTrackingInfo(int driverID, string appID, int postingID, string latid, string longit)
        {
            string retVal = "";
            try
            {
                PostingCreateBL returnObj = new PostingCreateBL();
                DataSet ds = new DataSet();
                string returnString = string.Empty;
                returnObj.SetVehicleTrackingInfo(driverID, appID, postingID, latid, longit, GetRequestHeader(Request, "UserToken"), ref retVal);

                if (retVal.ToUpper().Contains("SUCCESS"))
                {
                    return SetReturnMessage(true, "Success", "", retVal);
                }
                else
                {
                    return SetReturnMessage(false, "Failure", "", retVal);
                }
                
                
                
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }
    }
}