﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Http;
using TransporterCore;
using TransporterModel;

namespace Transporter.API
{
    [RoutePrefix("api/Booking")]
    public class BookingAPIController : API.BaseAPIController
    {

        [HttpGet]
        [Route("GetUserBookings/{userID}")]
        public IEnumerable<LoadSearchAgentDet> GetUserBookings(int userID)
        {
            BookingCreateBL returnObj = new BookingCreateBL();
            List<int> bookingList = new List<int>();
            DataTable dt = new DataTable();
            DateTime tempDate;
            tempDate = Convert.ToDateTime("01/01/1900");
            dt = returnObj.SearchforAgents(0, 0, tempDate, 0, 0, 0, userID,0);
            List<LoadSearchAgentDet> SearchList = new List<LoadSearchAgentDet>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                LoadSearchAgentDet LoadSearchAgentDet = new LoadSearchAgentDet();
                LoadSearchAgentDet.BookingID = Convert.ToInt32(dt.Rows[i]["BookingID"]);
                LoadSearchAgentDet.SourceCity = dt.Rows[i]["SourceCityName"].ToString();
                LoadSearchAgentDet.DestinationCity = dt.Rows[i]["DestinationCityName"].ToString();
                LoadSearchAgentDet.DeliveryDate = dt.Rows[i]["DeliveryDate"].ToString();
                LoadSearchAgentDet.GoodsWeightName = dt.Rows[i]["GoodsWeightName"].ToString();
                LoadSearchAgentDet.TruckTypeName = dt.Rows[i]["TruckTypeName"].ToString();
                SearchList.Add(LoadSearchAgentDet);
            }
            return SearchList;
        }
    }
}
