﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Http;
using TransporterCore;
using TransporterModel;

namespace Transporter.Controllers
{
    [RoutePrefix("api/Generic")]
    public class GenericController : API.BaseAPIController

    {
        City[] citylist = new City[]
         {
            new City { CityID = 1, CityName = "Chennai", StateName = "TamilNadu", StateID= 1 },
            new City { CityID = 2, CityName = "Trivandrum", StateName = "Kerala", StateID= 2 },
            new City { CityID = 3, CityName = "Bangalore", StateName = "Karnataka", StateID= 3 }
         };


        [HttpGet]
        [Route("GetCities")]
        public IEnumerable<City> GetAllCities()
        {
            List<City> cityFilterList = new List<City>();
            PopulateMasters CityObj = new PopulateMasters();
            cityFilterList = CityObj.FetchCity(0);
            return cityFilterList;
        }

        [HttpGet]
        [Route("GetCitiesByName/{cityName}")]
        public IEnumerable<City> GetCitiesByName(string cityName)
        {
            List<City> cityFilterList = new List<City>();
            PopulateMasters CityObj = new PopulateMasters();
            cityFilterList = CityObj.FilterCity(cityName);
            return cityFilterList;
        }

        [HttpGet]
        [Route("FilterCities/{cityName}")]
        public IHttpActionResult GetCities (string cityName)
        {
            //var cities = citylist.FirstOrDefault((p) => p.CityName.Contains(cityName));
            var cities = citylist.Where(item => cityName.Any((inneritem)=> item.CityName.Contains(cityName)));
            if (cities == null || !cities.Any())
            {
                //return NotFound();
                return Content(HttpStatusCode.NotFound, "No data found!");
            }
            return Ok(Json(cities));
        }  
        
        [HttpPost]
        [Route("SetViaCities/{CityName, CityID}")]
        public IHttpActionResult SetViaCities (int cityID)
        {
            string cityName="";
            var cities = citylist.Where(item => cityName.Any((inneritem) => item.CityName.Contains(cityName)));
            if (cities == null || !cities.Any())
            {
                //return NotFound();
                return Content(HttpStatusCode.NotFound, "No data found!");
            }
            return Ok(Json(cities));
        }

        [HttpGet]
        [Route("ValidateLogin/{UserID}/{Password}")]
        public IHttpActionResult ValidateLogin(string userID, string password)
        {
            string output = "";

            UserBL UserLayer = new UserBL();
            List<User> UserObj = new List<User>();
            List<ReportsPageMenu> ReportMenu = new List<ReportsPageMenu>();
            output = UserLayer.ValidateLogin(userID, password, ref UserObj, ref ReportMenu);
            if (output.ToUpper() == "VALID")
            {
                return Content(HttpStatusCode.OK, "Login successfull");
            }
            else
            {
                return Content(HttpStatusCode.ExpectationFailed, "Invalid login");
            }
        }
        
    }
}
