﻿using GenericHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransporterCore;
using System.Threading.Tasks;


namespace Transporter.API
{
    [RoutePrefix("api/AppSignIn")]
    public class AppSignInController : API.BaseAPIController
    {

        [HttpGet]
        [Route("CheckAppSignIn/{appID}")]
        public HttpResponseMessage CheckAppSignIn(string appID)
        {
            try
            {
                UserBL returnObj = new UserBL();

                string returnString = string.Empty;
                string userToken = string.Empty;
                int driverUserId = 0;
                returnObj.ValidateAppLogin(appID, ref returnString, ref driverUserId, ref userToken);

                if (returnString.ToUpper() != "SUCCESS")
                {
                    return SetReturnMessage(false, returnString, "", "");
                }

                MobileUser returnData = new MobileUser();
                returnData.UserID = driverUserId;
                returnData.ValidityDate = DateTime.Now.AddDays(1).ToString();
                returnData.UserToken = userToken;

                //retVal = JsonConvert.SerializeObject(returnData, Formatting.Indented);
                //retVal = retVal.Replace(@"\", "");

                return SetReturnMessage(true, "Success", "", returnData);
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

        [HttpGet]
        [Route("GenerateDriverRegOTP/{transporterCode}/{driverMobileNumer}")]
        public HttpResponseMessage GenerateDriverRegOTP(string transporterCode, string driverMobileNumer)
        {
            var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            try
            {
                UserBL driverObj = new UserBL();

                int transporterID = Convert.ToInt32(transporterCode);
                long driverMobileNum = Convert.ToInt64(driverMobileNumer);

                string returnString = string.Empty;
                int OTPNumber = 0;
                Int64 driverMobile = Convert.ToInt64(driverMobileNum);
                returnString = driverObj.GenerateDriverRegOTP(transporterID, driverMobile, ref OTPNumber);                

                if (returnString.ToUpper() == "SUCCESS")
                {
                    string MessageToSend = "Your OTP to register driver app: " + OTPNumber.ToString();

                    string tempVal = SMSHelper.SendSMSMsg(driverMobile, "OTP Number", MessageToSend);

                    return SetReturnMessage(true, tempVal, "", "");

                }
                else
                {
                    return SetReturnMessage(false, returnString, "", "");
                }
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

        [HttpGet]
        [Route("RegisterDriverApp/{appID}/{transporterCode}/{driverNumber}/{OTPNumber}")]
        public HttpResponseMessage RegisterDriverApp(string appID, string transporterCode, string driverNumber, string OTPNumber)
        {
            var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            
            try
            {
                UserBL returnObj = new UserBL();
                int transporterID = Convert.ToInt32(transporterCode);
                long driverMobileNum = Convert.ToInt64(driverNumber);
                int OtpNum = Convert.ToInt32(OTPNumber);

                string returnString = string.Empty;
                returnString = returnObj.RegisterDriverAPP(appID, transporterID, driverMobileNum, OtpNum);

                if (returnString.ToUpper() == "SUCCESS")
                {
                    return SetReturnMessage(true, returnString, "", "");
                    
                }
                else
                {
                    return SetReturnMessage(false, returnString, "", "");
                }
                
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

    }
}
