﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Transporter.API
{
    public class BaseAPIController : ApiController
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public NotFoundTextPlainActionResult NotFound(string message)
        {
            return new NotFoundTextPlainActionResult(message, Request);
        }

        public string GetRequestHeader(HttpRequestMessage req, string key)
        {
            IEnumerable<string> headerValues = req.Headers.GetValues(key);
            var id = headerValues.FirstOrDefault();
            return id.ToString();
        }


        public HttpResponseMessage NoDataFound()
        {
            var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            response.Content = new StringContent("No data found", System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        public class MobileUser
        {
            public int UserID { get; set; }
            public string ValidityDate { get; set; }
            public string UserToken { get; set;}
        }

        private class ReturnMessage
        {
            public string Message { get; set; }
            public string RedirectURL { get; set; }
            public Boolean IsSuccess { get; set; }
            public object Data { get; set; }
        }
        
        public HttpResponseMessage SetReturnMessage(Boolean isSuccess, string msgToSet, string redirectURL, object data)
        {
            var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            ReturnMessage retMsg = new ReturnMessage();
            string retVal = "";

            retMsg.IsSuccess = isSuccess;
            retMsg.Message = msgToSet;
            retMsg.RedirectURL = redirectURL;
            retMsg.Data = data;

            retVal = JsonConvert.SerializeObject(retMsg);
            retVal = retVal.Replace(@"\", "");
            if (isSuccess)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(retVal, System.Text.Encoding.UTF8, "application/json");
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(retVal, System.Text.Encoding.UTF8, "application/json");
                return response;
            }
        }

        public string SendCommunication(string MsgType, string EmailID, long toNumber, string subject, string resourceKey, int refID1, string refID2, string refID3, string refID4, string refType, int loggedInUserID)
        {
            string EmailStatus = "";
            log.Info("Send Communication from API status for userID " + loggedInUserID.ToString() + " Started '" + MsgType + "'");
            string retVal = TransporterCore.CommunicatorBL.SendEmail(MsgType, EmailID, subject, Message.ResourceManager.GetString(MsgType + "Email").ToString().Replace("{0}", refID1.ToString()).Replace("{1}", refID2.ToString()).Replace("{2}", refID3.ToString()).Replace("{3}", refID4.ToString()), loggedInUserID, refID1, refType, ref EmailStatus);
            log.Info("Send Communication status for userID " + loggedInUserID.ToString() + " on action '" + MsgType + "Email" + "' : " + EmailStatus);
            retVal = TransporterCore.CommunicatorBL.SendSMS(MsgType, toNumber, subject, Message.ResourceManager.GetString(MsgType + "SMS").ToString().Replace("{0}", refID1.ToString()).Replace("{1}", refID2.ToString()).Replace("{2}", refID3.ToString()).Replace("{3}", refID4.ToString()), loggedInUserID, refID1, refType);
            log.Info("Send Communication from API status for userID " + loggedInUserID.ToString() + " on action '" + MsgType + "SMS" + "' : " + retVal);
            return retVal;
        }
    }




    public class NotFoundTextPlainActionResult : ApiController
    {
        public NotFoundTextPlainActionResult(string message, HttpRequestMessage request)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Message = message;
            thisRequest = request;
        }

        public string Message { get; private set; }

        public HttpRequestMessage thisRequest { get; private set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        public HttpResponseMessage Execute()
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.NotFound);
            response.Content = new StringContent(Message); // Put the message in the response body (text/plain content).
            response.RequestMessage = thisRequest;
            return response;
        }
    }

    public static class ApiControllerExtensions
    {
        public static NotFoundTextPlainActionResult NotFound(this ApiController controller, string message)
        {
            return new NotFoundTextPlainActionResult(message, controller.Request);
        }
    }

}
