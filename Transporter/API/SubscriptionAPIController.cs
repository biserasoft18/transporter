﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransporterCore;
using System.Web.Script.Serialization;
using TransporterModel;

namespace Transporter.API
{
    [RoutePrefix("api/Subscription")]
    public class SubscriptionAPIController : BaseAPIController
    {   
        [HttpPost]
        [Route("PaymentConfirmation")]
        public HttpResponseMessage PaymentConfirmation([FromBody] WebHookResponse value)
        {
            try
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                log.Info("PaymentConfirmation processing start");
                //WebHookResponse obj = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<WebHookResponse>(value);
                if (value != null)
                {
                    log.Info("writing value");
                    log.Info(value.ToString());
                }
                else
                {
                    log.Info("response body /value seems to be null");
                }

                
                log.Info("PaymentConfirmation processing end");
                return SetReturnMessage(true, "Success", "", "Success");
            }
            catch (Exception ex)
            {
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

        [HttpPost]
        [Route("PaymentConfirmation1")]
        public HttpResponseMessage PaymentConfirmation1([FromBody] string value)
        {
            try
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                log.Info("PaymentConfirmation1 processing start");
                WebHookResponse obj = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<WebHookResponse>(value);
                if (value != null)
                {
                    log.Info("writing value");
                    log.Info(value.ToString());
                }
                else
                {
                    log.Info("response body /value seems to be null");
                }

                
                if (obj != null)
                {
                    log.Info("writing obj");
                    log.Info(obj.ToString());
                    log.Info(obj.status.ToString());
                }
                else
                {
                    log.Info("response body /obj seems to be null");
                }
                
                log.Info("PaymentConfirmation1 processing end");
                return SetReturnMessage(true, "Success", "", "Success");
            }
            catch (Exception ex)
            {
                log.Info(ex.Message.ToString());
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }


        [HttpPost]
        [Route("PaymentConfirmation2")]
        public HttpResponseMessage PaymentConfirmation2([FromBody] string value)
        {
            try
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                log.Info("PaymentConfirmation2 processing start");
                WebHookResponse obj = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<WebHookResponse>(value);
                if (value != null)
                {
                    log.Info("writing value");
                    log.Info(value.ToString());
                }
                else
                {
                    log.Info("response body /value seems to be null");
                }


                if (obj != null)
                {
                    log.Info("writing obj");
                    log.Info(obj.ToString());
                    log.Info(obj.status.ToString());
                }
                else
                {
                    log.Info("response body /obj seems to be null");
                }

                log.Info("PaymentConfirmation2 processing end");
                return SetReturnMessage(true, "Success", "", "Success");
            }
            catch (Exception ex)
            {
                log.Info(ex.Message.ToString());
                return SetReturnMessage(false, ex.Message.ToString(), "", "");
            }
        }

        //[HttpPost]
        //[Route("PaymentConfirmation/{pID}")]
        //public HttpResponseMessage PaymentConfirmation(int internalPaymentID)
        //{

        //    var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
        //    //string retVal = "";
        //    try
        //    {
        //        SubscriptionBL paymentObj = new SubscriptionBL();
        //        Streamre new StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();
        //        paymentObj.UpsertPaymentDetails(0, DBNull.Value.ToString(), DBNull.Value.ToString(), 0,
        //                    DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(),
        //                    DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString()
        //                    , DBNull.Value.ToString(), DBNull.Value.ToString(), 1, DBNull.Value.ToString(), ref internalPaymentID);

        //        return SetReturnMessage(true, "Success", "", "Success");
        //    }
        //    catch (Exception ex)
        //    {
        //        return SetReturnMessage(false, ex.Message.ToString(), "", "");
        //    }

        //}

        //[HttpPost]
        //[Route("PaymentConfirmation/{pID}")]
        //public HttpResponseMessage PaymentConfirmation([FromBody] PaymentResponse value, int pID)
        //{
        //    var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
        //    //string retVal = "";
        //    int retVal = 1;
        //    try
        //    {
        //        SubscriptionBL paymentObj = new SubscriptionBL();
        //        var json = new JavaScriptSerializer().Serialize(value);
        //        log.Info("From Webhook:" + json);
        //        paymentObj.UpsertPaymentDetails(0, DBNull.Value.ToString(), DBNull.Value.ToString(), 0,
        //                    DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(),
        //                    DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString()
        //                    , DBNull.Value.ToString(), DBNull.Value.ToString(), 1, json, ref retVal);

        //        return SetReturnMessage(true, "Success", "", "Success");
        //    }
        //    catch (Exception ex)
        //    {
        //        return SetReturnMessage(false, ex.Message.ToString(), "", "");
        //    }
        //}

    }
}
