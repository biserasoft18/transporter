﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Transporter.Models
{
    public class LoadBooking
    {
        public int BookingID { get; set; }
        public int UserID { get; set; }
        public int SourceCityID { get; set; }
        public int DestinationCityID { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int GoodsTypeID { get; set; }
        public int GoodsWeightID { get; set; }
        public int PackageTypeID { get; set;}
        public int TruckTypeID { get; set; }
        public int PaymentTypeID { get; set; }
        public string ContactPersonName { get; set; }
        public int ContactPersonPhoneNumber { get; set; }
        public int ContactPersonMobileNumber { get; set; }
        public string ContactPersonEmail { get; set; }
        public string PhotoPath { get; set; }
    }
}