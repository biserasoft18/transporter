﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Transporter.Models
{
    public class RegisterBooking
    {
        public int UserID { get; set; }
        public int UserTypeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int LoginID { get; set; }
        public string Password { get; set; }
        public int InvalidLoginAttempt { get; set; }
        public int ParentCompanyID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public int StateID { get; set; }
        public int PinCode { get; set; }
        public int Country { get; set; }
        public string EmailID { get; set; }
        public int PhoneNumber { get; set; }
        public int MobileNumber { get; set; }
        public string Website { get; set; }
        public int SecretQuestionID { get; set; }
        public string SecretAnswer { get; set; }
        public string PANNumber { get; set; }
        public string TINNumber { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public int IsAdmin { get; set; }
        public int IsPaidUser { get; set; }
        public int LocalityID { get; set; }
        public string FacebookkID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}