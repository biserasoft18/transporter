﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransporterData;
using TransporterModel;
using System.Data;
using System.Data.SqlClient;

namespace TransporterCore
{
    public class DashBoardBL
    {
        UserData DBObj = new UserData();
        Masters DAO = new Masters();
        SqlHelper DBHelp = new SqlHelper();

        public DashBoardPage GetDashBoardPageDetails(int UserID)
        {
            DashBoardPage returnObj = new DashBoardPage();
            DataSet DS = DAO.GetDashBoardPageDetails(UserID);

            if (DS.Tables[0].Rows.Count > 0)
            {
                returnObj.DashBoardDetails = DBHelp.ConvertTo<LoadDashBoard>(DS.Tables[0])[0];
            }
            else
            {
                returnObj.DashBoardDetails = new LoadDashBoard();
            }
           

            return returnObj;
        }

    }
}
