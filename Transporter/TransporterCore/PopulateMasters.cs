﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransporterData;
using TransporterModel;
using System.Data;

namespace TransporterCore
{
    public class PopulateMasters
    {
        SqlHelper DBHelp = new SqlHelper();
        Masters DAO = new Masters();

        public List<GoodsType> FetchGoodsType(int typeID)
        {
            return DBHelp.ConvertTo<GoodsType>(DAO.FetchGoodType(typeID));
        }

        public List<GoodsWeight> FetchGoodsWeight(int typeID)
        {
            return DBHelp.ConvertTo<GoodsWeight>(DAO.FetchGoodWeight(typeID));
        }

        public List<PackageType> FetchPackageType(int typeID)
        {
            return DBHelp.ConvertTo<PackageType>(DAO.FetchPackageType(typeID));
        }

        public List<PaymentType> FetchPaymentType(int typeID)
        {
            return DBHelp.ConvertTo<PaymentType>(DAO.FetchPaymentType(typeID));
        }

        public List<UserType> FetchUserType(int typeID)
        {
            return DBHelp.ConvertTo<UserType>(DAO.FetchUserType(typeID));
        }

        public List<City> FetchCity(int typeID)
        {
            return DBHelp.ConvertTo<City>(DAO.FetchCity(typeID));
        }
        

        public List<State> FetchState(int typeID)
        {
            return DBHelp.ConvertTo<State>(DAO.FetchState(typeID));
        }

       public List<BookingStatus> FetchBookingStatus(int typeID)
        {
            return DBHelp.ConvertTo<BookingStatus>(DAO.FetchBookingStatus(typeID));
        }
        public List<PostingStatus> FetchPostingStatus(int typeID)
        {
            return DBHelp.ConvertTo<PostingStatus>(DAO.FetchPostingStatus(typeID));
        }
        //Added New Business Type for Admin Reports
        public List<BusinessTypeMaster> FetchBusinessType(int typeID)
        {
            return DBHelp.ConvertTo<BusinessTypeMaster>(DAO.FetchBusinessType(typeID));
        }

        public List<Country> FetchCountry(int typeID)
        {
            return DBHelp.ConvertTo<Country>(DAO.FetchCountry(typeID));
        }

        public List<Town> FetchTown(int typeID)
        {
            return DBHelp.ConvertTo<Town>(DAO.FetchTown(typeID));
        }

        public List<SecretQuestion> FetchSecretQuestion(int typeID)
        {
            return DBHelp.ConvertTo<SecretQuestion>(DAO.FetchSecretQuestion(typeID));
        }

        public List<City> FilterCity(string cityName)
        {
            return DBHelp.ConvertTo<City>(DAO.FilterCity(cityName));
        }

        public List<TruckType> FetchTruckType(int typeID)
        {
            return DBHelp.ConvertTo<TruckType>(DAO.FetchTruckType(typeID));
        }        

    }
}
