﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericHelper;

namespace TransporterCore
{
    public static class CommunicatorBL
    {
        public static string SendEmail(string MessageType, string ToAddress, string MailSubject, string MailBody, int LoggedInUserID, int RefID, string RefType, ref string OutputStatus)
        {
            Boolean retStatus = EmailHelper.SendEmail(ToAddress, MailSubject, MailBody, ref OutputStatus);
            TransporterData.Communicator refObj = new TransporterData.Communicator();
            return refObj.LogCommunicationStatus(MessageType, OutputStatus, retStatus, "EMAIL", LoggedInUserID, RefID, RefType, MailBody, ToAddress);
        }

        public static string SendSMS(string MessageType, Int64 toNumber, string subject, string body, int LoggedInUserID, int RefID, string RefType)
        {
            if(body.Trim().Length > 0)
            {
                string retVal = SMSHelper.SendSMSMsg(toNumber, subject, body);
                TransporterData.Communicator refObj = new TransporterData.Communicator();
                return refObj.LogCommunicationStatus(MessageType, retVal, retVal.ToUpper().Contains("FAILURE") ? false:true, "SMS", LoggedInUserID, RefID, RefType, body, toNumber.ToString());
            }
            else
            {
                return "";
            }
        }        
    }
}
