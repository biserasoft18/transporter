﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransporterModel;
using TransporterData;
using System.Data;

namespace TransporterCore
{
    public class SubscriptionBL
    {
        PaymentDetails DBObj = new PaymentDetails();
        SqlHelper DBHelp = new SqlHelper();

        public UserPaymentHistory GetPaymentDetails(int UserID, int loggedInUser, ref string retVal)
        {
            UserPaymentHistory obj = new UserPaymentHistory();
            List<UserPaymentDetails> retObj = new List<UserPaymentDetails>();
            obj.PaymentDetails = retObj;
            DataSet Ds = DBObj.GetUserPaymentDetails(UserID, loggedInUser, ref retVal);

            if (retVal.ToUpper().Contains("SUCCESS"))
            {
                if (Ds.Tables.Count > 0)
                {
                    obj.UserName = Ds.Tables[0].Rows[0]["UserName"].ToString();
                    obj.MemberSince = Ds.Tables[0].Rows[0]["MemberSince"].ToString();
                    obj.MemberStatus = Ds.Tables[0].Rows[0]["MemberStatus"].ToString();
                    obj.UserType = Ds.Tables[0].Rows[0]["UserTypeName"].ToString();
                    obj.LastPaymentAttemptedOn = Ds.Tables[0].Rows[0]["LastPaymentAttemptedOn"].ToString();
                    obj.UserID = Convert.ToInt32(Ds.Tables[0].Rows[0]["UserID"].ToString());
                }
                if (Ds.Tables.Count > 1)
                {
                    obj.PaymentDetails = DBHelp.ConvertTo<UserPaymentDetails>(Ds.Tables[1]);
                }
            }
            
            return obj;
        }
        public string UpsertPaymentDetails(int userID, string curExpiryDate, string reqExpiryDate, Decimal paymentAmount, string extTransId, string extPaymId, string redirectURL,
            string webhookURL, string paymentURL, string initiateOn, string acknowOn, string finalRespOn,
            string extMacId, string extPayRespId, int paymentStatus, string webHookResponse, ref int intPaymentId)
        {
            string retObj = DBObj.UpsertPaymentDetails(userID, curExpiryDate, reqExpiryDate, paymentAmount, extTransId, extPaymId, redirectURL,
            webhookURL, paymentURL, initiateOn, acknowOn, finalRespOn,
            extMacId, extPayRespId, paymentStatus, webHookResponse, ref intPaymentId);

            return retObj;
        }
    }
}