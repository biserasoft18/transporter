﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransporterData;
using TransporterModel;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using GenericHelper;

namespace TransporterCore
{
    public class ReportBL
    {
        Reports DAO = new Reports();

        public ReportSchema GetGenericReportDetails(string reportName,int logInUserID)
        {
            ReportSchema returnObj = new ReportSchema();
            string reportXML= DAO.GetGenericReportDetails(reportName, logInUserID);

            XmlSerializer serializer = new XmlSerializer(typeof(ReportSchema));
            using (TextReader reader = new StringReader(reportXML))
            {
                returnObj = (ReportSchema)serializer.Deserialize(reader);
            }

            return returnObj;
        }


        public string GetGenericReportData(string SPName, ReportParamCollection sqlparams)
        {
            DataTable dt = new DataTable();
            SqlParameter[] SPparams = new SqlParameter[sqlparams.ReportParams.Count];
            for(int i=0;i<sqlparams.ReportParams.Count; i++)
            {
                SqlParameter tempObj = new SqlParameter();
                tempObj.Direction = ParameterDirection.Input;
                tempObj.SqlValue = sqlparams.ReportParams[i].ParamData;
                
                switch (sqlparams.ReportParams[i].ParamDataType)
                {
                    case FieldType.String: // string
                        tempObj.SqlDbType = SqlDbType.VarChar;
                        tempObj.Size = sqlparams.ReportParams[i].ParamSize;
                        break;
                    case FieldType.DateTime: // datetime
                        tempObj.SqlDbType = SqlDbType.DateTime2;
                        tempObj.Size = sqlparams.ReportParams[i].ParamSize;
                        break;
                    case FieldType.DropDown: //dropdown
                        tempObj.SqlDbType = SqlDbType.Int;
                        break;
                    case FieldType.CitySelect: //cityselect
                        tempObj.SqlDbType = SqlDbType.Int;
                        break;
                    case FieldType.Integer: //integer
                        tempObj.SqlDbType = SqlDbType.Int;
                        break;
                    case FieldType.StateSelect: //stateselect
                        tempObj.SqlDbType = SqlDbType.Int;
                        break;
                    default:
                        tempObj.SqlDbType = SqlDbType.VarChar;
                        tempObj.Size = sqlparams.ReportParams[i].ParamSize;
                        break;
                }
                tempObj.ParameterName = sqlparams.ReportParams[i].ParamName;
                SPparams[i] = tempObj;
            }
            dt = DAO.GetGenericReportData(SPName, SPparams);

            return DBHelper.ConvertDataTableToHTML(dt, false, false);
        }

    }
}
