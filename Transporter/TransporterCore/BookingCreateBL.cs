﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransporterData;
using TransporterModel;
using System.Data;

namespace TransporterCore
{
    public class BookingCreateBL
    {
        SqlHelper DBHelp = new SqlHelper();
        Masters DAO = new Masters();

        public BookingPage GetBookingPageDetails(int bookingID)
        {
            BookingPage returnObj = new BookingPage();
            DataSet DS = DAO.GetBookingPageDetails(bookingID);

            if (DS.Tables[0].Rows.Count > 0)
            {
                returnObj.BookingDetails = DBHelp.ConvertTo<LoadBooking>(DS.Tables[0])[0];
            }
            else
            {
                returnObj.BookingDetails = new LoadBooking();
            }
            returnObj.GoodsType = DBHelp.ConvertTo<GoodsType>(DS.Tables[1]);
            returnObj.GoodsWeight = DBHelp.ConvertTo<GoodsWeight>(DS.Tables[2]);
            returnObj.PackageType = DBHelp.ConvertTo<PackageType>(DS.Tables[3]);
            returnObj.TruckType = DBHelp.ConvertTo<TruckType>(DS.Tables[4]);
            returnObj.PaymentType = DBHelp.ConvertTo<PaymentType>(DS.Tables[5]);

            return returnObj;
        }

        public string SetBookingData(LoadBooking bookingData, BookingAddress AddressData,  ref int bookingID, int userID)
        {
            
            Booking CreateEditBooking = new Booking();
            String returnVal = "";
            //DataTable dt = GenericHelper.DBHelper.ObjectToData(bookingData);

            DataTable dt = DBHelp.ObjectToData(bookingData);
            returnVal = CreateEditBooking.SetBookingData(dt,  ref bookingID, userID);

            DataTable dtPickupAddress = new DataTable();
            DataTable dtDropAddress = new DataTable();
            if (AddressData?.PickupAddress != null)
            {
                AddressData.PickupAddress.CreatedBy = userID;
                AddressData.PickupAddress.UpdatedBy = userID;
                AddressData.PickupAddress.AddressTypeID = (int)AddressTypeID.Pickup;
                dtPickupAddress = DBHelp.ObjectToData(AddressData.PickupAddress);
            }
            if (AddressData?.DropAddress != null)
            {
                AddressData.DropAddress.CreatedBy = userID;
                AddressData.DropAddress.UpdatedBy = userID;
                AddressData.DropAddress.AddressTypeID = (int)AddressTypeID.Drop;
                dtDropAddress = DBHelp.ObjectToData(AddressData.DropAddress);
            }
            if(dtPickupAddress.Rows.Count >0 || dtDropAddress.Rows.Count > 0)
            {
                CreateEditBooking.SetBookingAddress(dtPickupAddress, dtDropAddress, bookingID, userID);
            }            

            return returnVal;
        }

        public AgentSearchPage GetAgentSearchDetails(int userID, Boolean needAllSearchData)
        {
            AgentSearchPage returnObj = new AgentSearchPage();
            DataSet DS = DAO.GetBookingSearchDetails(userID, needAllSearchData);
            returnObj.SearchDetails = new LoadSearchPage();
            if (DS.Tables[3].Rows.Count > 0)
            {
                returnObj.SearchData = DBHelp.ConvertTo<LoadSearchPage>(DS.Tables[3]);
            }
            else
            {
                returnObj.SearchData = new List<LoadSearchPage>();
            }
            returnObj.GoodsWeight = DBHelp.ConvertTo<GoodsWeight>(DS.Tables[0]);
            returnObj.TruckType = DBHelp.ConvertTo<TruckType>(DS.Tables[1]);
            returnObj.BookingStatus = DBHelp.ConvertTo<BookingStatus>(DS.Tables[2]);
            return returnObj;
        }


        public DataTable SearchforAgents(int SourceCityID, int DestinationCityID, DateTime DeliveryDate, int GoodsWeightID, int TruckTypeID,int BookingStatusID, int UserID, int BookingID)
        {
            AgentSearchDetails returnObj = new AgentSearchDetails();
            DataSet DS = DAO.SearchforAgents(SourceCityID, DestinationCityID, DeliveryDate, GoodsWeightID, TruckTypeID, BookingStatusID, UserID, BookingID);
            return DS.Tables[0];
        }



        public string GetMessageToAgent(int bookingID, int currUserID, ref int PostingID)
        {
            Booking returnObj = new Booking();
            return returnObj.GetMessageToAgent(bookingID, currUserID, ref PostingID);
        }

        public List<BookingMessage> GetAllMessageToAgent(int bookingID, int currUserID, Boolean isReqToTransporter)
        {
            List<BookingMessage> returnObj = new List<BookingMessage>();
            Booking bookingObj = new Booking();
            DataSet DS = bookingObj.GetAllMessageToAgent(bookingID, currUserID, isReqToTransporter);
            returnObj = DBHelp.ConvertTo<BookingMessage>(DS.Tables[0]);
            return returnObj;
        }
        
        public string SetbookingMessage(string Message, int bookingID, int postingID, int userID, ref string agentEmail, ref long agentMobileNumber)
        {

            Booking CreateEditbooking = new Booking();
            String returnVal = "";

            returnVal = CreateEditbooking.SetbookingMessage(Message, bookingID, postingID, userID, ref agentEmail, ref agentMobileNumber);

            return returnVal;
        }

        public string AcceptDeal(int TransporterID, int bookingID, int postingID, int loggedInUserID, ref string transporterEmail, ref long transporterMobileNumber)
        {
            Booking CreateEditbooking = new Booking();
            String returnVal = "";

            returnVal = CreateEditbooking.AcceptDeal(TransporterID, bookingID, postingID, loggedInUserID, ref transporterEmail, ref transporterMobileNumber);

            return returnVal;
        }

        public List<int> GetBookingIDList(int LoggedInUser)
        {
            Booking returnObj = new Booking();
            return returnObj.GetBookingIDList(LoggedInUser);
        }
        
        public string CancelBooking(int bookingID, int LogUserID)
        {
            Booking BookingObj = new Booking();

            return BookingObj.CancelBooking(bookingID, LogUserID) ;
        }

    }
}
