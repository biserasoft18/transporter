﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransporterData;
using TransporterModel;
using System.Data;
using System.Data.SqlClient;

namespace TransporterCore
{
    public class UserBL
    {
        UserData DBObj = new UserData();
        Masters DAO = new Masters();
        SqlHelper DBHelp = new SqlHelper();

        public string ValidateLogin(string Username, string Password, ref List<User> UserObj, ref List<ReportsPageMenu> ReportMenu)
        {
            DataSet DS = DBObj.ValidateUser(Username, Password);
            if (DS.Tables[0].Rows[0]["Uservalidity"].ToString().ToUpper() == "VALID")
            {
                //UserObj = DBHelp.BindList<User>(DT);
                UserObj = DBHelp.ConvertTo<User>(DS.Tables[0]);
                ReportMenu = DBHelp.ConvertTo<ReportsPageMenu>(DS.Tables[1]);
                return DS.Tables[0].Rows[0]["Uservalidity"].ToString();
            }
            else
            {
                return DS.Tables[0].Rows[0]["Uservalidity"].ToString();
            }
        }


        public DataTable GetUserPassword(string username, string EmailID, string secpasw)
        {
            DataTable DT = DBObj.GetUserPassword(username, EmailID, secpasw);
           return DT;
        }

    public RegistrationPage GetRegisterDetails(int UserID)
        {
            RegistrationPage Obj = new RegistrationPage();
            DataSet DS = DAO.GetRegisterDetails(UserID);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Obj.RegistrationDetails  = DBHelp.ConvertTo<LoadRegistration>(DS.Tables[0])[0];
            }
            else
            {
                Obj.RegistrationDetails = new LoadRegistration();
            }
            Obj.UserType = DBHelp.ConvertTo<UserType>(DS.Tables[1]);
            Obj.SecretQuestion = DBHelp.ConvertTo<SecretQuestion>(DS.Tables[2]);
            Obj.Country = DBHelp.ConvertTo<Country>(DS.Tables[3]);
            Obj.State = DBHelp.ConvertTo<State>(DS.Tables[4]);
            Obj.City = DBHelp.ConvertTo<City>(DS.Tables[5]);
            Obj.Town= DBHelp.ConvertTo<Town>(DS.Tables[6]);


            return Obj;
        }

        public DataTable GetStateDetails(int CountryID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@CountryID", CountryID);
            DataSet ds = new DataSet();
            DBHelp.Fill(ds, GenericHelper.Constants.SPName.GetStateDetails, objParameter);
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }

        public DataTable GetCityDetails(int StateID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@StateID", StateID);
            DataSet ds = new DataSet();
            DBHelp.Fill(ds, GenericHelper.Constants.SPName.GetCityDetails, objParameter);
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }

        public DataTable GetTownDetails(int CityID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@CityID", CityID);
            DataSet ds = new DataSet();
            DBHelp.Fill(ds, GenericHelper.Constants.SPName.GetTownDetails, objParameter);
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }

        public DataTable GetUserTypeDetails(int UserTypeID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] objParameter = new SqlParameter[1];
            objParameter[0] = new SqlParameter("@UserTypeID", UserTypeID);
            DataSet ds = new DataSet();
            DBHelp.Fill(ds, GenericHelper.Constants.SPName.GetUserTypeDetails, objParameter);
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            return dt;
        }

        public string ValidateLoginID(string LoginID)
        {
            DataTable DT = DBObj.ValidateLoginID(LoginID);
            if (DT.Rows[0]["Uservalidity"].ToString().ToUpper() == "VALID")
            {
                //UserObj = DBHelp.BindList<User>(DT);

                return DT.Rows[0]["Uservalidity"].ToString();
            }
            else
            {
                return DT.Rows[0]["Uservalidity"].ToString();
            }
        }

        public string SetRegisterData(LoadRegistration RegisterationData, ref int UserID)
        {

            UserData CreateRegistration = new UserData();
            String returnVal = "";
            //DataTable dt = GenericHelper.DBHelper.ObjectToData(bookingData);

            DataTable dt = DBHelp.ObjectToData(RegisterationData);

            returnVal = CreateRegistration.SetRegisterData(dt, ref UserID);

            return returnVal;
        }

        public DataTable GetPostingResponseFromAgent(int UserID)
        {
            LoadTransporterUserInbox returnObj = new LoadTransporterUserInbox();
            DataSet DS = DAO.GetPostingResponseFromAgent(UserID);

            return DS.Tables[0];
        }

        public DataTable GetBookingResponseFromTransporter(int UserID)
        {
            LoadAgentUserInbox returnObj = new LoadAgentUserInbox();
            DataSet DS = DAO.GetBookingResponseFromTransporter(UserID);

            return DS.Tables[0];
        }

        public string SetDriverDetails(DriverShell driverObj, int loggedInUserID )
        {
            UserData manageDriver = new UserData();
            String returnVal = "";

            DataTable dt = DBHelp.ObjectToData(driverObj);

            returnVal = manageDriver.SetDriverData(dt, loggedInUserID);

            return returnVal;
        }

        public DataTable GetDriverDetails(int driverID, int loggedInUser)
        {
            UserData returnObj = new UserData();
            return returnObj.GetDriverDetails(driverID, loggedInUser);
        }

        public string ValidateAppLogin(string AppID, ref string outVal, ref int driverUserID, ref string userToken)
        {
            UserData returnObj = new UserData();
            return returnObj.ValidateAppLogin(AppID, ref outVal, ref driverUserID, ref userToken);

        }

        public string GenerateDriverRegOTP(int transporterID, Int64 driverMobileNum, ref int OTPNumber)
        {
            UserData returnObj = new UserData();
            return returnObj.GenerateDriverRegOTP(transporterID, driverMobileNum, ref OTPNumber);
        }

        public string RegisterDriverAPP(string appID, int transporterID, Int64 driverMobileNum, int OTPNumber)
        {
            UserData returnObj = new UserData();
            return returnObj.RegisterDriverAPP(appID, transporterID, driverMobileNum, OTPNumber);
        }

        public string LogUserDetails(UserLog userDetails, int userID, string envDetails)
        {
            UserData CreateLog = new UserData();
            DataTable dt = DBHelp.ObjectToData(userDetails);
            return CreateLog.SetUserLog(dt, userID, envDetails);
        }

    }
}
