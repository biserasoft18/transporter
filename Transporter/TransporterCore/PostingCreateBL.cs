﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransporterData;
using TransporterModel;
using System.Data;
using GenericHelper;

namespace TransporterCore
{
    public class PostingCreateBL
    {
        SqlHelper DBHelp = new SqlHelper();
        Masters DAO = new Masters();

        public PostingPage GetPostingPageDetails(int postingID)
        {
            PostingPage returnObj = new PostingPage();
            DataSet DS = DAO.GetPostingPageDetails(postingID);

            if (DS.Tables[0].Rows.Count > 0)
            {
                returnObj.PostingDetails = DBHelp.ConvertTo<LoadPosting>(DS.Tables[0])[0];
            }
            else
            {
                returnObj.PostingDetails = new LoadPosting();
            }
            returnObj.TruckType = DBHelp.ConvertTo<TruckType>(DS.Tables[1]);
            returnObj.GoodsWeight = DBHelp.ConvertTo<GoodsWeight>(DS.Tables[2]);
            returnObj.IsCargoInsured = DBHelp.ConvertTo<YesNo>(DS.Tables[3]);
            returnObj.ViaCities = DBHelp.ConvertTo<City>(DS.Tables[4]);

            return returnObj;
        }

        public DataTable GetMessageToTransporter(int postingID, int currUserID, ref int countbookingID)
        {
            Posting returnObj = new Posting();
            DataSet DS= returnObj.GetMessageToTransporter(postingID, currUserID, ref countbookingID);
            return DS.Tables[0];
        }

        public List<MessageToOtherAgents> GetAllMessageToAgent(int postingID, int currUserID)
        {
            List<MessageToOtherAgents> returnObj = new List<MessageToOtherAgents>();
            Posting postingObj = new Posting();
            DataSet DS = postingObj.GetMessageToAgent(postingID, currUserID);
            returnObj = DBHelp.ConvertTo<MessageToOtherAgents>(DS.Tables[0]);
            return returnObj;
        }

        public List<PostingMessage> GetAllMessageToTransporter(int postingID, int currUserID)
        {
            List <PostingMessage> returnObj= new List<PostingMessage>();
            Posting postingObj = new Posting();
            DataSet DS = postingObj.GetAllMessageToTransporter(postingID, currUserID);
            returnObj = DBHelp.ConvertTo<PostingMessage>(DS.Tables[0]);
            return returnObj;
        }

        public TripTracking GetTripGeoLocData(int postingID, int loggedInUserID)
        {
            TripTracking retObj = new TripTracking();
            retObj.TripData = new List<TripViaRoutes>();
            retObj.TruckTrackingData  = new List<TruckTracking>();
            Posting postingObj = new Posting();
            string result = "";
            DataSet DS = postingObj.GetTripGeoLocData(postingID, loggedInUserID, ref result);
            retObj.Result = result;
            retObj.TripData = DBHelp.ConvertTo<TripViaRoutes>(DS.Tables[0]);
            retObj.TruckTrackingData = DBHelp.ConvertTo<TruckTracking>(DS.Tables[1]);

            return retObj;
        }

        public TripSheet GetTripData(int postingID, int bookingID, int userID)
        {
            TripSheet returnObj = new TripSheet();
            Posting postingObj = new Posting();
            DataSet DS = postingObj.GetTripSheetData(postingID, bookingID, userID);
            if(DS.Tables.Count > 0)
            {
                if(DS.Tables[0].Rows.Count > 0)
                {
                    returnObj.PostingID = Convert.ToInt32(DS.Tables[0].Rows[0]["PostingID"] == System.DBNull.Value ? 0 : DS.Tables[0].Rows[0]["PostingID"]);
                    returnObj.DriverID = Convert.ToInt32(DS.Tables[0].Rows[0]["DriverID"] == System.DBNull.Value ? 0 : DS.Tables[0].Rows[0]["DriverID"]);
                    returnObj.TruckNumber = Convert.ToString(DS.Tables[0].Rows[0]["TruckNumber"] == System.DBNull.Value ? "" : DS.Tables[0].Rows[0]["TruckNumber"]);
                    returnObj.StartDate = Convert.ToString(DS.Tables[0].Rows[0]["StartDate"] == System.DBNull.Value ? "" : DS.Tables[0].Rows[0]["StartDate"]);
                    returnObj.PostingStatus = Convert.ToInt32(DS.Tables[0].Rows[0]["PostingStatus"] == System.DBNull.Value ? 0 : DS.Tables[0].Rows[0]["PostingStatus"]);
                    returnObj.PostingStatusName = Convert.ToString(DS.Tables[0].Rows[0]["PostingStatusName"] == System.DBNull.Value ? "" : DS.Tables[0].Rows[0]["PostingStatusName"]);
                    returnObj.ViaRoutes = Convert.ToString(DS.Tables[0].Rows[0]["ViaRoutes"] == System.DBNull.Value ? "" : DS.Tables[0].Rows[0]["ViaRoutes"]);

                    returnObj.TripDriverData = DBHelp.ConvertTo<TripDriverList>(DS.Tables[1]);
                    returnObj.TripBookingData = DBHelp.ConvertTo<TripBookingList>(DS.Tables[2]);
                }
            }
            returnObj.TruckTrackingDetails = GetTripGeoLocData(postingID, userID);


            return returnObj;

        }

        public string SetPostingData(LoadPosting postingData, List<City> ViaCitiesData,  ref int postingID, int userID, int postingStatus)
        {

            Posting CreateEditPosting = new Posting();
            String returnVal = "";
            //DataTable dt = GenericHelper.DBHelper.ObjectToData(bookingData);

            DataTable dt = DBHelp.ObjectToData(postingData);
            DataTable dtCity = DBHelp.ConvertToDataTable(ViaCitiesData);
            
            returnVal = CreateEditPosting.SetPostingData(dt, dtCity, ref postingID, userID, postingStatus);

            return returnVal;
        }

        public string SetPostingMessage(string Message, int postingID, int bookingID, int userID, ref string transEmail, ref long transMobileNumber)
        {

            Posting CreateEditPosting = new Posting();
            String returnVal = "";

            returnVal = CreateEditPosting.SetPostingMessage(Message, postingID, bookingID, userID, ref transEmail, ref transMobileNumber);

            return returnVal;
        }

        public string ClosePosting(int postingID, int postingOwnerID)
        {
            Posting CreateEditPosting = new Posting();
            String returnVal = "";

            returnVal = CreateEditPosting.ClosePosting(postingID, postingOwnerID);

            return returnVal;
        }

        public TransportSearchPage GetTransportSearchDetails(int userID, Boolean needAllSearchData)
        {
            TransportSearchPage returnObj = new TransportSearchPage();
            DataSet DS = DAO.GetTransportSearchDetails(userID, needAllSearchData);

            returnObj.TransportSearchDetails = new LoadTransportSearch();
            if (DS.Tables[4].Rows.Count > 0)
            {
                returnObj.TransportSearchData = DBHelp.ConvertTo<LoadTransporterSearchDetail>(DS.Tables[4]);
            }
            else
            {
                returnObj.TransportSearchData = new List<LoadTransporterSearchDetail>();
            }
            returnObj.GoodsWeight = DBHelp.ConvertTo<GoodsWeight>(DS.Tables[0]);
            returnObj.GoodsType = DBHelp.ConvertTo<GoodsType>(DS.Tables[1]);
            returnObj.TruckType = DBHelp.ConvertTo<TruckType>(DS.Tables[2]);
            returnObj.PostingStatus = DBHelp.ConvertTo<PostingStatus>(DS.Tables[3]);

            return returnObj;
        }

        public DataTable SearchforTransport(int SourceCityID, int DestinationCityID, DateTime DeliveryDate, int GoodsWeightID, int TruckTypeID, int LoggedInUser, decimal AvlCapacity, int PostingStatusID, int PostingID)
        {
            AgentSearchDetails returnObj = new AgentSearchDetails();
            DataSet DS = DAO.SearchforTransporters(SourceCityID, DestinationCityID, DeliveryDate, GoodsWeightID, TruckTypeID, LoggedInUser, AvlCapacity, PostingStatusID, PostingID);
            return DS.Tables[0];
        }

        public DataSet GetDriverTripData(int driverUserID, string appID, string userToken, ref string returnString)
        {
            Posting returnObj = new Posting();
            DataSet DS = returnObj.GetDriverTripData(driverUserID, appID, userToken, ref returnString);
            return DS;
        }

        public void SetTripData(int userID, string appID, int postingID, int bookingID, int sortOrder, string latid, string longit, int pickupOrDrop, string userToken, ref string retVal)
        {
            Posting returnObj = new Posting();
            returnObj.SetTripSheetData(userID, appID, postingID, bookingID, sortOrder, latid, longit, pickupOrDrop, userToken, ref retVal);
        }

        public void SetVehicleTrackingInfo(int userID, string appID, int postingID, string latid, string longit, string userToken, ref string retVal)
        {
            Posting returnObj = new Posting();
            returnObj.SetVehicleTrackingInfo(userID, appID, postingID, latid, longit,userToken, ref retVal);
        }

        public string AcceptDeal(int agentID, int bookingID, int postingID, int loggedInUserId, ref string agentEmail, ref long agentMobileNumber)
        {
            Posting CreateEditPosting = new Posting();
            String returnVal = "";

            returnVal = CreateEditPosting.AcceptDeal(agentID, bookingID, postingID, loggedInUserId, ref agentEmail, ref agentMobileNumber);

            return returnVal;
        }


        public DataSet ShowcontactMessage(int cAgentID, int logUserID)
        {
            Posting CreateShowcontactMessage = new Posting();

            DataSet DS  = CreateShowcontactMessage.ShowcontactMessage(cAgentID, logUserID);

            return DS;
        }

        public List<int> GetPostingIDList(int LoggedInUser)
        {
            Posting returnObj = new Posting();
            return returnObj.GetPostingIDList(LoggedInUser);
        }

        public string SetTripSheetData(int postingId, List<BookingSortInfo> sortData, int driverID, string truckNumber, int logUserID, ref List<TripBookingList> agentContactInfo)
        {

            Posting CreateTripSheet = new Posting();
            String returnVal = "";
            //DataTable dt = GenericHelper.DBHelper.ObjectToData(bookingData);
            
            DataTable dtSortData = DBHelp.ConvertToDataTable(sortData);

            DataTable refDT = new DataTable();

            returnVal = CreateTripSheet.SetTripSheetData(dtSortData, postingId, driverID, truckNumber, logUserID, ref refDT);

            agentContactInfo = DBHelp.ConvertTo<TripBookingList>(refDT);

            return returnVal;
        }

        public string EndTrip(int driverID, string appID, int postingID, string userToken)
        {
            Posting TripSheet = new Posting();
            String returnVal = "";

            returnVal = TripSheet.EndTrip(driverID, appID, postingID, userToken);

            return returnVal;
        }

        public string ResetTrip(int postingID)
        {
            Posting TripSheet = new Posting();
            String returnVal = "";

            returnVal = TripSheet.ResetTrip(postingID);

            return returnVal;
        }
        public List<TrackingDetails> GetTrackingDetails(int postingID)
        {
            Posting TripSheet = new Posting();

            DataSet DS = TripSheet.GetTrackingDetails(postingID);
            List<TrackingDetails> retObj = new List<TrackingDetails>();
            for(int i=0; i < DS.Tables[0].Rows.Count; i++)
            {
                TrackingDetails refObj = new TrackingDetails();
                refObj.DriverID = (int)DS.Tables[0].Rows[i]["DriverID"];
                refObj.PostingID = (Int64)DS.Tables[0].Rows[i]["PostingID"];
                refObj.Latitude = DS.Tables[0].Rows[i]["Latitude"].ToString();
                refObj.Longitude = DS.Tables[0].Rows[i]["Longitude"].ToString();
                refObj.CapturedOn = DS.Tables[0].Rows[i]["CapturedOn"].ToString();
                retObj.Add(refObj);
            }

            return retObj;
        }

        public string CancelPosting(int postingID, int LogUserID)
        {
            Posting PostingObj = new Posting();

            return PostingObj.CancelPosting(postingID, LogUserID);
        }


    }
}
